﻿namespace PaySharp.Services
{
    public interface IPasswordHasher
    {
        string GetHashString(string inputString);
    }
}