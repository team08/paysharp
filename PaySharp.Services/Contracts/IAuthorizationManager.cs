﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Contracts
{
    public interface IAuthorizationManager
    {
        void CheckIfLogged(string tokenValue);
        long GetLoggedUserId(string tokenValue);
        void CheckForRole(string tokenValue, string roleToCheckFor);
    }
}
