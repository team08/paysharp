﻿using PaySharp.Services.DTO;
using PaySharp.Services.Utilities.RandomGenerator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PaySharp.Services.Contracts
{
    public interface IBannerService
    {
        Task<IEnumerable<BannerDTO>> GetAllBannersAsync(int currentPage, int pageSize = 6);
        Task<BannerDTO> CreateBanner(string name, string imgPath, string url, DateTime startDate, DateTime endDate);
        Task<BannerDTO> GetBannerAsync();
        Task<BannerDTO> DeleteBanner(string id,string root);
        Task<BannerDTO> EditBanner(string id,  DateTime startDate, DateTime endDate);
        Task<long> GetCount();


    }
}
