﻿using PaySharp.Entities;
using PaySharp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Mappers
{
    public class AdminDTOMapper : IDtoMapper<Admin, AdminDTO>
    {
        public AdminDTO MapFrom(Admin entity)
        => new AdminDTO
        {
            Id = entity.Id,
            Password = entity.Password,
            UserName = entity.UserName,
            Role = entity.Role.Name,
        };
    }
  
}
