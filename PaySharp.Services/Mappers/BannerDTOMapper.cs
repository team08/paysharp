﻿using PaySharp.Data.Entities;
using PaySharp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PaySharp.Services.Mappers
{
    public class BannerDTOMapper : IDtoMapper<Banner, BannerDTO>
    {
        public BannerDTO MapFrom(Banner entity)
        {
            var banner = new BannerDTO()
            {
                Id = entity.Id.ToString(),
                Name = entity.Name,
                Url=entity.Url,
                ImgPath=entity.ImgPath,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate
            };

            return banner;
        }
    }
}
