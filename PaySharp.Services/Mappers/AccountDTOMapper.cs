﻿using PaySharp.Data.Entities;
using PaySharp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PaySharp.Services.Mappers
{
    public  class AccountDTOMapper:IDtoMapper<Account,AccountDTO>
    {
    
        public AccountDTO MapFrom(Account entity)
        {
            if (entity.UsersAccounts!=null)
            {
                return new AccountDTO
                {
                    Id = entity.Id,
                    AccountNumber = entity.AccountNumber,
                    Balance = entity.Balance,
                    NickName = entity.NickName,
                    ClientName = entity.Client?.Name,
                    UsersAccounts = entity.UsersAccounts.Select(ua => new UsersAccountsDTO()
                    {
                        UserId=ua.UserId,
                        AccountId=ua.AccountId
                    })
                };
            }
            else
            {
                return new AccountDTO
                {
                    Id = entity.Id,
                    AccountNumber = entity.AccountNumber,
                    Balance = entity.Balance,
                    NickName = entity.NickName,
                    ClientName = entity.Client?.Name

                };
            }

        }
        
    }
}
