﻿using PaySharp.Data.Entities;
using PaySharp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Mappers
{
    public class UsersAccountsDTOMapper : IDtoMapper<UsersAccounts, UsersAccountsDTO>
    {
        
        public UsersAccountsDTO MapFrom(UsersAccounts entity)
          => new UsersAccountsDTO
          {
              UserId = entity.UserId,
              AccountId = entity.AccountId,
          };
    }
}
