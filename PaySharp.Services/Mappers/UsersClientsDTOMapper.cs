﻿using PaySharp.Data.Entities;
using PaySharp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Mappers
{
    public  class UsersClientsDTOMapper: IDtoMapper<UsersClients, UsersClientsDTO>
    {
        private readonly IDtoMapper<User, UserDTO> userMapper;
        public UsersClientsDTOMapper(IDtoMapper<User, UserDTO> userMapper)
        {
            this.userMapper = userMapper ?? throw new ArgumentNullException(nameof(userMapper));
         
        }
        public UsersClientsDTO MapFrom(UsersClients entity)
       => new UsersClientsDTO
       {
           UserId = entity.UserId,
           ClientId = entity.ClientId,
           User =userMapper.MapFrom(entity.User)
       };
    }
}
