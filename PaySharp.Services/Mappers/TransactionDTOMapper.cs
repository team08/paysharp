﻿using PaySharp.Data.Entities;
using PaySharp.Services.DTO;
using System;

namespace PaySharp.Services.Mappers
{
    public class TransactionDTOMapper : IDtoMapper<Transaction, TransactionDTO>
    {
        private readonly IDtoMapper<Account, AccountDTO> accountMapper;

        public TransactionDTOMapper(IDtoMapper<Account, AccountDTO> accountMapper)
        {
            this.accountMapper = accountMapper ?? throw new ArgumentNullException(nameof(accountMapper));
        }

        public TransactionDTO MapFrom(Transaction entity)
        {
            if (entity.SenderAccount == null || entity.ReceiverAccount == null)
            {
                return new TransactionDTO
                {
                    Id = entity.Id,
                    SenderAccountID = entity.SenderAccountID,
                    ReceiverAccountID = entity.ReceiverAccountID,
                    Amount = entity.Amount,
                    Description = entity.Description,
                    TimeStamp = entity.TimeStamp,
                    Status = entity.Status?.StatusName,
                    StatusId = entity.StatusId
                };
            }
            else
            {
                return new TransactionDTO
                {
                    Id = entity.Id,
                    SenderAccountID = entity.SenderAccountID,
                    ReceiverAccountID = entity.ReceiverAccountID,
                    Amount = entity.Amount,
                    Description = entity.Description,
                    TimeStamp = entity.TimeStamp,
                    Status = entity.Status?.StatusName,
                    StatusId = entity.StatusId,
                    SenderAccount = accountMapper.MapFrom(entity.SenderAccount),
                    ReceiverAccount = accountMapper.MapFrom(entity.ReceiverAccount)
                };
            }

        }
        public TransactionDTO MapFromIsSend(Transaction entity)
        {
            return new TransactionDTO
            {
                Id = entity.Id,
                SenderAccountID = entity.SenderAccountID,
                ReceiverAccountID = entity.ReceiverAccountID,
                Amount = entity.Amount,
                Description = entity.Description,
                TimeStamp = entity.TimeStamp,
                Status = entity.Status?.StatusName,
                StatusId = entity.StatusId,
                SenderAccount = accountMapper.MapFrom(entity.SenderAccount),
                ReceiverAccount = accountMapper.MapFrom(entity.ReceiverAccount),
                IsSend = true

            };
        }
        public TransactionDTO MapFromIsRecieved(Transaction entity)
        {
            return new TransactionDTO
            {
                Id = entity.Id,
                SenderAccountID = entity.SenderAccountID,
                ReceiverAccountID = entity.ReceiverAccountID,
                Amount = entity.Amount,
                Description = entity.Description,
                TimeStamp = entity.TimeStamp,
                Status = entity.Status?.StatusName,
                StatusId = entity.StatusId,
                SenderAccount = accountMapper.MapFrom(entity.SenderAccount),
                ReceiverAccount = accountMapper.MapFrom(entity.ReceiverAccount),
                IsRecieved = true
            };
        }
    }
}
