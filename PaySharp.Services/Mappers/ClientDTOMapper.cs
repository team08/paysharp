﻿using PaySharp.Data.Entities;
using PaySharp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PaySharp.Services.Mappers
{
    public class ClientDTOMapper : IDtoMapper<Client, ClientDTO>
    {
        private readonly IDtoMapper<Account, AccountDTO> accountDtoMapper;
        private readonly IDtoMapper<UsersClients, UsersClientsDTO> userClientDtoMapper;

        public ClientDTOMapper(IDtoMapper<Account, AccountDTO> accountDtoMapper,
            IDtoMapper<UsersClients, UsersClientsDTO> userClientDtoMapper)
        {
            this.accountDtoMapper = accountDtoMapper ?? throw new ArgumentNullException(nameof(accountDtoMapper));
            this.userClientDtoMapper = userClientDtoMapper ?? throw new ArgumentNullException(nameof(userClientDtoMapper));
        }
        public ClientDTO MapFrom(Client entity)
        => new ClientDTO
        {
            ClientId=entity.Id,
            Name=entity.Name,
            Accounts=entity.Accounts?.Select(a=>accountDtoMapper.MapFrom(a)).ToList(),
            UsersClients=entity.UsersClients?.Select(uc=>userClientDtoMapper.MapFrom(uc)).ToList()
        };
    }
}
