﻿using Microsoft.Extensions.DependencyInjection;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Mappers
{
    public static class DtoMapperRegistration
    {
        public static IServiceCollection AddDtoMappers(this IServiceCollection services)
        {

            services.AddScoped<IDtoMapper<Client, ClientDTO>, ClientDTOMapper>();
            services.AddScoped<IDtoMapper<Account, AccountDTO>, AccountDTOMapper>();
            services.AddScoped<IDtoMapper<Admin, AdminDTO>, AdminDTOMapper>();
            services.AddScoped<IDtoMapper<User, UserDTO>, UserDTOMapper>();
            services.AddScoped<IDtoMapper<UsersClients, UsersClientsDTO>, UsersClientsDTOMapper>();
            services.AddScoped<IDtoMapper<UsersAccounts, UsersAccountsDTO>, UsersAccountsDTOMapper>();
            services.AddScoped<IDtoMapper<Transaction, TransactionDTO>, TransactionDTOMapper>();
            services.AddScoped<IDtoMapper<Banner, BannerDTO>, BannerDTOMapper>();

            return services;
        }
    }
}
