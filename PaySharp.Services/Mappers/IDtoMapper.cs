﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Mappers
{
   public interface IDtoMapper<TEntity, TDto>
    {
        TDto MapFrom(TEntity entity);
   
    }
}
