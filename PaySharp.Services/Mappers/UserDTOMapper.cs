﻿using PaySharp.Data.Entities;
using PaySharp.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PaySharp.Services.Mappers
{
    public class UserDTOMapper : IDtoMapper<User, UserDTO>
    {
        
        public UserDTO MapFrom(User entity)
        => new UserDTO
        {
            UserId = entity.Id,
            Name = entity.Name,
            Password = entity.Password,
            UserName = entity.UserName,
            Role = entity.Role?.Name           
        };
    }
   
}
