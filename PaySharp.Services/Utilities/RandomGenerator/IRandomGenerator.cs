﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Utilities.RandomGenerator
{
    public interface IRandomGenerator
    {
        string GenerateNumber(int min, int max,int amount);

    }
}
