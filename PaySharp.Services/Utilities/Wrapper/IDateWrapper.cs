﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Utilities.Wrapper
{
    public interface IDateWrapper
    {
        DateTime Now();
    }
}
