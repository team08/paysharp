﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Exceptions
{
    public class TokenExpirationException : Exception
    {
        public TokenExpirationException(string message) : base(message)
        {

        }
    }
}
