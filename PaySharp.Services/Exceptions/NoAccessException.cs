﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.Exceptions
{
   
        public class NoAccessException : Exception
        {
            public NoAccessException(string message) : base(message)
            {

            }
        }
    
}
