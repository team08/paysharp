﻿using PaySharp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.DTO
{
    public class ClientDTO
    {
        public long ClientId { get; set; }
      
        public string Name { get; set; }

        public ICollection<AccountDTO> Accounts { get; set; }

        public ICollection<UsersClientsDTO> UsersClients { get; set; }
    }
}
