﻿using PaySharp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.DTO
{
    public class UsersClientsDTO
    {
        public long UserId { get; set; }
        public UserDTO User { get; set; }

        public long ClientId { get; set; }
        public ClientDTO Client { get; set; }
    }
}
