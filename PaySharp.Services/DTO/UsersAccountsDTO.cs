﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.DTO
{
   public class UsersAccountsDTO
    {
       
            public long UserId { get; set; }
            public UserDTO User { get; set; }

            public long AccountId { get; set; }
            public AccountDTO Account { get; set; }
        
    }
}
