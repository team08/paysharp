﻿using PaySharp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.DTO
{
    public class AdminDTO
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }
    }
}
