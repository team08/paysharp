﻿using PaySharp.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.DTO
{
   public class UserDTO
    {
       
        public long UserId { get; set; }

        
        public string Name { get; set; }

        public string UserName { get; set; }

      
        public string Password { get; set; }

        public string Role { get; set; }

        public ICollection<ClientDTO> Clients { get; set; }
        public ICollection<AccountDTO> Accounts { get; set; }
       
    }
}
