﻿using PaySharp.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PaySharp.Services.DTO
{
   public class AccountDTO
    {
        
        public long Id { get; set; }

        public string AccountNumber { get; set; }

        public string NickName { get; set; }

        public decimal Balance { get; set; }

        public bool IsAddedToUser { get; set; }
        public string ClientName { get; set; }

        public IEnumerable<UsersAccountsDTO> UsersAccounts { get; set; }
       
    }
}
