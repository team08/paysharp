﻿using System;

namespace PaySharp.Services.DTO
{
    public class BannerDTO
    {

        public string Id { get; set; }

        public string ImgPath { get; set; }


        public string Name { get; set; }


        public string Url { get; set; }


        public DateTime StartDate { get; set; }


        public DateTime EndDate { get; set; }
    }
}
