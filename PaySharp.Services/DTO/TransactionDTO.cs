﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Services.DTO
{

    public class TransactionDTO
    {
      
        public long Id { get; set; }

        public long SenderAccountID { get; set; }

        public AccountDTO SenderAccount { get; set; }


        public long ReceiverAccountID { get; set; }
       
        public AccountDTO ReceiverAccount { get; set; }

       
        public string Description { get; set; }

       
        public decimal Amount { get; set; }


        public DateTime TimeStamp { get; set; }

        public bool IsSend { get; set; }
        public bool IsRecieved { get; set; }
        
        public string Status { get; set; }
        public long StatusId { get; set; }
    }
}
