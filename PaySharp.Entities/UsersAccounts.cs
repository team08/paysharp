﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PaySharp.Data.Entities
{
    public class UsersAccounts
    {
        
        public long UserId { get; set; }
        public User User { get; set; }

        
        public long AccountId { get; set; }
        public Account Account { get; set; }
    }
}
