﻿using PaySharp.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Data.Entities
{
    public class Role
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public ICollection<User> Users { get; set; }
        public ICollection<Admin> Admins { get; set; }
    }
}
