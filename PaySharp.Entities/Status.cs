﻿using PaySharp.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PaySharp.Entities
{
    public class Status
    {
        public long Id { get; set; }

        [Required]
        public string StatusName { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
    }
}
