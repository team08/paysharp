﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Data.Entities
{
    public class UsersClients
    {
        public long UserId { get; set; }
        public User User { get; set; }

        public long ClientId { get; set; }
        public Client Client { get; set; }
    }
}
