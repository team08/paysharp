﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Tests;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.AccountServiceTests
{
    [TestClass]
    public class GetAccountAsync_Should_ById
    {
        [TestMethod]
        public async Task ThereIsAccount_ReturnAccount()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(ThereIsAccount_ReturnAccount));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "salary",
                    ClientId = 1
                };
                var client = new Client() { Name = "Masters", Id = 1 };

                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.GetAccountAsync(1);

                Assert.IsInstanceOfType(res, typeof(AccountDTO));
                Assert.AreEqual(res.AccountNumber, "1234567890");
                Assert.AreEqual(res.Balance, 10000);
                Assert.AreEqual(res.NickName, "salary");
                Assert.AreEqual(res.ClientName, "Masters");
            }
        }

        [TestMethod]
        public async Task ThereIsNoAccount_ThrowException()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(ThereIsNoAccount_ThrowException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such account!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_ACCOUNT")).Returns(confSection.Object);

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "salary",
                    ClientId = 1
                };
                var client = new Client() { Name = "Masters", Id = 1 };

                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.GetAccountAsync(2));
                Assert.AreEqual(ex.Message, "There is no such account!");
            }
        }
    }
}
