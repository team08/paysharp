﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.AccountServiceTests
{
    [TestClass]
    public class CreateAccountAsync_Should
    {
        [TestMethod]
        public async Task CreateAccount_ReturnAccountDTO()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(CreateAccount_ReturnAccountDTO));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("10000");
            configuration.Setup(c => c.GetSection("GlobalConstants:Balance")).Returns(confSection.Object);
            randomGenerator.SetupSequence(x => x.GenerateNumber(0, 9,10))
                .Returns("1234567890")
                .Returns("1234567899");
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var account = new Account()
                {
                    Id = 200,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "salary",
                    ClientId = 1
                };
                var client = new Client() { Name = "Masters", Id = 1 };
                arrangeContext.Accounts.Add(account);
                arrangeContext.Clients.Add(client);
                await arrangeContext.SaveChangesAsync();
                
                   
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.CreateAccountAsync(1);

                Assert.IsInstanceOfType(res, typeof(AccountDTO));
                Assert.AreEqual(res.AccountNumber, "1234567899");
                Assert.AreEqual(res.Balance, 10000);
                Assert.AreEqual(res.NickName, "1234567899");
                Assert.AreEqual(assertContex.Accounts.Count(), 2);
                Assert.AreEqual(assertContex.Accounts.Last().AccountNumber, "1234567899");
                Assert.AreEqual(assertContex.Accounts.Last().Balance, 10000);
                Assert.AreEqual(assertContex.Accounts.Last().NickName, "1234567899");
                Assert.AreEqual(assertContex.Accounts.Last().ClientId, 1);
            }
        }
    }
}
