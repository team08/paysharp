﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.AccountServiceTests
{
    [TestClass]
    public class GetUserAccountsAsync_Should
    {
        [TestMethod]
        public async Task GetUserAccounts()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(GetUserAccounts));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "1234567890",
                    ClientId = 1
                };
                var account2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1234567891",
                    Balance = 10000,
                    NickName = "1234567891",
                    ClientId = 1
                };
                var client = new Client() { Name = "Masters", Id = 1 };
                var userAccount = new UsersAccounts() { UserId = 1, AccountId = 1 };
                var userAccount2 = new UsersAccounts() { UserId = 1, AccountId = 2 };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.UsersAccounts.Add(userAccount);
                arrangeContext.UsersAccounts.Add(userAccount2);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.GetUserAccountsAsync(1);

                Assert.AreEqual(res.Count(), 2);
                
            }
        }

        [TestMethod]
        public async Task GetUserAccount()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(GetUserAccount));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "1234567890",
                    ClientId = 1
                };

                var client = new Client() { Name = "Masters", Id = 1 };
                var userAccount = new UsersAccounts() { UserId = 1, AccountId = 1 };

                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                arrangeContext.UsersAccounts.Add(userAccount);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.GetUserAccountsAsync(1);

                Assert.AreEqual(res.Count(), 1);
                Assert.AreEqual(res[0].AccountNumber, "1234567890");
                Assert.AreEqual(res[0].NickName, "1234567890");
                Assert.AreEqual(res[0].Id, 1);

            }
        }

        [TestMethod]
        public async Task GetNoUserAccounts()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(GetNoUserAccounts));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "1234567890",
                    ClientId = 1
                };

                var client = new Client() { Name = "Masters", Id = 1 };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.GetUserAccountsAsync(1);

                Assert.AreEqual(res.Count(), 0);
                

            }
        }
    }
}
