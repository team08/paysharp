﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.AccountServiceTests
{
    [TestClass]
    public class GetUserAccountsIdsAsync_Should
    {
        [TestMethod]
        public async Task GetAccountIds()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(GetAccountIds));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "1234567890",
                    ClientId = 1
                };
                var account2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1234567891",
                    Balance = 10000,
                    NickName = "1234567891",
                    ClientId = 1
                };
                var client = new Client() { Name = "Masters", Id = 1 };
                var userAccount = new UsersAccounts() { UserId = 1, AccountId = 1 };
                var userAccount2 = new UsersAccounts() { UserId = 1, AccountId = 2 };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.UsersAccounts.Add(userAccount);
                arrangeContext.UsersAccounts.Add(userAccount2);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.GetUserAccountsIdsAsync(1);

                Assert.AreEqual(res.Count(), 2);
                Assert.IsTrue(res.Contains(1));
                Assert.IsTrue(res.Contains(2));
            }
        }
        [TestMethod]
        public async Task GetNoAccountIds()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(GetAccountIds));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "1234567890",
                    ClientId = 1
                };
               
                var client = new Client() { Name = "Masters", Id = 1 };
             
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.GetUserAccountsIdsAsync(1);

                Assert.AreEqual(res.Count(), 0);
            }
        }
    }
}
