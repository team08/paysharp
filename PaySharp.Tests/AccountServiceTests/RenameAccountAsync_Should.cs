﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.AccountServiceTests
{
    [TestClass]
    public class RenameAccountAsync_Should
    {
        [TestMethod]
        public async Task RenameAccountCorrectly()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(RenameAccountCorrectly));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {

                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "1234567890",
                    ClientId = 1
                };
               
                var client = new Client() { Name = "Masters", Id = 1 };

                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.RenameAccountAsync(1,"NewName");

                Assert.IsInstanceOfType(res, typeof(AccountDTO));
                Assert.AreEqual(res.Id, 1);
                Assert.AreEqual(res.NickName, "NewName");
                Assert.AreEqual(assertContex.Accounts.First().Id, 1);
                Assert.AreEqual(assertContex.Accounts.First().NickName, "NewName");
            }
        }
    }
}
