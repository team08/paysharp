﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Tests;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.AccountServiceTests
{
    [TestClass]
    public class GetAccountAsync_Should_ByAccountNumber
    {
        [TestMethod]
        public async Task ThereIsAccount_ReturnAccount()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(ThereIsAccount_ReturnAccount));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance=10000,
                    NickName="salary",
                    ClientId=1
                };
                var client = new Client() { Name = "Masters", Id = 1 };

                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.GetAccountAsync("1234567890");

                Assert.AreEqual(res.AccountNumber,"1234567890");
                Assert.AreEqual(res.Balance, 10000);
                Assert.AreEqual(res.NickName, "salary");
                Assert.AreEqual(res.ClientId, 1);
                Assert.AreEqual(res.Client.Name, "Masters");
            }
        }

        [TestMethod]
        public async Task ThereIsNoAccount_ReturnNull()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(ThereIsNoAccount_ReturnNull));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "salary",
                    ClientId = 1
                };
                var client = new Client() { Name = "Masters", Id = 1 };

                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.GetAccountAsync("1234567899");

                Assert.IsNull(res);
            }
        }
    }
}
