﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Tests;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.AccountServiceTests
{
    [TestClass]
    public class FindAccountsContainingAsync_Should
    {
        [TestMethod]
        public async Task TakeFiveAccounts()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(TakeFiveAccounts));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
              
                var account1 = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "1234567890",
                    ClientId = 1
                };
                var account2 = new Account()
                { Id = 2,
                    AccountNumber = "1234567891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567891"
                };
                var account3 = new Account()
                {
                    Id = 3,
                    AccountNumber = "1234567892",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567892"
                };
                var account4 = new Account()
                {
                    Id = 4,
                    AccountNumber = "1234567893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567893"
                };
                var account5 = new Account()
                {
                    Id = 5,
                    AccountNumber = "1234567894",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567894"
                };
                var account6 = new Account()
                {
                    Id = 6,
                    AccountNumber = "1234567895",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567895"
                };
                
                var client = new Client() { Name = "Masters", Id = 1 };
               
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account1);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.Accounts.Add(account4);
                arrangeContext.Accounts.Add(account5);
                arrangeContext.Accounts.Add(account6);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.FindAccountsContainingAsync("123");

                Assert.IsInstanceOfType(res, typeof(IList<AccountDTO>));
                Assert.AreEqual(res.Count(), 5);

            }
        }

        [TestMethod]
        public async Task TakeOneAccount()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(TakeFiveAccounts));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {

                var account1 = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "1234567890",
                    ClientId = 1
                };
                var account2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1234567891",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567891"
                };
                var account3 = new Account()
                {
                    Id = 3,
                    AccountNumber = "1234567892",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567892"
                };
                var account4 = new Account()
                {
                    Id = 4,
                    AccountNumber = "1234567893",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567893"
                };
                var account5 = new Account()
                {
                    Id = 5,
                    AccountNumber = "1234567894",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567894"
                };
                var account6 = new Account()
                {
                    Id = 6,
                    AccountNumber = "1234567895",
                    Balance = 10000,
                    ClientId = 1,
                    NickName = "1234567895"
                };

                var client = new Client() { Name = "Masters", Id = 1 };

                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account1);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.Accounts.Add(account4);
                arrangeContext.Accounts.Add(account5);
                arrangeContext.Accounts.Add(account6);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.FindAccountsContainingAsync("1234567890");

                Assert.IsInstanceOfType(res, typeof(IList<AccountDTO>));
                Assert.AreEqual(res.Count(), 1);
                Assert.AreEqual(res[0].AccountNumber, "1234567890");
            }
        }
    }
}
