﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.AccountServiceTests
{
    [TestClass]
    public class RemoveAccountFromUserAsync_Should
    {
        [TestMethod]
        public async Task AccountDoesntExist_ThrowException()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(AccountDoesntExist_ThrowException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such account!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_ACCOUNT")).Returns(confSection.Object);

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "salary",
                    ClientId = 1
                };
                var client = new Client() { Name = "Masters", Id = 1 };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                   async () => await sut.RemoveAccountFromUserAsync(2, 2));
                Assert.AreEqual(ex.Message, "There is no such account!");

            }
        }
        [TestMethod]
        public async Task UserDoesntExist_ThrowException()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(UserDoesntExist_ThrowException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such user!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_USER")).Returns(confSection.Object);

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "salary",
                    ClientId = 1
                };
                var client = new Client() { Name = "Masters", Id = 1 };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                   async () => await sut.RemoveAccountFromUserAsync(2, 1));
                Assert.AreEqual(ex.Message, "There is no such user!");

            }
        }
        [TestMethod]
        public async Task AccountIsNotAddedToUser_ThrowException()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(AccountIsNotAddedToUser_ThrowException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("User is not connected to this account");
            configuration.Setup(c => c.GetSection("GlobalConstants:USER_NO_ACCOUNT")).Returns(confSection.Object);

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "salary",
                    ClientId = 1
                };
                var client = new Client() { Name = "Masters", Id = 1 };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                   async () => await sut.RemoveAccountFromUserAsync(1, 1));
                Assert.AreEqual(ex.Message, "User is not connected to this account");

            }
        }
        [TestMethod]
        public async Task AddingAccountToUserCorrectly()
        {
            //arrange
            var accountMapper = new AccountDTOMapper();
            var randomGenerator = new Mock<IRandomGenerator>();
            var options = TestUtils.GetOptions(nameof(AddingAccountToUserCorrectly));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "salary",
                    ClientId = 1
                };
                var userAccount = new UsersAccounts() { UserId = 1, AccountId = 1 };
                var client = new Client() { Name = "Masters", Id = 1 };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Accounts.Add(account);
                arrangeContext.UsersAccounts.Add(userAccount);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new AccountService(assertContex,
                    configuration.Object, accountMapper, randomGenerator.Object);

                var res = await sut.RemoveAccountFromUserAsync(1, 1);

                Assert.IsTrue(res);
                Assert.AreEqual(assertContex.UsersAccounts.Count(), 0);
                
            }
        }
    }
}
