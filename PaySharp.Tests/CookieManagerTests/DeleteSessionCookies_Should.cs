﻿using Microsoft.AspNetCore.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services;

namespace PaySharp.ServiceTests.CookieManagerTests
{
    [TestClass]
    public class DeleteSessionCookies_Should
    {
        [TestMethod]
        public void DeleteSessionCookies_ShouldCall_ResponseCookiesDelete()
        {
            var contextMock = new Mock<IHttpContextAccessor>();

            contextMock.Setup(x => x.HttpContext
                                    .Response
                                    .Cookies
                                    .Delete("SecurityToken"))
                                    .Verifiable();

            var sut = new CookieManager(contextMock.Object);

            sut.DeleteSessionCookies();

            contextMock
                .Verify(x => x.HttpContext
                                .Response
                                .Cookies
                                .Delete("SecurityToken"),
                                        Times.Once);
        }
    }
}
