﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.ClientServiceTests
{
    [TestClass]
    public class GetClientAsyncById_Should
    {
        [TestMethod]
        public async Task IfThereIsNotClient_ThrowException()
        {
            //arrange
            var userMapper = new UserDTOMapper();
            var accountsMapper = new AccountDTOMapper();
            var userClientsMapper = new UsersClientsDTOMapper(userMapper);
            var clientMapper = new ClientDTOMapper(accountsMapper,userClientsMapper);
            var options = TestUtils.GetOptions(nameof(IfThereIsNotClient_ThrowException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such client!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_CLIENT")).Returns(confSection.Object);
            
           
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client()
                {
                    Id = 1,
                    Name = "name"
                };
                arrangeContext.Clients.Add(client);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new ClientService(assertContext, clientMapper, configuration.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                   async () => await sut.GetClientAsync(2));
                Assert.AreEqual(ex.Message, "There is no such client!");
               
            }
        }
        [TestMethod]
        public async Task ReturnsClientDTO()
        {
            //arrange
            var userMapper = new UserDTOMapper();
            var accountsMapper = new AccountDTOMapper();
            var userClientsMapper = new UsersClientsDTOMapper(userMapper);
            var clientMapper = new ClientDTOMapper(accountsMapper, userClientsMapper);
            var options = TestUtils.GetOptions(nameof(IfThereIsNotClient_ThrowException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such client!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_CLIENT")).Returns(confSection.Object);


            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client()
                {
                    Id = 1,
                    Name = "name"
                };
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var userClient = new UsersClients() { ClientId = 1, UserId = 1 };
                var account = new Account() { AccountNumber = "1234567890", Id = 1, ClientId = 1 };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.UsersClients.Add(userClient);
                arrangeContext.Accounts.Add(account);
               
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var context = new PaySharpDBContext(options))
            {
                var sut = new ClientService(context, clientMapper, configuration.Object);
                var res = await sut.GetClientAsync(1);
                Assert.IsInstanceOfType(res, typeof(ClientDTO));
                Assert.AreEqual(res.Name, "name");
                Assert.AreEqual(res.UsersClients.Count, 1);
                Assert.AreEqual(res.Accounts.Count, 1);
                Assert.AreEqual(res.Accounts.First().AccountNumber, "1234567890");
            }
        }
    }
}
