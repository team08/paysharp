﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.ClientServiceTests
{
    [TestClass]
    public class CreateClientAsync_Should
    {
        [TestMethod]
        public async Task IfClientExist_ThrowsException()
        {
            
            var userMapper = new UserDTOMapper();
            var accountMapper = new AccountDTOMapper();
            var userClientMapper = new UsersClientsDTOMapper(userMapper);
            var clientMapper = new ClientDTOMapper(accountMapper, userClientMapper);
            var options = TestUtils.GetOptions(nameof(IfClientExist_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("Client with this name already exists!");
            configuration.Setup(c => c.GetSection("GlobalConstants:CLIENT_EXISTS")).Returns(confSection.Object);
           
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client()
                {
                    Id = 1,
                    Name = "name"
                };
                arrangeContext.Clients.Add(client);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new ClientService(assertContext, clientMapper, configuration.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityAlreadyExistsException>(
                    async () => await sut.CreateClientAsync("name"));
                Assert.AreEqual(ex.Message, "Client with this name already exists!");
            }
        }

        [TestMethod]
        public async Task IfNewClientIsCreated_ReturnClientDTO()
        {

            var userMapper = new UserDTOMapper();
            var accountMapper = new AccountDTOMapper();
            var userClientMapper = new UsersClientsDTOMapper(userMapper);
            var clientMapper = new ClientDTOMapper(accountMapper, userClientMapper);
            var options = TestUtils.GetOptions(nameof(IfClientExist_ThrowsException));

            var configuration = new Mock<IConfiguration>();

            using (var context = new PaySharpDBContext(options))
            {
                var sut = new ClientService(context, clientMapper, configuration.Object);
                var res = await sut.CreateClientAsync("name");
                Assert.IsInstanceOfType(res, typeof(ClientDTO));
                Assert.AreEqual(context.Clients.Count(), 1);
                Assert.AreEqual(res.Name, "name");
            }
        }
    }
}
