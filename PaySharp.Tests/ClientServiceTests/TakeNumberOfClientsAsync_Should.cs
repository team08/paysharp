﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.ClientServiceTests
{
    [TestClass]
    public class TakeNumberOfClientsAsync_Should
    {
        [TestMethod]
        public async Task TakeTwoUsersFromFirstPage()
        {
            //arrange
            var userMapper = new UserDTOMapper();
            var accountMapper = new AccountDTOMapper();
            var userClientMapper = new UsersClientsDTOMapper(userMapper);
            var clientMapper = new ClientDTOMapper(accountMapper, userClientMapper);
            var options = TestUtils.GetOptions(nameof(TakeTwoUsersFromFirstPage));

            var configuration = new Mock<IConfiguration>();
           
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client() { Id = 1, Name = "name" };
                var client2 = new Client() { Id = 2, Name = "name2" };

                arrangeContext.Clients.Add(client);
                arrangeContext.Clients.Add(client2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new ClientService(assertContext, clientMapper, configuration.Object);
                var res = await sut.TakeNumberOfClientsAsync(1, 5);

                Assert.IsInstanceOfType(res, typeof(List<ClientDTO>));
                Assert.AreEqual(res.Count, 2);
            }
        }

        [TestMethod]
        public async Task TakeTwoUsersFromSecondPage()
        {
            //arrange
            var userMapper = new UserDTOMapper();
            var accountMapper = new AccountDTOMapper();
            var userClientMapper = new UsersClientsDTOMapper(userMapper);
            var clientMapper = new ClientDTOMapper(accountMapper, userClientMapper);
            var options = TestUtils.GetOptions(nameof(TakeTwoUsersFromSecondPage));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client() { Id = 1, Name = "name" };
                var client2 = new Client() { Id = 2, Name = "name2" };
                var client3 = new Client() { Id = 3, Name = "name3" };
                var client4 = new Client() { Id = 4, Name = "name4" };

                arrangeContext.Clients.Add(client);
                arrangeContext.Clients.Add(client2);
                arrangeContext.Clients.Add(client3);
                arrangeContext.Clients.Add(client4);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new ClientService(assertContext, clientMapper, configuration.Object);
                var res = await sut.TakeNumberOfClientsAsync(2, 2);

                Assert.IsInstanceOfType(res, typeof(List<ClientDTO>));
                Assert.AreEqual(res.Count, 2);
            }
        }
    }
}
