﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace PaySharp.ServiceTests.ClientServiceTests
{
    [TestClass]
   public class FindClientContainingAsync_Should
    {
        [TestMethod]
        public async Task GetListOfFiveClientNames()
        {
            //arrange
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(GetListOfFiveClientNames));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client() { Id = 1, Name = "name" };
                var client2 = new Client() { Id = 2, Name = "name2" };
                var client3 = new Client() { Id = 3, Name = "name3" };
                var client4 = new Client() { Id = 4, Name = "name4" };
                var client5 = new Client() { Id = 5, Name = "name5" };
                var client6 = new Client() { Id = 6, Name = "name6" };

                arrangeContext.Clients.Add(client);
                arrangeContext.Clients.Add(client2);
                arrangeContext.Clients.Add(client3);
                arrangeContext.Clients.Add(client4);
                arrangeContext.Clients.Add(client5);
                arrangeContext.Clients.Add(client6);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new ClientService(assertContext, clientMapper.Object, configuration.Object);
                var res = await sut.FindClientContainingAsync("nam");

                Assert.IsInstanceOfType(res, typeof(IList<string>));
                Assert.AreEqual(res.Count, 5);
            }
        }
        [TestMethod]
        public async Task GetListOfOneClientName()
        {
            //arrange
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(GetListOfFiveClientNames));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client() { Id = 1, Name = "name" };
                var client2 = new Client() { Id = 2, Name = "name2" };
                var client3 = new Client() { Id = 3, Name = "name3" };
                var client4 = new Client() { Id = 4, Name = "name4" };
                var client5 = new Client() { Id = 5, Name = "name5" };
                var client6 = new Client() { Id = 6, Name = "name6" };

                arrangeContext.Clients.Add(client);
                arrangeContext.Clients.Add(client2);
                arrangeContext.Clients.Add(client3);
                arrangeContext.Clients.Add(client4);
                arrangeContext.Clients.Add(client5);
                arrangeContext.Clients.Add(client6);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new ClientService(assertContext, clientMapper.Object, configuration.Object);
                var res = await sut.FindClientContainingAsync("name3");

                Assert.IsInstanceOfType(res, typeof(IList<string>));
                Assert.AreEqual(res.Count, 1);
                Assert.AreEqual(res[0], "name3");
            }
        }
    }
}
