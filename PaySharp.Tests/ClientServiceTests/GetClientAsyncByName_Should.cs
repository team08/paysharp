﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.ClientServiceTests
{
    [TestClass]
    public class GetClientAsyncByName_Should
    {
        [TestMethod]
        public async Task ReturnClient()
        {
            //arrange
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(ReturnClient));

            var configuration = new Mock<IConfiguration>();
            
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client()
                {
                    Id = 1,
                    Name = "name"
                };
                arrangeContext.Clients.Add(client);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new ClientService(assertContext, clientMapper.Object, configuration.Object);
                var res = await sut.GetClientByNameAsync("name");
                Assert.IsInstanceOfType(res, typeof(Client));
                Assert.AreEqual(res.Name, "name");
            }
        }
        [TestMethod]
        public async Task ReturnNoClient()
        {
            //arrange
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(ReturnClient));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client()
                {
                    Id = 1,
                    Name = "name"
                };
                arrangeContext.Clients.Add(client);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new ClientService(assertContext, clientMapper.Object, configuration.Object);
                var res = await sut.GetClientByNameAsync("namenew");
                Assert.IsNull(res);
                
            }
        }
    }
}
