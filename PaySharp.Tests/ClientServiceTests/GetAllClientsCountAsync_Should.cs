﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.ClientServiceTests
{
    [TestClass]
    public class GetAllClientsCountAsync_Should
    {
        [TestMethod]
        public async Task GetClientsCount()
        {
            //arrange
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(GetClientsCount));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var client = new Client() { Id = 1, Name = "name" };
                var client2 = new Client() { Id = 2, Name = "name2" };

                arrangeContext.Clients.Add(client);
                arrangeContext.Clients.Add(client2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new ClientService(assertContext, clientMapper.Object, configuration.Object);
                var res = await sut.GetAllClientsCountAsync();

                Assert.AreEqual(res, 2);
            }
        }
    }
}
