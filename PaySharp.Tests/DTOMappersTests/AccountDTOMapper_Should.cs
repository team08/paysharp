﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaySharp.Data.Entities;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using System.Collections.Generic;
using System.Linq;

namespace PaySharp.ServiceTests.DTOMappersTests
{
    [TestClass]
    public class AccountDTOMapper_Should
    {
        [TestMethod]
        public void IfUserAccountIsNull_ReturnsDTO()
        {
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "test",
                Client = new Client() { Id = 1, Name = "Masters" }
            };
            var sut = new AccountDTOMapper();
            var res = sut.MapFrom(account);

            Assert.IsInstanceOfType(res, typeof(AccountDTO));
            Assert.AreEqual(res.AccountNumber, "1234567890");
            Assert.AreEqual(res.Balance, 10000);
            Assert.AreEqual(res.NickName, "test");
            Assert.AreEqual(res.ClientName, "Masters");
        }
        [TestMethod]
        public void IfUserAccountIsNotNull_ReturnsDTO()
        {
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "test",
                Client = new Client() { Id = 1, Name = "Masters" },
                UsersAccounts =new List<UsersAccounts>()
                { new UsersAccounts { UserId = 1, AccountId = 1 } }
            };
            var sut = new AccountDTOMapper();
            var res = sut.MapFrom(account);

            Assert.IsInstanceOfType(res, typeof(AccountDTO));
            Assert.AreEqual(res.AccountNumber, "1234567890");
            Assert.AreEqual(res.Balance, 10000);
            Assert.AreEqual(res.NickName, "test");
            Assert.AreEqual(res.ClientName, "Masters");
            Assert.AreEqual(res.UsersAccounts.Count(), 1);
        }
    }
}
