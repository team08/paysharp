﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.Wrapper;
using System;
using System.Collections.Generic;

namespace PaySharp.ServiceTests.DTOMappersTests
{
    [TestClass]
    public class TransactionDTOMapper_Should
    {
        [TestMethod]
        public void IfSenderAccountIsNull_ReturnsDTO()
        {
            var accountMapper = new AccountDTOMapper();
            var date = DateTime.Now;
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "test",
                Client = new Client() { Id = 1, Name = "Masters" },
                UsersAccounts = new List<UsersAccounts>()
                { new UsersAccounts { UserId = 1, AccountId = 1 } }
            };
            var transaction = new Transaction()
            {
                Id = 1,
                Amount = 1000,
                Description = "test",
                SenderAccountID = 1,
                SenderAccount=null,
                ReceiverAccount=account,
                ReceiverAccountID = 1,
                StatusId = 1,
                TimeStamp = date,
                Status=new Status() { Id=1,StatusName="sent"}
            };
            var sut = new TransactionDTOMapper(accountMapper);
            var res = sut.MapFrom(transaction);

            Assert.IsInstanceOfType(res, typeof(TransactionDTO));
            Assert.AreEqual(res.Id, 1);
            Assert.AreEqual(res.Amount, 1000);
            Assert.AreEqual(res.Description, "test");
            Assert.AreEqual(res.TimeStamp, date);
            Assert.AreEqual(res.StatusId, 1);
            Assert.AreEqual(res.Status, "sent");
            Assert.AreEqual(res.SenderAccountID, 1);
            Assert.AreEqual(res.ReceiverAccountID, 1);
        }
        [TestMethod]
        public void IfRecieverAccountIsNull_ReturnsDTO()
        {
            var accountMapper = new AccountDTOMapper();
            var date = DateTime.Now;
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "test",
                Client = new Client() { Id = 1, Name = "Masters" },
                UsersAccounts = new List<UsersAccounts>()
                { new UsersAccounts { UserId = 1, AccountId = 1 } }
            };
            var transaction = new Transaction()
            {
                Id = 1,
                Amount = 1000,
                Description = "test",
                SenderAccountID = 1,
                SenderAccount = account,
                ReceiverAccount = null,
                ReceiverAccountID = 1,
                StatusId = 1,
                TimeStamp = date,
                Status = new Status() { Id = 1, StatusName = "sent" }
            };
            var sut = new TransactionDTOMapper(accountMapper);
            var res = sut.MapFrom(transaction);

            Assert.IsInstanceOfType(res, typeof(TransactionDTO));
            Assert.AreEqual(res.Id, 1);
            Assert.AreEqual(res.Amount, 1000);
            Assert.AreEqual(res.Description, "test");
            Assert.AreEqual(res.TimeStamp, date);
            Assert.AreEqual(res.StatusId, 1);
            Assert.AreEqual(res.Status, "sent");
            Assert.AreEqual(res.SenderAccountID, 1);
            Assert.AreEqual(res.ReceiverAccountID, 1);
        }
        [TestMethod]
        public void IfAccountsAreNotNull_ReturnsDTO()
        {
            var accountMapper = new AccountDTOMapper();
            var date = DateTime.Now;
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "test",
                Client = new Client() { Id = 1, Name = "Masters" },
                UsersAccounts = new List<UsersAccounts>()
                { new UsersAccounts { UserId = 1, AccountId = 1 } }
            };
            var transaction = new Transaction()
            {
                Id = 1,
                Amount = 1000,
                Description = "test",
                SenderAccountID = 1,
                SenderAccount = account,
                ReceiverAccount = account,
                ReceiverAccountID = 1,
                StatusId = 1,
                TimeStamp = date,
                Status = new Status() { Id = 1, StatusName = "sent" }
            };
            var sut = new TransactionDTOMapper(accountMapper);
            var res = sut.MapFrom(transaction);

            Assert.IsInstanceOfType(res, typeof(TransactionDTO));
            Assert.AreEqual(res.Id, 1);
            Assert.AreEqual(res.Amount, 1000);
            Assert.AreEqual(res.Description, "test");
            Assert.AreEqual(res.TimeStamp, date);
            Assert.AreEqual(res.StatusId, 1);
            Assert.AreEqual(res.Status, "sent");
            Assert.AreEqual(res.SenderAccountID, 1);
            Assert.AreEqual(res.SenderAccount.Id, 1);
            Assert.AreEqual(res.SenderAccount.ClientName, "Masters");
            Assert.AreEqual(res.SenderAccount.NickName, "test");
            Assert.AreEqual(res.SenderAccount.Balance, 10000);
            Assert.AreEqual(res.SenderAccount.AccountNumber, "1234567890");
            Assert.AreEqual(res.ReceiverAccountID, 1);
            Assert.AreEqual(res.ReceiverAccount.Id, 1);
            Assert.AreEqual(res.ReceiverAccount.ClientName, "Masters");
            Assert.AreEqual(res.ReceiverAccount.NickName, "test");
            Assert.AreEqual(res.ReceiverAccount.Balance, 10000);
            Assert.AreEqual(res.ReceiverAccount.AccountNumber, "1234567890");
        }
    }
}
