﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Services.Utilities.Wrapper;
using System;
using System.Numerics;

namespace PaySharp.ServiceTests.RandomGeneratorTests
{
    [TestClass]
    public class GenerateNumber_Should
    {
        [TestMethod]
        public void ThrowIfInvalidParamsPassed()
        {
            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("Min border cannot be greater than max border");
            configuration.Setup(c => c.GetSection("GlobalConstants:MIN_MAX")).Returns(confSection.Object);

            var sut = new RandomGenerator(configuration.Object);
            
            var ex = Assert.ThrowsException<ArgumentOutOfRangeException>(
                  () => sut.GenerateNumber(5, 3,10));
            Assert.AreEqual(ex.Message, "Min border cannot be greater than max border");
        }

        [TestMethod]
        public void ThrowIfAmmounNegativeOrZero()
        {
            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("Amount cannot be negative");
            configuration.Setup(c => c.GetSection("GlobalConstants:AMOUNT_NEGATIVE"))
                .Returns(confSection.Object);

            var sut = new RandomGenerator(configuration.Object);

            var ex = Assert.ThrowsException<ArgumentOutOfRangeException>(
                  () => sut.GenerateNumber(1, 3, -1));
            Assert.AreEqual(ex.Message, "Amount cannot be negative");
        }


        [TestMethod]
        public void ReturnsTenNumberString()
        {
            var configuration = new Mock<IConfiguration>();
            var sut = new RandomGenerator(configuration.Object);
            var res = sut.GenerateNumber(0, 9,10);

            Assert.AreEqual(res.Length,10);
            Assert.IsInstanceOfType(BigInteger.Parse(res),typeof(BigInteger));
           

        }

        [TestMethod]
        public void ReturnsMin()
        {
            var configuration = new Mock<IConfiguration>();
            var sut = new RandomGenerator(configuration.Object);
            var res = sut.GenerateNumber(9, 9,1);

            Assert.AreEqual(res, "9");
            Assert.IsInstanceOfType(BigInteger.Parse(res), typeof(BigInteger));


        }
    }
}
