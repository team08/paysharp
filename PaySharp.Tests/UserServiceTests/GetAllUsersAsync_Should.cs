﻿
//using Microsoft.Extensions.Configuration;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using PaySharp.Data.DataContext;
//using PaySharp.Data.Entities;
//using PaySharp.Entities;
//using PaySharp.Services;
//using PaySharp.Services.DTO;
//using PaySharp.Services.Mappers;
//using PaySharp.Tests;
//using System.Collections.Generic;
//using System.Threading.Tasks;

//namespace PaySharp.ServiceTests.UserServiceTests
//{
//    [TestClass]
//    public class GetAllUsersAsync_Should
//    {
//        [TestMethod]
//        public async Task ReturnListAllUserDTO()
//        {
//            //arrange
//            var hashPassword = new Mock<IHashPassword>();
//            var userMapper = new UserDTOMapper();
//            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
//            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
//            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
//            var options = TestUtils.GetOptions(nameof(ReturnListAllUserDTO));

//            var configuration = new Mock<IConfiguration>();

//            using (var arrangeContext = new PaySharpDBContext(options))
//            {
//                var user = new User()
//                {
//                    Name = "name",
//                    UserName = "username",
//                    Password = "password",
//                    RoleId = 2
//                };
//                var user2 = new User()
//                {
//                    Name = "name",
//                    UserName = "username",
//                    Password = "password",
//                    RoleId = 2
//                };
//                var role = new Role()
//                {
//                    Id = 2,
//                    Name = "user"
//                };
//                arrangeContext.Roles.Add(role);
//                arrangeContext.Users.Add(user);
//                arrangeContext.Users.Add(user2);
//                await arrangeContext.SaveChangesAsync();
//            }

//            //act,assert
//            using (var assertContex = new PaySharpDBContext(options))
//            {
//                var sut = new UserService(assertContex, hashPassword.Object,
//                    configuration.Object, userMapper, adminMapper.Object,
//                    clientMapper.Object, accountMapper.Object);
//                var res = await sut.GetAllUsersAsync();

//                Assert.IsInstanceOfType(res, typeof(List<UserDTO>));
//                Assert.AreEqual(res.Count, 2);
              
//            }
//        }

//    }
//}
