﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Linq;
using System.Threading.Tasks;
namespace PaySharp.ServiceTests.UserServiceTests
{
    [TestClass]
    public class AddUserToClientAsync_ByClientName_Should
    {
        [TestMethod]
        public async Task IfUserIsNull_ThrowException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such user!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_USER")).Returns(confSection.Object);

            var options = TestUtils.GetOptions(nameof(IfUserIsNull_ThrowException));

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Roles.Add(role);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContext, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.AddUserToClientAsync(1, "Masters"));
                Assert.AreEqual(ex.Message, "There is no such user!");

            }
        }
        [TestMethod]
        public async Task IfClientIsNull_ThrowException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such client!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_CLIENT")).Returns(confSection.Object);

            var options = TestUtils.GetOptions(nameof(IfClientIsNull_ThrowException));

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id=1,
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Users.Add(user);
                arrangeContext.Roles.Add(role);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContext, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.AddUserToClientAsync(1, "Masters"));
                Assert.AreEqual(ex.Message, "There is no such client!");

            }
        }

        [TestMethod]
        public async Task IfUserClientExists_ThrowException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("User is already connected to this client");
            configuration.Setup(c => c.GetSection("GlobalConstants:USER_CLIENT")).Returns(confSection.Object);

            var options = TestUtils.GetOptions(nameof(IfClientIsNull_ThrowException));

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var client = new Client() { Name = "Masters", Id = 1 };
                var userClient = new UsersClients() { ClientId = 1, UserId = 1 };
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Roles.Add(role);
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.UsersClients.Add(userClient);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContext, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityAlreadyExistsException>(
                    async () => await sut.AddUserToClientAsync(1, "Masters"));
                Assert.AreEqual(ex.Message, "User is already connected to this client");

            }
        }

        [TestMethod]
        public async Task AddsUserToClient_ReturnsUserDTO()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();

            var configuration = new Mock<IConfiguration>();
           
            var options = TestUtils.GetOptions(nameof(AddsUserToClient_ReturnsUserDTO));

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var client = new Client() { Name = "Masters", Id = 1 };
                
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Roles.Add(role);
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContex, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var res = await sut.AddUserToClientAsync(1, "Masters");

                Assert.IsInstanceOfType(res, typeof(UserDTO));
                Assert.AreEqual(assertContex.UsersClients.Count(), 1);
                Assert.AreEqual(assertContex.UsersClients.First().UserId, 1);
                Assert.AreEqual(assertContex.UsersClients.First().ClientId, 1);
                Assert.AreEqual(res.Password, "password");
                Assert.AreEqual(res.UserName, "username");

            }
        }
    }
}
