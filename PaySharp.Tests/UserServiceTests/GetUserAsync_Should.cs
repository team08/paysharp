﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.UserServiceTests
{
    [TestClass]
    public class GetUserAsync_Should
    {
        [TestMethod]
        public async Task IfUserIsNull_ThrowException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();  
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such user!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_USER")).Returns(confSection.Object);

            var options = TestUtils.GetOptions(nameof(IfUserIsNull_ThrowException));

            hashPassword.Setup(h => h.GetHashString("password")).Returns("password");

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Roles.Add(role);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContext, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.GetUserAsync( "userName", "password"));
                Assert.AreEqual(ex.Message, "There is no such user!");
               
            }
        }

        [TestMethod]
        public async Task IfPasswordIsInCorrect_ThrowException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(IfPasswordIsInCorrect_ThrowException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("The password is incorrect!");
            configuration.Setup(c => c.GetSection("GlobalConstants:INCORRECT_PASSWORD")).Returns(confSection.Object);

            hashPassword.Setup(h => h.GetHashString("password")).Returns("pass");

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Users.Add(user);
                arrangeContext.Roles.Add(role);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContext, hashPassword.Object,
                     configuration.Object, userMapper, adminMapper.Object,
                     clientMapper.Object, accountMapper.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.GetUserAsync("username", "password"));
                Assert.AreEqual(ex.Message, "The password is incorrect!");
            }
        }

        [TestMethod]
        public async Task IfGetTheRightUser_ReturnUserDTO()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(IfGetTheRightUser_ReturnUserDTO));
            
            var configuration = new Mock<IConfiguration>();

            hashPassword.Setup(h => h.GetHashString("password")).Returns("password");

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Name="name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Users.Add(user);
                arrangeContext.Roles.Add(role);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContex, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var res = await sut.GetUserAsync( "username", "password");

                Assert.IsInstanceOfType(res, typeof(UserDTO));
                Assert.AreEqual(res.Name, "name");
                Assert.AreEqual(res.Password, "password");
                Assert.AreEqual(res.UserName, "username");
            }
        }
    }
}
