using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.UserServiceTests
{
    [TestClass]
    public class AddUserAsync_Should
    {
        [TestMethod]
        public async Task IfUserNameExists_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("User with this name already exists!");
            configuration.Setup(c => c.GetSection("GlobalConstants:USER_EXISTS")).Returns(confSection.Object);
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();;
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>(); 
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();   

            var options = TestUtils.GetOptions(nameof(IfUserNameExists_ThrowsException));

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContext, hashPassword.Object, 
                    configuration.Object, userMapper, adminMapper.Object, 
                    clientMapper.Object,accountMapper.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityAlreadyExistsException>(
                    async () => await sut.AddUserAsync("name", "userName", "password"));
                Assert.AreEqual(ex.Message, "User with this name already exists!");
            }
        }

        [TestMethod]
        public async Task IfUserNameDoesNotExists_ReturnUserDTO()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var configuration = new Mock<IConfiguration>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();  
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();

            var options = TestUtils.GetOptions(nameof(IfUserNameDoesNotExists_ReturnUserDTO));

            hashPassword.Setup(x => x.GetHashString("password")).Returns("password");
        
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut  = new UserService(assertContext, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var res = await sut.AddUserAsync("name", "userName", "password");

                Assert.IsInstanceOfType(res, typeof(UserDTO));
                Assert.AreEqual(assertContext.Users.Count(), 1);
                Assert.AreEqual(assertContext.Users.First().Name, "name");
                Assert.AreEqual(assertContext.Users.First().Password, "password");
                Assert.AreEqual(assertContext.Users.First().UserName, "userName");
                Assert.AreEqual(assertContext.Users.First().RoleId, 2);
                Assert.AreEqual(res.Name, "name");
                Assert.AreEqual(res.Password, "password");
                Assert.AreEqual(res.UserName, "userName");
            }
        }
    }
}
