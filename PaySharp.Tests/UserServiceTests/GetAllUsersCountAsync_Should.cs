﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.UserServiceTests
{
    [TestClass]
    public class GetAllUsersCountAsync_Should
    {
        [TestMethod]
        public async Task GetUsersCount()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(GetUsersCount));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var user2 = new User()
                {
                    Name = "name2",
                    UserName = "username2",
                    Password = "password",
                    RoleId = 2
                };
                
               
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContex, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var res = await sut.GetAllUsersCountAsync();

                Assert.AreEqual(res, 2);

            }
        }
    }
}
