﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.UserServiceTests
{
    [TestClass]
    public class GetAdminAync_Should
    {
        [TestMethod]
        public async Task IfAdminIsNull_ThrowException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new Mock<IDtoMapper<User, UserDTO>>();
            var adminMapper = new AdminDTOMapper();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such user!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_USER")).Returns(confSection.Object);

            var options = TestUtils.GetOptions(nameof(IfAdminIsNull_ThrowException));

            hashPassword.Setup(h => h.GetHashString("password")).Returns("password");

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Roles.Add(role);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContext, hashPassword.Object,
                    configuration.Object, userMapper.Object, adminMapper,
                    clientMapper.Object, accountMapper.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.GetAdminAsync("userName", "password"));
                Assert.AreEqual(ex.Message, "There is no such user!");

            }
        }

        [TestMethod]
        public async Task IfPasswordIsInCorrect_ThrowException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new Mock<IDtoMapper<User, UserDTO>>();
            var adminMapper = new AdminDTOMapper();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(IfPasswordIsInCorrect_ThrowException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("The password is incorrect!");
            configuration.Setup(c => c.GetSection("GlobalConstants:INCORRECT_PASSWORD")).Returns(confSection.Object);

            hashPassword.Setup(h => h.GetHashString("password")).Returns("pass");

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var admin = new Admin()
                {
                    
                    UserName = "admin",
                    Password = "password",
                    RoleId = 1
                };
                var role = new Role()
                {
                    Id = 1,
                    Name = "admin"
                };
                arrangeContext.Admins.Add(admin);
                arrangeContext.Roles.Add(role);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContext, hashPassword.Object,
                     configuration.Object, userMapper.Object, adminMapper,
                     clientMapper.Object, accountMapper.Object);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.GetAdminAsync("admin", "password"));
                Assert.AreEqual(ex.Message, "The password is incorrect!");
            }
        }

        [TestMethod]
        public async Task IfGetTheRightAdmin_ReturnAdminDTO()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new Mock<IDtoMapper<User, UserDTO>>();
            var adminMapper = new AdminDTOMapper();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(IfGetTheRightAdmin_ReturnAdminDTO));

            var configuration = new Mock<IConfiguration>();

            hashPassword.Setup(h => h.GetHashString("password")).Returns("password");

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var admin = new Admin()
                {

                    UserName = "admin",
                    Password = "password",
                    RoleId = 1
                };
                var role = new Role()
                {
                    Id = 1,
                    Name = "admin"
                };
                arrangeContext.Admins.Add(admin);
                arrangeContext.Roles.Add(role);
                await arrangeContext.SaveChangesAsync();
                
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContex, hashPassword.Object,
                    configuration.Object, userMapper.Object, adminMapper,
                    clientMapper.Object, accountMapper.Object);
                var res = await sut.GetAdminAsync("admin", "password");

                Assert.IsInstanceOfType(res, typeof(AdminDTO));
                Assert.AreEqual(res.Role, "admin");
                Assert.AreEqual(res.Password, "password");
                Assert.AreEqual(res.UserName, "admin");
            }
        }
    }
}
