﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.UserServiceTests
{
    [TestClass]
    public class GetUserAsync_Should_ById
    {
        [TestMethod]
        public async Task ReturnsUserDTO()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new AccountDTOMapper();
            var userClientMapper =  new UsersClientsDTOMapper(userMapper);
            var clientMapper = new ClientDTOMapper(accountMapper,userClientMapper);
            var options = TestUtils.GetOptions(nameof(ReturnsUserDTO));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id=1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var role = new Role() { Id = 2, Name = "user" };
                var client = new Client() { Name = "Masters", Id = 1 };
                var userClient = new UsersClients() { ClientId = 1, UserId = 1 };
                var account = new Account() { AccountNumber = "1234567890", Id = 1,ClientId=1 };
                var userAccount = new UsersAccounts() { AccountId = 1, UserId = 1 };
                arrangeContext.Users.Add(user);
                arrangeContext.Roles.Add(role);
                arrangeContext.Clients.Add(client);
                arrangeContext.UsersClients.Add(userClient);
                arrangeContext.Accounts.Add(account);
                arrangeContext.UsersAccounts.Add(userAccount);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContex, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper, accountMapper);
                var res = await sut.GetUserAsync(1);

                Assert.IsInstanceOfType(res, typeof(UserDTO));
                Assert.AreEqual(res.Name, "name");
                Assert.AreEqual(res.UserId, 1);
                Assert.AreEqual(res.Password, null);
                Assert.AreEqual(res.UserName, "username");
                Assert.AreEqual(res.Accounts.Count, 1);
                Assert.AreEqual(res.Clients.Count, 1);
            }
        }
        [TestMethod]
        public async Task IfUserIsNull_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new AccountDTOMapper();
            var userClientMapper = new UsersClientsDTOMapper(userMapper);
            var clientMapper = new ClientDTOMapper(accountMapper, userClientMapper);
            var options = TestUtils.GetOptions(nameof(IfUserIsNull_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such user!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_USER")).Returns(confSection.Object);

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Id = 1,
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var role = new Role() { Id = 2, Name = "user" };
                var client = new Client() { Name = "Masters", Id = 1 };
                var userClient = new UsersClients() { ClientId = 1, UserId = 1 };
                var account = new Account() { AccountNumber = "1234567890", Id = 1, ClientId = 1 };
                var userAccount = new UsersAccounts() { AccountId = 1, UserId = 1 };
                arrangeContext.Users.Add(user);
                arrangeContext.Roles.Add(role);
                arrangeContext.Clients.Add(client);
                arrangeContext.UsersClients.Add(userClient);
                arrangeContext.Accounts.Add(account);
                arrangeContext.UsersAccounts.Add(userAccount);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContex, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper, accountMapper);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.GetUserAsync(2));
                Assert.AreEqual(ex.Message, "There is no such user!");
            }
        }
    }
}
