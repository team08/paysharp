﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Tests;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.UserServiceTests
{
    [TestClass]
    public class FindUserContainingAsync_Should
    {
        [TestMethod]
        public async Task GetListOfFiveUserNames()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(GetListOfFiveUserNames));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Name = "name",
                    UserName = "username",
                    Password = "password",
                    RoleId = 2
                };
                var user2 = new User()
                {
                    Name = "name2",
                    UserName = "username2",
                    Password = "password",
                    RoleId = 2
                };
                var user3 = new User()
                {
                    Name = "name3",
                    UserName = "username3",
                    Password = "password",
                    RoleId = 2
                };
                var user4 = new User()
                {
                    Name = "name4",
                    UserName = "username4",
                    Password = "password",
                    RoleId = 2
                };
                var user5 = new User()
                {
                    Name = "name5",
                    UserName = "username5",
                    Password = "password",
                    RoleId = 2
                };
                var user6 = new User()
                {
                    Name = "name6",
                    UserName = "username6",
                    Password = "password",
                    RoleId = 2
                };
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Roles.Add(role);
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.Users.Add(user3);
                arrangeContext.Users.Add(user4);
                arrangeContext.Users.Add(user5);
                arrangeContext.Users.Add(user6);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContex, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var res = await sut.FindUserContainingAsync("use");

                Assert.AreEqual(res.Count, 5);
            }
        }
        [TestMethod]
        public async Task GetListOfOneUserName()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var userMapper = new UserDTOMapper();
            var adminMapper = new Mock<IDtoMapper<Admin, AdminDTO>>();
            var accountMapper = new Mock<IDtoMapper<Account, AccountDTO>>();
            var clientMapper = new Mock<IDtoMapper<Client, ClientDTO>>();
            var options = TestUtils.GetOptions(nameof(GetListOfOneUserName));

            var configuration = new Mock<IConfiguration>();

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User()
                {
                    Name = "name",
                    UserName = "username1",
                    Password = "password",
                    RoleId = 2
                };
                var user2 = new User()
                {
                    Name = "name2",
                    UserName = "username2",
                    Password = "password",
                    RoleId = 2
                };
                var user3 = new User()
                {
                    Name = "name3",
                    UserName = "username3",
                    Password = "password",
                    RoleId = 2
                };
                var user4 = new User()
                {
                    Name = "name4",
                    UserName = "username4",
                    Password = "password",
                    RoleId = 2
                };
                var user5 = new User()
                {
                    Name = "name5",
                    UserName = "username5",
                    Password = "password",
                    RoleId = 2
                };
                var user6 = new User()
                {
                    Name = "name6",
                    UserName = "username6",
                    Password = "password",
                    RoleId = 2
                };
                var role = new Role()
                {
                    Id = 2,
                    Name = "user"
                };
                arrangeContext.Roles.Add(role);
                arrangeContext.Users.Add(user);
                arrangeContext.Users.Add(user2);
                arrangeContext.Users.Add(user3);
                arrangeContext.Users.Add(user4);
                arrangeContext.Users.Add(user5);
                arrangeContext.Users.Add(user6);
                await arrangeContext.SaveChangesAsync();
            }

            //act,assert
            using (var assertContex = new PaySharpDBContext(options))
            {
                var sut = new UserService(assertContex, hashPassword.Object,
                    configuration.Object, userMapper, adminMapper.Object,
                    clientMapper.Object, accountMapper.Object);
                var res = await sut.FindUserContainingAsync("username6");

                Assert.AreEqual(res.Count, 1);
                Assert.AreEqual(res[0], "username6");
            }
        }
    }
}
