﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.Contracts;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.Wrapper;
using PaySharp.Tests;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.TransactionServiceTests
{
    [TestClass]
    public class GetTransactionsCount_Should
    {
        [TestMethod]
        public async Task ReturnsAllUserTransactionsCount()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReturnsAllUserTransactionsCount));

            var configuration = new Mock<IConfiguration>();
            
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var account3 = new Account()
            {
                Id = 3,
                AccountNumber = "1234567892",
                Balance = 10000,
                NickName = "1234567892",
                ClientId = 1
            };
            var accountIds = new List<long>() { 1, 2 };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetUserAccountsIdsAsync(1)).ReturnsAsync(accountIds);
           
           
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var transaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 3,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var user = new User() {Id=1, UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.Transactions.Add(transaction);
                arrangeContext.Transactions.Add(transaction2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
         
                var res = await sut.GetTransactionsCount(1, null);

                Assert.AreEqual(res, 1);
            }
        }

        [TestMethod]
        public async Task ReturnsUserAccountTransactionsCount()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReturnsUserAccountTransactionsCount));

            var configuration = new Mock<IConfiguration>();

            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var account3 = new Account()
            {
                Id = 3,
                AccountNumber = "1234567892",
                Balance = 10000,
                NickName = "1234567892",
                ClientId = 1
            };
            var accountIds = new List<long>() { 1, 2 };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetUserAccountsIdsAsync(1)).ReturnsAsync(accountIds);


            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var transaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 3,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var user = new User() { Id = 1, UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.Transactions.Add(transaction);
                arrangeContext.Transactions.Add(transaction2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);

                var res = await sut.GetTransactionsCount(1, 1);

                Assert.AreEqual(res, 1);
            }
        }
        [TestMethod]
        public async Task ReturnsZeroUserAccountTransactionsCount()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReturnsZeroUserAccountTransactionsCount));

            var configuration = new Mock<IConfiguration>();

            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var account3 = new Account()
            {
                Id = 3,
                AccountNumber = "1234567892",
                Balance = 10000,
                NickName = "1234567892",
                ClientId = 1
            };
            var accountIds = new List<long>() { 1, 2 };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetUserAccountsIdsAsync(1)).ReturnsAsync(accountIds);


            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var transaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 3,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var user = new User() { Id = 1, UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.Transactions.Add(transaction);
                arrangeContext.Transactions.Add(transaction2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);

                var res = await sut.GetTransactionsCount(1, 2);

                Assert.AreEqual(res, 0);
            }
        }
    }
}
