﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Entities;
using PaySharp.Services;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.Wrapper;
using PaySharp.Tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.TransactionServiceTests
{
    [TestClass]
    public class GetAllUserTransactions_Should
    {
        [TestMethod]
        public async Task ReturnsAllUserTransactions()
        {
            //arrange
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReturnsAllUserTransactions));

            var configuration = new Mock<IConfiguration>();

            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var account3 = new Account()
            {
                Id = 3,
                AccountNumber = "1234567892",
                Balance = 10000,
                NickName = "1234567892",
                ClientId = 1
            };
            var accountIds = new List<long>() { 1, 2 };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetUserAccountsIdsAsync(1)).ReturnsAsync(accountIds);


            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 1,
                    TimeStamp = date
                };
                var transaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var status = new Status() { Id = 1, StatusName = "sent" };
                var status2 = new Status() { Id = 2, StatusName = "saved" };
                var user = new User() { Id = 1, UserName = "username" };
                var userAccount = new UsersAccounts() { UserId = 1, AccountId=1 };
                var userAccount2 = new UsersAccounts() { UserId = 1, AccountId = 2 };
                var client = new Client() { Id = 1,Name = "Master" };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Status.Add(status);
                arrangeContext.Status.Add(status2);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.UsersAccounts.Add(userAccount);
                arrangeContext.UsersAccounts.Add(userAccount2);
                arrangeContext.Transactions.Add(transaction);
                arrangeContext.Transactions.Add(transaction2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);

                var res = await sut.GetAllUserTransactions(1, 1);

                Assert.IsInstanceOfType(res, typeof(List<TransactionDTO>));
                Assert.AreEqual(res.Count(), 3);
                Assert.IsTrue(res[0].IsSend);
                Assert.IsTrue(res[1].IsSend);
                Assert.IsTrue(res[2].IsRecieved);
            }
        }

        [TestMethod]
        public async Task ReturnsAllUserTransactionsWithTrueForSetAndFalseForRecieved()
        {
            //arrange
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReturnsAllUserTransactions));

            var configuration = new Mock<IConfiguration>();

            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var account3 = new Account()
            {
                Id = 3,
                AccountNumber = "1234567892",
                Balance = 10000,
                NickName = "1234567892",
                ClientId = 1
            };
            var accountIds = new List<long>() { 1, 2 };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetUserAccountsIdsAsync(1)).ReturnsAsync(accountIds);


            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 1,
                    TimeStamp = date
                };
                var transaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 3,
                    StatusId = 2,
                    TimeStamp = date
                };
                var status = new Status() { Id = 1, StatusName = "sent" };
                var status2 = new Status() { Id = 2, StatusName = "saved" };
                var user = new User() { Id = 1, UserName = "username" };
                var userAccount = new UsersAccounts() { UserId = 1, AccountId = 1 };
                var userAccount2 = new UsersAccounts() { UserId = 1, AccountId = 2 };
                var client = new Client() { Id = 1, Name = "Master" };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Status.Add(status);
                arrangeContext.Status.Add(status2);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.UsersAccounts.Add(userAccount);
                arrangeContext.UsersAccounts.Add(userAccount2);
                arrangeContext.Transactions.Add(transaction);
                arrangeContext.Transactions.Add(transaction2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);

                var res = await sut.GetAllUserTransactions(1, 1);

                Assert.IsInstanceOfType(res, typeof(List<TransactionDTO>));
                Assert.AreEqual(res.Count(), 3);
                Assert.IsTrue(res[0].IsSend);
                Assert.IsTrue(res[1].IsSend);
                Assert.IsTrue(res[2].IsRecieved);
            }
        }
        [TestMethod]
        public async Task ReturnsUserTransactionsPaginatedFirstPage()
        {
            //arrange
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReturnsAllUserTransactions));

            var configuration = new Mock<IConfiguration>();

            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var account3 = new Account()
            {
                Id = 3,
                AccountNumber = "1234567892",
                Balance = 10000,
                NickName = "1234567892",
                ClientId = 1
            };
            var accountIds = new List<long>() { 1, 2 };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetUserAccountsIdsAsync(1)).ReturnsAsync(accountIds);


            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 1,
                    TimeStamp = date
                };
                var transaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 3,
                    StatusId = 2,
                    TimeStamp = date
                };
                var status = new Status() { Id = 1, StatusName = "sent" };
                var status2 = new Status() { Id = 2, StatusName = "saved" };
                var user = new User() { Id = 1, UserName = "username" };
                var userAccount = new UsersAccounts() { UserId = 1, AccountId = 1 };
                var userAccount2 = new UsersAccounts() { UserId = 1, AccountId = 2 };
                var client = new Client() { Id = 1, Name = "Master" };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Status.Add(status);
                arrangeContext.Status.Add(status2);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.UsersAccounts.Add(userAccount);
                arrangeContext.UsersAccounts.Add(userAccount2);
                arrangeContext.Transactions.Add(transaction);
                arrangeContext.Transactions.Add(transaction2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);

                var res = await sut.GetAllUserTransactions(1, 1,1);

                Assert.IsInstanceOfType(res, typeof(List<TransactionDTO>));
                Assert.AreEqual(res.Count(), 1);
                Assert.IsTrue(res[0].IsSend);
            }
        }
        [TestMethod]
        public async Task ReturnsUserTransactionsPaginatedSecondPage()
        {
            //arrange
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReturnsAllUserTransactions));

            var configuration = new Mock<IConfiguration>();

            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var account3 = new Account()
            {
                Id = 3,
                AccountNumber = "1234567892",
                Balance = 10000,
                NickName = "1234567892",
                ClientId = 1
            };
            var accountIds = new List<long>() { 1, 2 };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetUserAccountsIdsAsync(1)).ReturnsAsync(accountIds);


            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 1,
                    TimeStamp = date
                };
                var transaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 3,
                    StatusId = 2,
                    TimeStamp = date
                };
                var status = new Status() { Id = 1, StatusName = "sent" };
                var status2 = new Status() { Id = 2, StatusName = "saved" };
                var user = new User() { Id = 1, UserName = "username" };
                var userAccount = new UsersAccounts() { UserId = 1, AccountId = 1 };
                var userAccount2 = new UsersAccounts() { UserId = 1, AccountId = 2 };
                var client = new Client() { Id = 1, Name = "Master" };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Status.Add(status);
                arrangeContext.Status.Add(status2);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.UsersAccounts.Add(userAccount);
                arrangeContext.UsersAccounts.Add(userAccount2);
                arrangeContext.Transactions.Add(transaction);
                arrangeContext.Transactions.Add(transaction2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);

                var res = await sut.GetAllUserTransactions(1, 2, 1);

                Assert.IsInstanceOfType(res, typeof(List<TransactionDTO>));
                Assert.AreEqual(res.Count(), 1);
                Assert.IsTrue(res[0].IsSend);
                Assert.IsFalse(res[0].IsRecieved);

            }
        }
        [TestMethod]
        public async Task ReturnsUserTransactionsRecieved()
        {
            //arrange
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReturnsAllUserTransactions));

            var configuration = new Mock<IConfiguration>();

            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var account3 = new Account()
            {
                Id = 3,
                AccountNumber = "1234567892",
                Balance = 10000,
                NickName = "1234567892",
                ClientId = 1
            };
            var accountIds = new List<long>() { 1, 2 };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetUserAccountsIdsAsync(1)).ReturnsAsync(accountIds);


            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 3,
                    ReceiverAccountID = 2,
                    StatusId = 1,
                    TimeStamp = date
                };
                var transaction2 = new Transaction()
                {
                    Id = 2,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 3,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var status = new Status() { Id = 1, StatusName = "sent" };
                var status2 = new Status() { Id = 2, StatusName = "saved" };
                var user = new User() { Id = 1, UserName = "username" };
                var userAccount = new UsersAccounts() { UserId = 1, AccountId = 1 };
                var userAccount2 = new UsersAccounts() { UserId = 1, AccountId = 2 };
                var client = new Client() { Id = 1, Name = "Master" };
                arrangeContext.Users.Add(user);
                arrangeContext.Clients.Add(client);
                arrangeContext.Status.Add(status);
                arrangeContext.Status.Add(status2);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Accounts.Add(account3);
                arrangeContext.UsersAccounts.Add(userAccount);
                arrangeContext.UsersAccounts.Add(userAccount2);
                arrangeContext.Transactions.Add(transaction);
                arrangeContext.Transactions.Add(transaction2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);

                var res = await sut.GetAllUserTransactions(1, 1);

                Assert.IsInstanceOfType(res, typeof(List<TransactionDTO>));
                Assert.AreEqual(res.Count(), 1);
                Assert.IsFalse(res[0].IsSend);
                Assert.IsTrue(res[0].IsRecieved);

            }
        }
    }
}
