﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.Wrapper;
using PaySharp.Tests;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.TransactionServiceTests
{
    [TestClass]
    public class SendTransactionAsync_Should
    {
        [TestMethod]
        public async Task SenderAccountNull_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(SenderAccountNull_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such account!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_ACCOUNT")).Returns(confSection.Object);
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetAccountAsync("1234567890")).ReturnsAsync((Account)null);
            dateWrapper.Setup(x => x.Now()).Returns(date);
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.SendTransactionAsync("1234567890", "1234567891", "test", 500));
                Assert.AreEqual(ex.Message, "There is no such account!");
            }
        }
        [TestMethod]
        public async Task ReceiverAccountNull_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReceiverAccountNull_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such account!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_ACCOUNT")).Returns(confSection.Object);
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 1,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetAccountAsync("1234567890")).ReturnsAsync(account);
            accountService.Setup(x => x.GetAccountAsync("1234567891")).ReturnsAsync((Account)null);
            dateWrapper.Setup(x => x.Now()).Returns(date);
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.SendTransactionAsync("1234567890", "1234567891", "test", 500));
                Assert.AreEqual(ex.Message, "There is no such account!");
            }
        }
        [TestMethod]
        public async Task ReceiverAndSenderAccountTheSame_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReceiverAndSenderAccountTheSame_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("The two accounts are the same");
            configuration.Setup(c => c.GetSection("GlobalConstants:SAME_ACCOUNT")).Returns(confSection.Object);
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetAccountAsync("1234567890")).ReturnsAsync(account);
            dateWrapper.Setup(x => x.Now()).Returns(date);
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
                var ex = await Assert.ThrowsExceptionAsync<EntityAlreadyExistsException>(
                    async () => await sut.SendTransactionAsync("1234567890", "1234567890", "test", 500));
                Assert.AreEqual(ex.Message, "The two accounts are the same");
            }
        }
        [TestMethod]
        public async Task SenderAccountBalanceNotEnogh_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(SenderAccountBalanceNotEnogh_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is not enough money in the account!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NOT_ENOUGH")).Returns(confSection.Object);
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetAccountAsync("1234567890")).ReturnsAsync(account);
            accountService.Setup(x => x.GetAccountAsync("1234567891")).ReturnsAsync(account2);
            dateWrapper.Setup(x => x.Now()).Returns(date);
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
                var ex = await Assert.ThrowsExceptionAsync<InsufficientFundsException>(
                    async () => await sut.SendTransactionAsync("1234567890", "1234567891", "test", 10500));
                Assert.AreEqual(ex.Message, "There is not enough money in the account!");
            }
        }

        [TestMethod]
        public async Task CompletesTransactionCorrectly()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(CompletesTransactionCorrectly));

            var configuration = new Mock<IConfiguration>();

            using (var assertContext = new PaySharpDBContext(options))
            {
                var account = new Account()
                {
                    Id = 1,
                    AccountNumber = "1234567890",
                    Balance = 10000,
                    NickName = "salary",
                    ClientId = 1
                };
                var account2 = new Account()
                {
                    Id = 2,
                    AccountNumber = "1234567891",
                    Balance = 10000,
                    NickName = "1234567891",
                    ClientId = 1
                };
                var date = DateTime.UtcNow;
                accountService.Setup(x => x.GetAccountAsync("1234567890")).ReturnsAsync(account);
                accountService.Setup(x => x.GetAccountAsync("1234567891")).ReturnsAsync(account2);
                dateWrapper.Setup(x => x.Now()).Returns(date);

                var user = new User() { UserName = "userName" };
                assertContext.Users.Add(user);
                assertContext.Accounts.Add(account);
                assertContext.Accounts.Add(account2);
                await assertContext.SaveChangesAsync();

                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);

                var res = await sut.SendTransactionAsync("1234567890", "1234567891", "test", 1000);
                //TODO
                Assert.IsInstanceOfType(res, typeof(TransactionDTO));
                Assert.AreEqual(assertContext.Transactions.Count(), 1);
                Assert.AreEqual(assertContext.Transactions.First().Amount, 1000);
                Assert.AreEqual(assertContext.Transactions.First().SenderAccountID, 1);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccountID, 2);
                Assert.AreEqual(assertContext.Transactions.First().Description, "test");
                Assert.AreEqual(assertContext.Transactions.First().StatusId, 1);
                Assert.AreEqual(assertContext.Transactions.First().TimeStamp, date);
                Assert.AreEqual(assertContext.Accounts.First(f => f.Id == 1).Balance, 9000);
                Assert.AreEqual(assertContext.Accounts.First(f => f.Id == 2).Balance, 11000);
                Assert.AreEqual(res.Amount, 1000);
                Assert.AreEqual(res.SenderAccountID, 1);
                Assert.AreEqual(res.ReceiverAccountID, 2);
                Assert.AreEqual(res.Description, "test");
                Assert.AreEqual(res.StatusId, 1);
                Assert.AreEqual(res.TimeStamp, date);
                Assert.AreEqual(account.Balance, 9000);
                Assert.AreEqual(account2.Balance, 11000);
            }
        }
    }
}
