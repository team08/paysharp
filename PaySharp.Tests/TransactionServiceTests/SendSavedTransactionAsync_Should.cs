﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.Wrapper;
using PaySharp.Tests;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.TransactionServiceTests
{
    [TestClass]
    public class SendSavedTransactionAsync_Should
    {
        [TestMethod]
        public async Task TransactionsIsNull_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(TransactionsIsNull_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such transaction!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_TRANSACTION")).Returns(confSection.Object);
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetAccountAsync("1234567890")).ReturnsAsync(account);
            accountService.Setup(x => x.GetAccountAsync("1234567891")).ReturnsAsync(account2);
            dateWrapper.Setup(x => x.Now()).Returns(date);
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Transactions.Add(transaction);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.SendSavedTransactionAsync(2));
                Assert.AreEqual(ex.Message, "There is no such transaction!");
            }
        }
        [TestMethod]
        public async Task SenderAccountIsNull_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(TransactionsIsNull_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such account!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_ACCOUNT")).Returns(confSection.Object);
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetAccountByIdAsync(1)).ReturnsAsync((Account)null);
            accountService.Setup(x => x.GetAccountByIdAsync(2)).ReturnsAsync(account2);
            dateWrapper.Setup(x => x.Now()).Returns(date);
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Transactions.Add(transaction);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.SendSavedTransactionAsync(1));
                Assert.AreEqual(ex.Message, "There is no such account!");
            }
        }
        [TestMethod]
        public async Task ReceiverAccountIsNull_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(ReceiverAccountIsNull_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is no such account!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NO_ACCOUNT")).Returns(confSection.Object);
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetAccountByIdAsync(1)).ReturnsAsync(account);
            accountService.Setup(x => x.GetAccountByIdAsync(2)).ReturnsAsync((Account)null);
            dateWrapper.Setup(x => x.Now()).Returns(date);
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Transactions.Add(transaction);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
                var ex = await Assert.ThrowsExceptionAsync<EntityNotFoundException>(
                    async () => await sut.SendSavedTransactionAsync(1));
                Assert.AreEqual(ex.Message, "There is no such account!");
            }
        }
        [TestMethod]
        public async Task SenderAccountBalanceNotEnough_ThrowsException()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(SenderAccountBalanceNotEnough_ThrowsException));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is not enough money in the account!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NOT_ENOUGH")).Returns(confSection.Object);
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetAccountByIdAsync(1)).ReturnsAsync(account);
            accountService.Setup(x => x.GetAccountByIdAsync(2)).ReturnsAsync(account2);
            dateWrapper.Setup(x => x.Now()).Returns(date);
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 10500,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Transactions.Add(transaction);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
                var ex = await Assert.ThrowsExceptionAsync<InsufficientFundsException>(
                    async () => await sut.SendSavedTransactionAsync(1));
                Assert.AreEqual(ex.Message, "There is not enough money in the account!");
            }
        }

        [TestMethod]
        public async Task CompletesTransactionCorrectly()
        {
            //arrange
            var hashPassword = new Mock<IPasswordHasher>();
            var dateWrapper = new Mock<IDateWrapper>();
            var accountMapper = new AccountDTOMapper();
            var transactionMapper = new TransactionDTOMapper(accountMapper);
            var accountService = new Mock<IAccountService>();

            var options = TestUtils.GetOptions(nameof(CompletesTransactionCorrectly));

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("There is not enough money in the account!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NOT_ENOUGH")).Returns(confSection.Object);
            var account = new Account()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "salary",
                ClientId = 1
            };
            var account2 = new Account()
            {
                Id = 2,
                AccountNumber = "1234567891",
                Balance = 10000,
                NickName = "1234567891",
                ClientId = 1
            };
            var date = DateTime.UtcNow;
            accountService.Setup(x => x.GetAccountByIdAsync(1)).ReturnsAsync(account);
            accountService.Setup(x => x.GetAccountByIdAsync(2)).ReturnsAsync(account2);
            dateWrapper.Setup(x => x.Now()).Returns(date);
            using (var arrangeContext = new PaySharpDBContext(options))
            {
                var transaction = new Transaction()
                {
                    Id = 1,
                    Amount = 1000,
                    Description = "test",
                    SenderAccountID = 1,
                    ReceiverAccountID = 2,
                    StatusId = 2,
                    TimeStamp = date
                };
                var user = new User() { UserName = "userName" };
                arrangeContext.Users.Add(user);
                arrangeContext.Accounts.Add(account);
                arrangeContext.Accounts.Add(account2);
                arrangeContext.Transactions.Add(transaction);
                await arrangeContext.SaveChangesAsync();
            }
            //act,assert
            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new TransactionService(assertContext,
                    configuration.Object, transactionMapper, accountService.Object
                    , dateWrapper.Object, accountMapper);
               
                var res = await sut.SendSavedTransactionAsync(1);

                Assert.IsInstanceOfType(res, typeof(TransactionDTO));
                Assert.AreEqual(assertContext.Transactions.Count(), 1);
                Assert.AreEqual(assertContext.Transactions.First().Amount, 1000);
                Assert.AreEqual(assertContext.Transactions.First().SenderAccountID, 1);
                Assert.AreEqual(assertContext.Transactions.First().ReceiverAccountID, 2);
                Assert.AreEqual(assertContext.Transactions.First().Description, "test");
                Assert.AreEqual(assertContext.Transactions.First().StatusId, 1);
                Assert.AreEqual(assertContext.Transactions.First().TimeStamp, date);
                Assert.AreEqual(res.Amount, 1000);
                Assert.AreEqual(res.SenderAccountID, 1);
                Assert.AreEqual(res.ReceiverAccountID, 2);
                Assert.AreEqual(res.Description, "test");
                Assert.AreEqual(res.StatusId, 1);
                Assert.AreEqual(res.TimeStamp, date);
                Assert.AreEqual(account.Balance, 9000);
                Assert.AreEqual(account2.Balance, 11000);
            }
        }
    }
}
