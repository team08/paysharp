﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services;

namespace PaySharp.ServiceTests.TokenManagerTests
{
    [TestClass]
    public class GetToken_Should
    {
        [TestMethod]
        public void GetToken_Should_ReturnNull_WhenExceptionIsThrown()
        {
            var exampleToken2 = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InN0YWthdGEiLCJ1c2VyUm9sZSI6IkFkbWluIiwibmJmIjoxNTU4MzM2OTcxLCJleHAiOjE1NTg0NDQ5NzAsImlhdCI6MTU1ODMzNjk3MX0.LUz95iTGgKvKqwVaWmRJSG_gnuZ0yS7DPVTkj9FC4Sw";

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("exception throw");
            configuration.Setup(c => c.GetSection("TokenSecrets:JWT"))
                .Returns(confSection.Object);

            var sut = new TokenManager(configuration.Object);

            var result = sut.GetPrincipal(exampleToken2);

            Assert.IsTrue(result == null);
        }

        [TestMethod]
        public void GenerateToken_Should_ReturnNotEmptyOrNullString_WhenValidParameterIsPassed()
        {
            var exampleSecret = "d8bec9dfa11de0c600a88b0d5dec0acce9deb0cc812f0865671ca2c2cfa40da8";

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns(exampleSecret);
            configuration.Setup(c => c.GetSection("TokenSecrets:JWT"))
                .Returns(confSection.Object);

            var sut = new TokenManager(configuration.Object);

            var result = sut.GenerateToken("valid", "Admin", "1");

            Assert.IsFalse(string.IsNullOrEmpty(result));
        }
    }
}
