﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.Entities;
using PaySharp.Services;
using System.Collections.Generic;


namespace PaySharp.ServiceTests.TokenManagerTests
{
    [TestClass]
    public class GetPrincipal_Should
    {
        [TestMethod]
        public void GetPrincipal_Should_ReturnNull_WhenInvalidTokenIsPassed()
        {
            var exampleSecret = "d8bec9dfa11de0c600a88b0d5dec0acce9deb0cc812f0865671ca2c2cfa40da8";
            var validToken = @"invalid";

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns(exampleSecret);
            configuration.Setup(c => c.GetSection("TokenSecrets:JWT"))
                .Returns(confSection.Object);

            var sut = new TokenManager(configuration.Object);

            var result = sut.GetPrincipal(validToken);

            Assert.IsTrue(result == null);
        }
    }
}
