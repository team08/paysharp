﻿//using Microsoft.Extensions.Configuration;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using PaySharp.Data.Entities;
//using PaySharp.Services;
//using System.Collections.Generic;


//namespace PaySharp.ServiceTests.TokenManagerTests
//{
//    [TestClass]
//    public class ValidateToken_Should
//    {
//        [TestMethod]
//        public void ValidateToken_ReturnProperUserName_WhenValidTokenIsPassed()
//        {
//            var exampleSecret = "d8bec9dfa11de0c600a88b0d5dec0acce9deb0cc812f0865671ca2c2cfa40da8";
//            var exampleToken2 = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InN0YWthdGEiLCJ1c2VyUm9sZSI6IkFkbWluIiwibmJmIjoxNTU4MzM2OTcxLCJleHAiOjE1NTg0NDQ5NzAsImlhdCI6MTU1ODMzNjk3MX0.LUz95iTGgKvKqwVaWmRJSG_gnuZ0yS7DPVTkj9FC4Sw";

//            var expectedUser = new User()
//            {
//                Id = 3,
//                Name = "stakata",
//                UserName = "ExampleUserName3",
//                Password = "ExamplePassword3",
//                Role = new Role { Name = "User" },
//                UsersAccounts = new List<UsersAccounts>(),
//                UsersClients = new List<UsersClients>()
//            };

//            var configuration = new Mock<IConfiguration>();
//            var confSection = new Mock<IConfigurationSection>();
//            confSection.Setup(x => x.Value).Returns(exampleSecret);

//            configuration.Setup(c => c.GetSection("TokenSecrets:JWT"))
//                .Returns(confSection.Object);

//            var sut = new TokenManager(configuration.Object);

//            var result = sut.ValidateToken(exampleToken2);

//            Assert.IsTrue(result);
//        }
//        [TestMethod]
//        public void ValidateToken_ReturnNull_WhenInvalidTokenIsPassed()
//        {
//            var exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
//            var validToken = @"invalid";

//            var configuration = new Mock<IConfiguration>();
//            var confSection = new Mock<IConfigurationSection>();
//            confSection.Setup(x => x.Value).Returns(exampleToken);
//            configuration.Setup(c => c.GetSection("TokenSecrets:JWT")).Returns(confSection.Object);

//            var sut = new TokenManager(configuration.Object);

//            var result = sut.ValidateToken(validToken);

//            Assert.IsFalse(result);
//        }
//    }
//}
