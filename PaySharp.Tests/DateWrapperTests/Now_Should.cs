﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaySharp.Services.Utilities.Wrapper;
using System;

namespace PaySharp.ServiceTests.DateWrapperTests
{
    [TestClass]
    public class Now_Should
    {
        [TestMethod]
        public void ReturnCorrectDateTimeType()
        {
            var sut = new DateWrapper();
            var res = sut.Now();

            Assert.IsInstanceOfType(res, typeof(DateTime));
        }
    }
}
