﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PaySharp.Data.DataContext;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaySharp.Tests
{
    public class TestUtils
    {
        public static DbContextOptions<PaySharpDBContext> GetOptions(string databaseName)
        {
            var provider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .BuildServiceProvider();

            return new DbContextOptionsBuilder<PaySharpDBContext>()
            .UseInMemoryDatabase(databaseName)
            .UseInternalServiceProvider(provider)
            .Options;
        }
    }
}
