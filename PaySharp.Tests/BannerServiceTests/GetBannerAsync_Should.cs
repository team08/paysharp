﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Services.Utilities.Wrapper;
using PaySharp.Tests;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.BannerServiceTests
{
    [TestClass]
    public class GetBannerAsync_Should
    {
        [TestMethod]
        public async Task IfBannersCountIsZero_ReturnNull()
        {
            var dateWrapper = new Mock<IDateWrapper>();
            var random = new Mock<IRandomGenerator>();
            var bannerMapper = new BannerDTOMapper();
            var options = TestUtils.GetOptions(nameof(IfBannersCountIsZero_ReturnNull));
            random.Setup(r => r.GenerateNumber(1, 2, 1)).Returns("1");
            var startDate = DateTime.ParseExact("10/10/2016", "MM/dd/yyyy", CultureInfo.InvariantCulture);
            dateWrapper.Setup(r => r.Now()).Returns(startDate);
            using (var context = new PaySharpDBContext(options))
            {
                await context.SaveChangesAsync();
            }

            using (var context = new PaySharpDBContext(options))
            {
                var sut = new BannerService(context, random.Object, bannerMapper,dateWrapper.Object);

                var res = await sut.GetBannerAsync();
                Assert.IsNull(res);
            }
        }

        [TestMethod]
        public async Task IfBannersExists_ReturnBannerDTO()
        {
            var dateWrapper = new Mock<IDateWrapper>();
            var random = new Mock<IRandomGenerator>();
            var bannerMapper = new BannerDTOMapper();
            var options = TestUtils.GetOptions(nameof(IfBannersExists_ReturnBannerDTO));
            random.Setup(r => r.GenerateNumber(0, 0, 1)).Returns("0");
            var startDate = DateTime.ParseExact("10/10/2016", "MM/dd/yyyy", CultureInfo.InvariantCulture);
            var endDate = DateTime.ParseExact("10/10/2020", "MM/dd/yyyy", CultureInfo.InvariantCulture);
            dateWrapper.Setup(r => r.Now()).Returns(startDate);

            var banner = new Banner()
            {
                Name = "name",
                StartDate=startDate,
                EndDate=endDate
            };

            using (var context = new PaySharpDBContext(options))
            {
                await context.AddAsync(banner);
                await context.SaveChangesAsync();
            }

            using (var context = new PaySharpDBContext(options))
            {
                var sut = new BannerService(context, random.Object, bannerMapper,dateWrapper.Object);

                var res = await sut.GetBannerAsync();
                Assert.IsInstanceOfType(res, typeof(BannerDTO));
                Assert.AreEqual("name", res.Name);
            }

        }
    }
}
