﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Services.Utilities.Wrapper;
using PaySharp.Tests;
using System;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.BannerServiceTests
{
    [TestClass]
    public class EditBanner_Should
    {
        [TestMethod]
        public async Task IfBannersIsEdited_ReturnBannerDTO()
        {
            var dateWrapper = new Mock<IDateWrapper>();
            var random = new Mock<IRandomGenerator>();
            var bannerMapper = new BannerDTOMapper();
            var options = TestUtils.GetOptions(nameof(IfBannersIsEdited_ReturnBannerDTO));

            var banner = new Banner()
            {
                Id = new System.Guid(),
                Name = "name",
                StartDate = new DateTime(2019, 2, 2),
                EndDate = new DateTime(2019, 3, 3)
            };

            using (var context = new PaySharpDBContext(options))
            {
                context.Add(banner);
                await context.SaveChangesAsync();
            }

            using (var context = new PaySharpDBContext(options))
            {
                var sut = new BannerService(context, random.Object, bannerMapper, dateWrapper.Object);

                var res = await sut.EditBanner(banner.Id.ToString(), new DateTime(2020, 4, 4), new DateTime(2021, 5, 5));
                Assert.AreEqual(new DateTime(2020, 4, 4), res.StartDate);
                Assert.AreEqual(new DateTime(2021, 5, 5), res.EndDate);
                Assert.IsInstanceOfType(res, typeof(BannerDTO));
            }
        }
    }
}
