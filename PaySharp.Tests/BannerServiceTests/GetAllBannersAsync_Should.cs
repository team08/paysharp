﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Services.Utilities.Wrapper;
using PaySharp.Tests;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.BannerServiceTests
{
    [TestClass]
    public class GetAllBannersAsync_Should
    {
        [TestMethod]
        public async Task WhenIsExecuted_ReturnListOfBannerDTO()
        {
            var dateWrapper = new Mock<IDateWrapper>();
            var random = new Mock<IRandomGenerator>();
            var bannerMapper = new BannerDTOMapper();
            var options = TestUtils.GetOptions(nameof(WhenIsExecuted_ReturnListOfBannerDTO));

            var banner = new Banner()
            {
                Id = new System.Guid(),
                Name = "name"
            };

            using (var arrangeContext = new PaySharpDBContext(options))
            {
                arrangeContext.Add(banner);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new PaySharpDBContext(options))
            {
                var sut = new BannerService(assertContext, random.Object, bannerMapper, dateWrapper.Object);

                var res = await sut.GetAllBannersAsync(1, 2);

                Assert.IsInstanceOfType(res, typeof(IEnumerable<BannerDTO>));
            }
        }
    }
}