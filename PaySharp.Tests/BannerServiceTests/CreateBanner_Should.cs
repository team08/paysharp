﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Services.Utilities.Wrapper;
using PaySharp.Tests;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.BannerServiceTests
{
    [TestClass]
    public class CreateBanner_Should
    {
        [TestMethod]
        public async Task IfNewBannerIsCreated_ReturnBannerDTO()
        {
            var dateWrapper = new Mock<IDateWrapper>();
            var random = new Mock<IRandomGenerator>();
            var bannerMapper = new BannerDTOMapper();
            var options = TestUtils.GetOptions(nameof(IfNewBannerIsCreated_ReturnBannerDTO));

            using (var context = new PaySharpDBContext(options))
            {
                var sut = new BannerService(context, random.Object, bannerMapper,dateWrapper.Object);

                var res = await sut.CreateBanner("name", "path", "url", new DateTime(2022, 1, 2), new DateTime(2023, 3, 3));
                Assert.IsInstanceOfType(res, typeof(BannerDTO));
                Assert.AreEqual(context.Banners.Count(), 1);
                Assert.AreEqual(res.Name, "name");
                Assert.AreEqual(res.ImgPath, "path");
                Assert.AreEqual(res.Url, "url");

                Assert.AreEqual(res.StartDate, new DateTime(2022, 1, 2));
                Assert.AreEqual(res.EndDate, new DateTime(2023, 3, 3));
            }
        }
    }
}
