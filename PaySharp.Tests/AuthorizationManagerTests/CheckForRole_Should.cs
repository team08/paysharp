﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.Contracts;
using PaySharp.Services.Exceptions;
using System.Collections.Generic;
using System.Security.Claims;

namespace PaySharp.ServiceTests.AuthorizationManagerTests
{
    [TestClass]
    public class CheckForRole_Should
    {
       

        [TestMethod]
        public void ThrowNoAccessException_WhenUserRoleIsNotAuthorized()
        {
            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("You do not have permissions for that!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NOT_AUTHORIZED")).Returns(confSection.Object);

            IList<Claim> eaxmpleClaimCollection = new List<Claim>
        {
            new Claim("userRole", "User")
        };
        ClaimsIdentity exampleClaimsIdentity = new ClaimsIdentity(eaxmpleClaimCollection);

        ClaimsPrincipal exampleClaimPrincipal = new ClaimsPrincipal(exampleClaimsIdentity);


        var exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            var tokenManagerMock = new Mock<ITokenManager>();

            tokenManagerMock.Setup(x => x.GetPrincipal(exampleToken)).Returns(exampleClaimPrincipal);

            var sut = new AuthorizationManager(tokenManagerMock.Object,configuration.Object);

            var ex = Assert.ThrowsException<NoAccessException>(() =>
                sut.CheckForRole(exampleToken, "Admin"));

            Assert.AreEqual(ex.Message, "You do not have permissions for that!");
        }

        [TestMethod]
        public void ThrowNoAccessException_WhenTokenValueIsNull()
        {
            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("You do not have permissions for that!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NOT_AUTHORIZED")).Returns(confSection.Object);

            var tokenManagerMock = new Mock<ITokenManager>();

            var sut = new AuthorizationManager(tokenManagerMock.Object, configuration.Object);

            var ex = Assert.ThrowsException<NoAccessException>(() =>
                sut.CheckForRole(null, "Admin"));

            Assert.AreEqual(ex.Message, "You do not have permissions for that!");
        }
    }
}
