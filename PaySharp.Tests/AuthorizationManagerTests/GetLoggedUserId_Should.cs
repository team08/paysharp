﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.Contracts;
using PaySharp.Services.Exceptions;
using System.Collections.Generic;
using System.Security.Claims;

namespace PaySharp.ServiceTests.AuthorizationManagerTests
{
    [TestClass]
    public class GetLoggedUserId_Should
    {
        [TestMethod]
        public void GetCorrectUserId()
        {
            var tokenManagerMock = new Mock<ITokenManager>();

            var eaxmpleClaimCollection = new List<Claim>
            {
                new Claim("userRole", "User"),
                new Claim("userId", "1")
            };

            var configuration = new Mock<IConfiguration>();

            var exampleClaimsIdentity = new ClaimsIdentity(eaxmpleClaimCollection);

            var exampleClaimPrincipal = new ClaimsPrincipal(exampleClaimsIdentity);

            var exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            tokenManagerMock.Setup(x => x.GetPrincipal(It.IsAny<string>())).Returns(exampleClaimPrincipal);

            var sut = new AuthorizationManager(tokenManagerMock.Object,configuration.Object);

            var result = sut.GetLoggedUserId(exampleToken);

            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void ThrowNoAccessException_WhenNoUserLogged()
        {
            string exampleToken = @"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            var configuration = new Mock<IConfiguration>();
            var confSection = new Mock<IConfigurationSection>();
            confSection.Setup(x => x.Value).Returns("You must be logged in to view that!");
            configuration.Setup(c => c.GetSection("GlobalConstants:NOT_LOGGED")).Returns(confSection.Object);

            var tokenManagerMock = new Mock<ITokenManager>();

            tokenManagerMock.Setup(x => x.GetPrincipal(exampleToken)).Returns(value: null);

            var sut = new AuthorizationManager(tokenManagerMock.Object,configuration.Object);

            var ex = Assert.ThrowsException<NoAccessException>(() =>
                sut.GetLoggedUserId(exampleToken));

            Assert.AreEqual(ex.Message, "You must be logged in to view that!");
        }
    }
}
