﻿using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Data.DataContext;
using PaySharp.Data.Entities;
using PaySharp.Services;
using PaySharp.Services.DTO;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Tests;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.ServiceTests.HashPasswordTests
{
    [TestClass]
    public class GetHashString_Should
    {
        [TestMethod]
        public void ReturnsHashString()
        {
            //arrange
            //act,assert
            var sut = new PasswordHasher();
            var res = sut.GetHashString("test");
            Assert.AreEqual(res, 
                "9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08");
        }
    }
}
