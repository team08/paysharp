﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using PaySharp.Services.Contracts;
using PaySharp.Services.Exceptions;
using PaySharp.Web.ViewModels;
using System;
using System.Threading.Tasks;

namespace PaySharp.Web.Controllers
{
    public class IdentityController : Controller
    {
        private readonly ITokenManager tokenManager;
        private readonly ICookieManager cookieManager;
        private readonly IMemoryCache cacheService;
        private readonly IUserService userService;
        private readonly IBannerService bannerService;

        public IdentityController(IMemoryCache cacheService, IUserService userService, IBannerService bannerService, ITokenManager tokenManager,
              ICookieManager cookieManager)
        {
            this.cacheService = cacheService ?? throw new ArgumentNullException(nameof(cacheService));
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.bannerService = bannerService ?? throw new ArgumentNullException(nameof(bannerService));
            this.tokenManager = tokenManager ?? throw new ArgumentNullException(nameof(tokenManager));
            this.cookieManager = cookieManager ?? throw new ArgumentNullException(nameof(cookieManager));
        }

        [HttpGet]
        public async Task<IActionResult> Login()
        {
            var banner = await GetBanner();
            
            return View(banner);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel userModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(userModel);
                }

                var user = await userService.GetUserAsync(userModel.UserName, userModel.Password);

                var token = tokenManager.GenerateToken(user.UserName, user.Role, user.UserId.ToString());

                cookieManager.AddSessionCookieForToken(token, user.UserName);

                return RedirectToAction("Index", "Dashboard", new { area = "User" });


            }
            catch (EntityNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpGet]
        public IActionResult Logout()
        {

            cookieManager.DeleteSessionCookies();

            return RedirectToAction("Login", "Identity");
        }
        private async Task<BannerViewModel> GetBanner()
        {
            var cacheTeams = await cacheService.GetOrCreateAsync("Data", async entry =>
            {
                entry.AbsoluteExpiration = DateTime.UtcNow.AddSeconds(30);
                var banner = await bannerService.GetBannerAsync();
                if (banner == null)
                {
                    var vmNUll = new BannerViewModel()
                    {
                        Id = "no banner",
                        Name = "no banner",
                        Url = "",
                        ImgPath = "test.jpg"

                    };
                    return vmNUll;
                }
                var bannerVM = new BannerViewModel()
                {
                    Id = banner.Id.ToString(),
                    Name = banner.Name,
                    Url = banner.Url,
                    ImgPath = banner.ImgPath,
                    StartDate = banner.StartDate.ToString("dd/MM/yyyy"),
                    EndDate = banner.EndDate.ToString("dd/MM/yyyy")
                };
                return bannerVM;
            });
            return cacheTeams;
        }
    }
}