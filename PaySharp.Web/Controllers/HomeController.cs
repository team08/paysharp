﻿using Microsoft.AspNetCore.Mvc;

namespace PaySharp.Web.Controllers
{
    public class HomeController : Controller
    {
  
        public IActionResult Invalid()
        {
            return View();
        }

        public IActionResult ServerError()
        {
            return View();
        }
        public IActionResult NoAccess()
        {
            return View();
        }
    }
}
