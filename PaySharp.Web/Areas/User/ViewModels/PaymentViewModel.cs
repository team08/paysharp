﻿using PaySharp.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Areas.User.ViewModels
{
    public class PaymentViewModel
    {
        public IEnumerable<AccountViewModel> Accounts { get; set; }

        public TransactionViewModel Transaction { get; set; }
    }
}
