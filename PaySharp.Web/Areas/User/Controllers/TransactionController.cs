﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Mappers;
using PaySharp.Web.Utilities;
using PaySharp.Web.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Areas.User.Controllers
{
    [Area("User")]
    [CustomAuthorize("User")]
    public class TransactionController : Controller
    {
        private readonly ITransactionService transactionService;
        private readonly IAccountService accountService;
        private readonly IAuthorizationManager authorizationManager;
        private readonly IHttpContextAccessor contextAccessor;
        private readonly IViewModelMapper<TransactionDTO, TransactionViewModel> transactionMapper;
        private readonly IViewModelMapper<AccountDTO, AccountViewModel> accountMapper;

        public TransactionController(ITransactionService transactionService,
            IAccountService accountService,
            IAuthorizationManager authorizationManager,
            IHttpContextAccessor contextAccessor,
            IViewModelMapper<TransactionDTO, TransactionViewModel> transactionMapper,
            IViewModelMapper<AccountDTO, AccountViewModel> accountMapper)
        {
            this.transactionService = transactionService ?? throw new ArgumentNullException(nameof(transactionService));
            this.accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            this.authorizationManager = authorizationManager ?? throw new ArgumentNullException(nameof(authorizationManager));
            this.contextAccessor = contextAccessor ?? throw new ArgumentNullException(nameof(contextAccessor));
            this.transactionMapper = transactionMapper ?? throw new ArgumentNullException(nameof(transactionMapper));
            this.accountMapper = accountMapper ?? throw new ArgumentNullException(nameof(accountMapper));
        }


        [HttpGet]
        public async Task<IActionResult> List(int? currentPage, long? accountId)
        {
            var token = contextAccessor.HttpContext.Request.Cookies["SecurityToken"];
            var currentUserId = authorizationManager.GetLoggedUserId(token);
            var userAccounts = await accountService.GetUserAccountsAsync(currentUserId);
            var accountsViewModels = userAccounts.Select(a => accountMapper.MapFrom(a));
            if (accountId == null)
            {
                var allUserTransactions = await transactionService.GetAllUserTransactions(currentUserId, currentPage ?? 1);
                var transactionsViewModel = allUserTransactions.Select(t => transactionMapper.MapFrom(t));
                var count = await transactionService.GetTransactionsCount(currentUserId, null);
                var vm = new PaginationTransactionsViewModel()
                {
                    CurrentPage = currentPage ?? 1,
                    NextPage = (currentPage ?? 1) + 1,
                    PrevPage = (currentPage ?? 1) - 1,
                    Count = count,
                    Data = transactionsViewModel,
                    UserAccounts = accountsViewModels,

                };

                return View(vm);
            }
            else
            {
                var accountTransactions = await transactionService
                    .GetUserTransactionsByAccount(accountId ?? 1, currentUserId, currentPage ?? 1);
                var transactionsViewModel = accountTransactions.Select(t => transactionMapper.MapFrom(t));
                var accountPreSelected = await accountService.GetAccountAsync((long)accountId);
                var accountViewModel = accountMapper.MapFrom(accountPreSelected);
                var count = await transactionService.GetTransactionsCount(currentUserId, accountId);
                var vm = new PaginationTransactionsViewModel()
                {
                    CurrentPage = currentPage ?? 1,
                    NextPage = (currentPage ?? 1) + 1,
                    PrevPage = (currentPage ?? 1) - 1,
                    Count = count,
                    Data = transactionsViewModel,
                    UserAccounts = accountsViewModels,
                    PreSelectedAccount = accountViewModel
                };

                return View(vm);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetTransactions(int? currentPage, long? accountId)
        {
            var token = contextAccessor.HttpContext.Request.Cookies["SecurityToken"];
            var currentUserId = authorizationManager.GetLoggedUserId(token);
            var userAccounts = await accountService.GetUserAccountsAsync(currentUserId);
            var accountsViewModels = userAccounts.Select(a => accountMapper.MapFrom(a));
            if (accountId == null)
            {
                var allUserTransactions = await transactionService.GetAllUserTransactions(currentUserId, currentPage ?? 1);
                var transactionsViewModel = allUserTransactions.Select(t => transactionMapper.MapFrom(t));
                var count = await transactionService.GetTransactionsCount(currentUserId, null);
                var vm = new PaginationTransactionsViewModel()
                {
                    CurrentPage = currentPage ?? 1,
                    NextPage = (currentPage ?? 1) + 1,
                    PrevPage = (currentPage ?? 1) - 1,
                    Count = count,
                    Data = transactionsViewModel,
                    UserAccounts = accountsViewModels,

                };

                return PartialView("Transactions/_AllUserTransactionsPartial", vm);
            }
            else
            {
                var accountTransactions = await transactionService
                    .GetUserTransactionsByAccount(accountId ?? 1, currentUserId, currentPage ?? 1);
                var transactionsViewModel = accountTransactions.Select(t => transactionMapper.MapFrom(t));
                var accountPreSelected = await accountService.GetAccountAsync((long)accountId);
                var accountViewModel = accountMapper.MapFrom(accountPreSelected);
                var count = await transactionService.GetTransactionsCount(currentUserId, accountId);
                var vm = new PaginationTransactionsViewModel()
                {
                    CurrentPage = currentPage ?? 1,
                    NextPage = (currentPage ?? 1) + 1,
                    PrevPage = (currentPage ?? 1) - 1,
                    Count = count,
                    Data = transactionsViewModel,
                    UserAccounts = accountsViewModels,
                    PreSelectedAccount = accountViewModel
                };

                return PartialView("Transactions/_AllUserTransactionsPartial", vm);
            }

        }


        [HttpPost]
        public async Task<IActionResult> SendTransaction(TransactionViewModel model)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Index", "Dashboard", new { area = "User" });
                }
                var senderAccountNumber = string.Empty;
                if (model.SenderAccount == null)
                {
                    senderAccountNumber = model.SenderAccountID.ToString();
                }
                else
                {
                    senderAccountNumber = model.SenderAccount.AccountNumber;
                }
                var transaction = await transactionService.SendTransactionAsync(
                    senderAccountNumber,
                    model.ReceiverAccount.AccountNumber,
                    model.Description,
                    model.Amount);

                return Ok($"Transaction between {senderAccountNumber} and {model.ReceiverAccount.AccountNumber} was made successfully");
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SendSavedTransactions(long transactionId)
        {


            var transaction = await transactionService.SendSavedTransactionAsync(transactionId);


            return Ok($"Transaction between {transaction.SenderAccount.AccountNumber} " +
                $"and {transaction.ReceiverAccount.AccountNumber} was made successfully");

        }

        [HttpPost]
        public async Task<IActionResult> SaveTransaction(TransactionViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Index", "Dashboard", new { area = "User" });
                }
                var senderAccountNumber = string.Empty;
                if (model.SenderAccount == null)
                {
                    senderAccountNumber = model.SenderAccountID.ToString();
                }
                else
                {
                    senderAccountNumber = model.SenderAccount.AccountNumber;
                }
                var transaction = await transactionService.SaveTransactionAsync(
                    senderAccountNumber,
                    model.ReceiverAccount.AccountNumber,
                    model.Description,
                    model.Amount);

                return Ok($"Transaction between {senderAccountNumber} and {model.ReceiverAccount.AccountNumber} was made safed!");
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public async Task<IActionResult> EditTransaction(TransactionViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Index", "Dashboard", new { area = "User" });
                }

                var transaction = await transactionService.EditTransactionAsync(
                    model.SenderAccount.AccountNumber,
                    model.ReceiverAccount.AccountNumber,
                    model.Description,
                    model.Amount,
                    model.Id);

                return Ok($"Transaction was made modified and saved!");
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public async Task<IActionResult> MakePayment()
        {
            var token = contextAccessor.HttpContext.Request.Cookies["SecurityToken"];
            var currentUserId = authorizationManager.GetLoggedUserId(token);
            var userAccounts = await accountService.GetUserAccountsAsync(currentUserId);
            var accountsViewModels = userAccounts.Select(a => accountMapper.MapFrom(a));

            var vm = new TransactionViewModel
            {
                Accounts = accountsViewModels
            };
            return View(vm);
        }
    }
}