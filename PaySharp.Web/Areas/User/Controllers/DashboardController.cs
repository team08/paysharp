﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Mappers;
using PaySharp.Web.Utilities;
using PaySharp.Web.ViewModels;


namespace PaySharp.Web.Areas.User.Controllers
{
    [Area("User")]
    [CustomAuthorize("User")]
    public class DashboardController : Controller
    {
        private readonly IAuthorizationManager authorizationManager;
        private readonly IHttpContextAccessor contextAccessor;
        private readonly IAccountService accountService;
        private readonly IViewModelMapper<AccountDTO, AccountViewModel> accountMapper;

        public DashboardController(IAuthorizationManager authorizationManager,
            IHttpContextAccessor contextAccessor,
            IAccountService accountService,
            IViewModelMapper<AccountDTO, AccountViewModel> accountMapper)
        {
            this.authorizationManager = authorizationManager ?? throw new ArgumentNullException(nameof(authorizationManager));
            this.contextAccessor = contextAccessor ?? throw new ArgumentNullException(nameof(contextAccessor));
            this.accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            this.accountMapper = accountMapper ?? throw new ArgumentNullException(nameof(accountMapper));
        }
        public async Task<IActionResult> Index()
        {
            
            var token = contextAccessor.HttpContext.Request.Cookies["SecurityToken"];
            var currentUserId = authorizationManager.GetLoggedUserId(token);
            var allUserAccounts = await accountService.GetUserAccountsAsync(currentUserId);
            var vm = allUserAccounts.Select(a => accountMapper.MapFrom(a)).ToList();
          
            return View(vm);
        }
       
    }
}