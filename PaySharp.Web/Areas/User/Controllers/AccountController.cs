﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Mappers;
using PaySharp.Web.Utilities;
using PaySharp.Web.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;


namespace PaySharp.Web.Areas.User.Controllers
{
    [Area("User")]
    [CustomAuthorize("User")]
    public class AccountController : Controller
    {
        private readonly IAuthorizationManager authorizationManager;
        private readonly IHttpContextAccessor contextAccessor;
        private readonly IAccountService accountService;
        private readonly IViewModelMapper<AccountDTO, AccountViewModel> accountMapper;

        public AccountController(IAuthorizationManager authorizationManager,
            IHttpContextAccessor contextAccessor,
            IAccountService accountService,
            IViewModelMapper<AccountDTO, AccountViewModel> accountMapper)
        {
            this.authorizationManager = authorizationManager ?? throw new ArgumentNullException(nameof(authorizationManager));
            this.contextAccessor = contextAccessor ?? throw new ArgumentNullException(nameof(contextAccessor));
            this.accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            this.accountMapper = accountMapper ?? throw new ArgumentNullException(nameof(accountMapper));
        }
        [HttpPost]
        public async Task<IActionResult> Rename(long accountId, string newNickName)
        {
            var account = await accountService.RenameAccountAsync(accountId, newNickName);
            return Ok($"The account nickname was renamed to {newNickName}!");
        }

        [HttpGet]
        public async Task<IActionResult> GetUserAccounts()
        {
            var token = contextAccessor.HttpContext.Request.Cookies["SecurityToken"];
            var currentUserId = authorizationManager.GetLoggedUserId(token);
            var allUserAccounts = await accountService.GetUserAccountsAsync(currentUserId);
            var vm = allUserAccounts.Select(a => accountMapper.MapFrom(a)).ToList();

            return PartialView("Accounts/_AllUserAccountsPartial", vm);
        }
        [HttpGet]
        public async Task<IActionResult> GetAllUserAccounts()
        {
            var token = contextAccessor.HttpContext.Request.Cookies["SecurityToken"];
            var currentUserId = authorizationManager.GetLoggedUserId(token);
            var allUserAccounts = await accountService.GetUserAccountsAsync(currentUserId);
            var accounts = allUserAccounts.Select(a => accountMapper.MapFrom(a)).ToList();

            return Json(accounts);
        }


        [HttpPost]
        public async Task<JsonResult> FindSingleByAccName(string prefix)
        {

            if (string.IsNullOrEmpty(prefix))
            {
                return Json(string.Empty);
            }

            var allAccountsContaining = await accountService.FindAccountsContainingAsync(prefix);

            var allAccountsModels = allAccountsContaining.Select(a => accountMapper.MapFrom(a));

            return Json(allAccountsModels);

        }
    }
}