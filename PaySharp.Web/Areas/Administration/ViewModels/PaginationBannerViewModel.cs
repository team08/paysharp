﻿using PaySharp.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Areas.Administration.ViewModels
{
    public class PaginationBannerViewModel
    {
        public int CurrentPage { get; set; } = 1;
        public int PrevPage { get; set; }
        public int NextPage { get; set; }
        public long Count { get; set; }
        public int PageSize { get; set; } = 6;

        public int TotalPages => (int)Math.Ceiling(decimal.Divide(Count, PageSize));

        public IEnumerable<BannerViewModel> Banners { get; set; }
    }
}
