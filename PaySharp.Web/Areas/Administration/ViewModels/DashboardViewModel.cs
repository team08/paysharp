﻿using PaySharp.Web.ViewModels;
using System.Collections.Generic;

namespace PaySharp.Web.Areas.Administration.ViewModels
{
    public class DashboardViewModel
    {
       public PaginationClientViewModel Clients { get; set; }

        public PaginationUserViewModel Users { get; set; }
    }
}
