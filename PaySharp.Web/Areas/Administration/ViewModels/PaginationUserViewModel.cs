﻿using PaySharp.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Areas.Administration.ViewModels
{
    public class PaginationUserViewModel
    {
        public int CurrentPage { get; set; } = 1;
        public int PrevPage { get; set; }
        public int NextPage { get; set; }
        public long Count { get; set; }
        public int PageSize { get; set; } = 5;

        public int TotalPages => (int)Math.Ceiling(decimal.Divide(Count, PageSize));

        public IEnumerable<UserViewModel> Users { get; set; }
    }
}
