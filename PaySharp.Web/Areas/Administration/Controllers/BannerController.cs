﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.Administration.ViewModels;
using PaySharp.Web.Mappers;
using PaySharp.Web.Utilities;
using PaySharp.Web.ViewModels;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Areas.Administration.Controllers
{
    [Area("Administration")]
    [CustomAuthorize("Admin")]
    public class BannerController : Controller
    {
        private readonly IBannerService bannerService;
        private readonly IViewModelMapper<BannerDTO, BannerViewModel> bannerMapper;
        private readonly IHostingEnvironment hostingEnvironment;

        public BannerController(IBannerService bannerService, IViewModelMapper<BannerDTO, BannerViewModel> bannerMapper, IHostingEnvironment hostingEnvironment)
        {
            this.bannerService = bannerService ?? throw new ArgumentNullException(nameof(bannerService));
            this.bannerMapper = bannerMapper ?? throw new ArgumentNullException(nameof(bannerMapper));
            this.hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
        }
        [HttpGet]
        public async Task<IActionResult> Index(int? currentPage)
        {

            var bannersDTO = await bannerService.GetAllBannersAsync(currentPage ?? 1);
            var banners = bannersDTO.Select(b => bannerMapper.MapFrom(b));
            var count = await bannerService.GetCount();
            var vm = new PaginationBannerViewModel()
            {
                CurrentPage = currentPage ?? 1,
                NextPage = (currentPage ?? 1) + 1,
                PrevPage = (currentPage ?? 1) - 1,
                Count = count,
                PageSize = 6,
                Banners = banners
            };

            return View(vm);
        }

        //TODO
        [HttpGet]
        public async Task<IActionResult> Delete(string id)
        {
            var uploads = Path.Combine(this.hostingEnvironment.WebRootPath, "banners");
            var banner = await bannerService.DeleteBanner(id, uploads);

            return RedirectToAction("Index", "Banner", new { area = "Administration" });
        }
        [HttpPost]
        public async Task<IActionResult> Edit(string id, DateTime startDate, DateTime endDate)
        {
            var banner = await bannerService.EditBanner(id, startDate, endDate);

            return RedirectToAction("Index", "Banner", new { area = "Administration" });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateBanner(CreateBannerViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var imageNameToSave = $"{model.Name}" + "_banner.jpg";
                var url = "https://www." + model.Url;

                var dates = model.DateRange.Split(" - ");
                var startDate = DateTime.ParseExact(dates[0], "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var endDate = DateTime.ParseExact(dates[1], "MM/dd/yyyy", CultureInfo.InvariantCulture);
                using (var ms = new MemoryStream())
                {
                    model.Image.CopyTo(ms);
                    var uploads = Path.Combine(this.hostingEnvironment.WebRootPath, "banners");
                    var filePath = Path.Combine(uploads, imageNameToSave);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await model.Image.CopyToAsync(fileStream);
                    }
                    var banner = await this.bannerService.CreateBanner(model.Name, imageNameToSave, url, startDate, endDate);
                }

                return RedirectToAction("Index", "Banner", new { area = "Administration" });
            }
            ModelState.AddModelError(string.Empty, $"Invalid banner!");
            return RedirectToAction("Index", "Banner", new { area = "Administration", model });

        }
    }
}