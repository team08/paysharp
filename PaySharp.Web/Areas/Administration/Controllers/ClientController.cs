﻿using Microsoft.AspNetCore.Mvc;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.ViewModels;
using PaySharp.Web.Mappers;
using PaySharp.Web.Utilities;
using PaySharp.Web.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Areas.Administration.Controllers
{
    [Area("Administration")]
    [CustomAuthorize("Admin")]
    public class ClientController : Controller
    {
        private readonly IClientService clientService;
        private readonly IViewModelMapper<ClientDTO, ClientViewModel> clientMapper;
        private readonly IViewModelMapper<AccountDTO, AccountViewModel> accountMapper;
        private readonly IViewModelMapper<UserDTO, UserViewModel> userMapper;

        public ClientController(IClientService clientService,
            IViewModelMapper<ClientDTO, ClientViewModel> clientMapper,
            IViewModelMapper<AccountDTO, AccountViewModel> accountMapper,
            IViewModelMapper<UserDTO, UserViewModel> userMapper)

        {
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
            this.clientMapper = clientMapper ?? throw new ArgumentNullException(nameof(clientMapper));
            this.accountMapper = accountMapper ?? throw new ArgumentNullException(nameof(accountMapper));
            this.userMapper = userMapper ?? throw new ArgumentNullException(nameof(userMapper));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateClient(ClientViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {

                    return RedirectToAction("Index", "Dashboard", new { area = "Administration" });
                }

                var client = await clientService.CreateClientAsync(model.Name);

                return Ok($"Client with name {model.Name} was added successfully");
            }
            catch (EntityAlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }


        }
        [HttpGet]
        public async Task<IActionResult> ClientDetails(long id)
        {
            var client = await clientService.GetClientAsync(id);
            if (client == null)
            {
                return NotFound();
            }
            var vm = new ClientViewModel()
            {
                Id = client.ClientId,
                Name = client.Name,
                Accounts = client.Accounts.Select(a => accountMapper.MapFrom(a)).ToList(),
                Users = client.UsersClients.Select(uc => userMapper.MapFrom(uc.User)).ToList()
            };
            return View(vm);
        }

        [HttpGet]
        public async Task<IActionResult> ShowAllClients(int? currentPage)
        {
            var clients = await clientService.TakeNumberOfClientsAsync(currentPage ?? 1);
            var allClients = clients.Select(c => clientMapper.MapFrom(c));
            var count = await clientService.GetAllClientsCountAsync();

            var vm = new PaginationClientViewModel()
            {
                CurrentPage = currentPage ?? 1,
                NextPage = (currentPage ?? 1) + 1,
                PrevPage = (currentPage ?? 1) - 1,
                Count = count,
                Clients = allClients
            };

            return PartialView("Clients/_ShowClientsPartial", vm);
        }

        [HttpGet]
        public async Task<IActionResult> GetClientUsers(long id)
        {
            var client = await clientService.GetClientAsync(id);
            var vm = new ClientViewModel()
            {
                Users = client.UsersClients.Select(uc => userMapper.MapFrom(uc.User)).ToList()
            };
            return PartialView("Users/_ShowClientUsersPartial", vm.Users);
        }

        [HttpPost]
        public async Task<IActionResult> GetClient(string searchName)
        {
            try
            {
                var client = await clientService.GetClientAsync(searchName);
                var vm = new ClientViewModel()
                {
                    Id = client.ClientId,
                    Name = client.Name,
                    Accounts = client.Accounts.Select(a => accountMapper.MapFrom(a)).ToList(),
                    Users = client.UsersClients.Select(uc => userMapper.MapFrom(uc.User)).ToList()
                };

                return View("../Client/ClientDetails", vm);
            }
            catch (EntityNotFoundException ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<JsonResult> FindClientbyName(string prefix)
        {

            if (string.IsNullOrEmpty(prefix))
            {
                return Json(string.Empty);
            }

            var allClientsContaining = await clientService.FindClientContainingAsync(prefix);

            return Json(allClientsContaining);
        }
    }
}