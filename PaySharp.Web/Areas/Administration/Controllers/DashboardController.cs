﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.ViewModels;
using PaySharp.Web.Mappers;
using PaySharp.Web.Utilities;
using PaySharp.Web.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Areas.Administration.Controllers
{
    [Area("Administration")]
    public class DashboardController : Controller
    {

        private readonly IUserService userService;
        private readonly IClientService clientService;
        private readonly ITokenManager tokenManager;
        private readonly IViewModelMapper<ClientDTO, ClientViewModel> clientMapper;
        private readonly ICookieManager cookieManager;
        private readonly IViewModelMapper<UserDTO, UserViewModel> userMapper;

        public DashboardController(IUserService userService, IClientService clientService, ITokenManager tokenManager,
            IViewModelMapper<ClientDTO, ClientViewModel> clientMapper, ICookieManager cookieManager,
            IViewModelMapper<UserDTO, UserViewModel> userMapper)

        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.clientService = clientService ?? throw new ArgumentNullException(nameof(clientService));
            this.tokenManager = tokenManager ?? throw new ArgumentNullException(nameof(tokenManager));
            this.clientMapper = clientMapper ?? throw new ArgumentNullException(nameof(clientMapper));
            this.cookieManager = cookieManager ?? throw new ArgumentNullException(nameof(cookieManager));
            this.userMapper = userMapper ?? throw new ArgumentNullException(nameof(userMapper));
        }


        [HttpGet]
        [CustomAuthorize("Admin")]
        public async Task<IActionResult> Index()
        {
            var clients = await clientService.TakeNumberOfClientsAsync(1);
            var allClients = clients.Select(c => clientMapper.MapFrom(c));
            var countClients = await clientService.GetAllClientsCountAsync();

            var vmClients = new PaginationClientViewModel()
            {
                CurrentPage = 1,
                NextPage = 2,
                PrevPage = 0,
                Count = countClients,
                Clients = allClients
            };
            var users = await userService.TakeNumberOfUsersAsync(1);
            var allusers = users.Select(c => userMapper.MapFrom(c));
            var countUsers = await userService.GetAllUsersCountAsync();

            var vmUsers = new PaginationUserViewModel()
            {
                CurrentPage = 1,
                NextPage = 2,
                PrevPage = 0,
                Count = countUsers,
                Users = allusers
            };

            var dashboardModel = new DashboardViewModel()
            {
                Clients = vmClients,
                Users = vmUsers
            };

            return View(dashboardModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel userModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(userModel);
                }

                var admin = await userService.GetAdminAsync(userModel.UserName, userModel.Password);

                var token = tokenManager.GenerateToken(admin.UserName, admin.Role, admin.Id.ToString());

                cookieManager.AddSessionCookieForToken(token, admin.UserName);

                return RedirectToAction("Index", "Dashboard", new { area = "Administration" });
            }
            catch (EntityNotFoundException ex)
            {
                ModelState.AddModelError(string.Empty, $"Invalid login attempt.{ex.Message}");
                return View(userModel);
            }

        }
        [HttpGet]
        [CustomAuthorize("Admin")]
        public IActionResult Logout()
        {

            cookieManager.DeleteSessionCookies();

            return RedirectToAction("Login", "Dashboard", new { area = "Administration" });
        }

    }
}