﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.ViewModels;
using PaySharp.Web.Mappers;
using PaySharp.Web.Utilities;
using PaySharp.Web.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Areas.Administration.Controllers
{
    [Area("Administration")]
    [CustomAuthorize("Admin")]
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly IViewModelMapper<ClientDTO, ClientViewModel> clientMapper;
        private readonly IViewModelMapper<AccountDTO, AccountViewModel> accountMapper;
        private readonly IViewModelMapper<UserDTO, UserViewModel> userMapper;

        public UserController(IUserService userService,
            IViewModelMapper<ClientDTO, ClientViewModel> clientMapper,
            IViewModelMapper<AccountDTO, AccountViewModel> accountMapper,
            IViewModelMapper<UserDTO, UserViewModel> userMapper)
        {
            this.userService = userService ?? throw new ArgumentNullException(nameof(userService));
            this.clientMapper = clientMapper ?? throw new ArgumentNullException(nameof(clientMapper));
            this.accountMapper = accountMapper ?? throw new ArgumentNullException(nameof(accountMapper));
            this.userMapper = userMapper ?? throw new ArgumentNullException(nameof(userMapper));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateUser(UserViewModel userModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Index", "Dashboard", new { area = "Administration" });
                }

                var user = await userService.AddUserAsync(userModel.Name, userModel.UserName, userModel.Password);

                return Ok($"User with name {userModel.Name} was created successfully");
            }
            catch (EntityAlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateUserToClient(UserViewModel userModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return RedirectToAction("Index", "Dashboard", new { area = "Administration" });
                }
                var user = await userService.AddUserAsync(userModel.Name, userModel.UserName, userModel.Password);
                var userAdded = await userService.AddUserToClientAsync(user.UserId, userModel.ClientId);
                return Ok($"User with name {userModel.Name} was created successfully");
            }
            catch (EntityAlreadyExistsException ex)
            {
                return BadRequest(ex.Message);
            }

        }
        [HttpPost]
        public async Task<IActionResult> AddUserToClient(long userId, string clientName)
        {
            var user = await userService.AddUserToClientAsync(userId, clientName);

            return RedirectToAction("UserDetails", "User", new { Id = user.UserId, area = "Administration" });
        }


        [HttpGet]
        public async Task<IActionResult> ShowAllUsers(int? currentPage)
        {

            var users = await userService.TakeNumberOfUsersAsync(currentPage ?? 1);
            var allusers = users.Select(c => userMapper.MapFrom(c));
            var count = await userService.GetAllUsersCountAsync();

            var vm = new PaginationUserViewModel()
            {
                CurrentPage = currentPage ?? 1,
                NextPage = (currentPage ?? 1) + 1,
                PrevPage = (currentPage ?? 1) - 1,
                Count = count,
                Users = allusers
            };

            return PartialView("Users/_AllUsersPartial", vm);
        }

        [HttpPost]
        public async Task<IActionResult> GetUser(string searchName)
        {
            try
            {
                var user = await userService.GetUserAsync(searchName);
                var vm = new UserViewModel()
                {
                    Id = user.UserId,
                    Name = user.Name,
                    UserName = user.UserName,
                    Accounts = user.Accounts.Select(a => accountMapper.MapFrom(a)).ToList(),
                    Clients = user.Clients.Select(c => clientMapper.MapFrom(c)).ToList()
                };
                return View("../User/UserDetails", vm);
            }
            catch (EntityNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> UserDetails(long Id)
        {
            try
            {
                var user = await userService.GetUserAsync(Id);
                var vm = new UserViewModel()
                {
                    Id = user.UserId,
                    Name = user.Name,
                    UserName = user.UserName,
                    Accounts = user.Accounts.Select(a => accountMapper.MapFrom(a)).ToList(),
                    Clients = user.Clients.Select(c => clientMapper.MapFrom(c)).ToList()
                };
                return View(vm);
            }
            catch (EntityNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public async Task<JsonResult> FindUserByName(string prefix)
        {
            if (string.IsNullOrEmpty(prefix))
            {
                return Json(string.Empty);
            }

            var allUsersContaining = await userService.FindUserContainingAsync(prefix);
         
            return Json(allUsersContaining);
        }
    }
}