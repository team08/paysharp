﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Mappers;
using PaySharp.Web.Utilities;
using PaySharp.Web.ViewModels;

namespace PaySharp.Web.Areas.Administration.Controllers
{
    [Area("Administration")]
    [CustomAuthorize("Admin")]
    public class AccountController : Controller
    {
        private readonly IAccountService accountService;
        private readonly IViewModelMapper<AccountDTO, AccountViewModel> accountMapper;

        public AccountController(IAccountService accountService,
            IViewModelMapper<AccountDTO, AccountViewModel> accountMapper)
        {
            this.accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            this.accountMapper = accountMapper ?? throw new ArgumentNullException(nameof(accountMapper));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string id)
        {
            try
            {
                var clientId = long.Parse(id);
                var account = await accountService.CreateAccountAsync(clientId);
               
                return Ok("Account created");
            }
            catch (EntityAlreadyExistsException ex)
            {

                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetUserAccounts(UserViewModel user)
        {
            var accounts = await accountService.GetUserClientsAccountsAsync(user.Id);
            var vm = accounts.Select(a => accountMapper.MapFrom(a)).ToList();
            for (int i = 0; i < vm.Count(); i++)
            {
                vm[i].UserId= user.Id;
            }
           
            return PartialView("Accounts/_AllClientAccountsPartial", vm);
        }

        [HttpGet]
        public async Task<IActionResult> GetClientAccounts(long id)
        {
            var accounts = await accountService.GetClientAccountsAsync(id);
            var vm = accounts.Select(a => accountMapper.MapFrom(a)).ToList();
            return PartialView("Accounts/_ShowClientAccountsPartial", vm);
        }

        
        [HttpGet]
        public async Task<IActionResult> AddAccountToUser(AccountViewModel account)
        {
            var res = await accountService.AddAccountToUserAsync(account.UserId, account.Id);
            return Ok("Account added to user");
        }

        [HttpGet]
        public async Task<IActionResult> RemoveAccountFromUser(AccountViewModel account)
        {
            var res = await accountService.RemoveAccountFromUserAsync(account.UserId, account.Id);
            return Ok("Account removed from user");
        }
    }
}