﻿//UserPage

//payment separate page
$('#safe-transaction').live('click', function (e) {
    clickedButton = $(this).attr('id');

    urlToSubmitTransaction = "/User/Transaction/SaveTransaction";
})
$('#send-transaction').live('click', function (e) {
    clickedButton = $(this).attr('id');
    urlToSubmitTransaction = "/User/Transaction/SendTransaction";
})

$(document).ready(function () {
    $('#sender-account-selector').select2();
});


$('#sender-account-selector').on('select2:select', function (e) {
    var selectedClientName = $('#sender-account-selector option:selected').attr('att');
    $('#sender-client-name-div').empty();
    $('#sender-client-name-div').append("Client Name: " + selectedClientName);
});


$('#create-payment-form2').live('submit', function (ev) {
    ev.preventDefault();
    console.log("pay2");
   
    var $this = $(this);

    var dataToSend = $this.serialize();
    var senderAccountNumber = $('#sender-account-selector').val();
   // alert(senderAccountNumber);
    var amount = Number($('#transaction-amount').val());
   // console.log(amount);
    var receiverAccountNumber = $('#input-for-receiver-account').val();
    if (senderAccountNumber.length != 10) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Same accounts");
        $('#input-for-receiver-account').val("");
        return false;
    }
    if (senderAccountNumber === receiverAccountNumber) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Same accounts");
        $('#input-for-receiver-account').val("");
        return false;
    }
    if (amount <= 0) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Amount should be positive!");
        $('#input-for-receiver-account').val("");
        $('#transaction-amount').val("0");
        return false;
    }
    if (!$this.valid()) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Data not Valid!");
        return false;
    };
   // console.log(urlToSubmitTransaction);
   // console.log(dataToSend);
    $.post(urlToSubmitTransaction, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        //TODO
       // toastr.success(response);

        window.location.href = "/user/transaction/list";

    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);

        $('#create-payment-form2').trigger("reset");

    });
});
//pagination 
$("#paging-transaction-next").live('click', function (ev) {
    ev.preventDefault();
    console.log("next");
    var $this = $(this);
    var url = $this.attr("href");

    //window.history.pushState("", "", url);
    $.get(url).done(function (data) {
        $('#target-transaction-accounts').empty();
        $('#target-transaction-accounts').append(data);
    })
        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});
$("#paging-transaction-prev").live('click', function (ev) {
    ev.preventDefault();
    console.log("prev");
    var $this = $(this);
    var url = $this.attr("href");
    //alert(url);
    //window.history.pushState("", "", url);
    $.get(url).done(function (data) {
        $('#target-transaction-accounts').empty();
        $('#target-transaction-accounts').append(data);
    })
        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});
$("#paging-transaction-last").live('click', function (ev) {
    ev.preventDefault();
    console.log("prev");
    var $this = $(this);
    var url = $this.attr("href");
    //alert(url);
    //window.history.pushState("", "", url);
    $.get(url).done(function (data) {
        $('#target-transaction-accounts').empty();
        $('#target-transaction-accounts').append(data);
    })
        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});
//edit payment
$(".modal-edit-payment").live('click', function () {
    console.log("edit");
    var recieverAccountNumber = $(this).data('receiver');
    var senderAccountNumber = $(this).data('sender');
    var amount = $(this).data('amount');
    var description = $(this).data('description');
    var id = $(this).data('id');
    var preselected = $(this).data('preselected');
    console.log(preselected);
    $("#account-pre-id").val(preselected);
    $("#input-for-receiver-account").val(recieverAccountNumber);
    $("#senderAccount").val(senderAccountNumber);
    $("#transaction-amount").val(amount);
    $("#descriptionTran").val(description);
    $("#transaction-id").val(id);

});
//edit transaction
$("#edit-payment-form").live("submit", function (ev) {
    ev.preventDefault();
    var $this = $(this);
    var url = $this.attr('action');
    var dataToSend = $this.serialize();
    var id = $('#account-pre-id').val();
    console.log(id);
    $('#formEditPaymentModal').modal('toggle');
    $.post(url, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
       

        toastr.success(response);
        if (id === "") {
            $.get("/user/transaction/GetTransactions").done(function (data) {
                $('#target-transaction-accounts').empty();

                $('#target-transaction-accounts').append(data);
            })
                .fail(function (failResponse) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-full-width",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(response.responseText);



                });
        }
        else {
            var url = "/user/transaction/GetTransactions?accountid=" + id;
            $.get(url).done(function (data) {
                $('#target-transaction-accounts').empty();

                $('#target-transaction-accounts').append(data);
            })
                .fail(function (failResponse) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-full-width",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(response.responseText);



                });
        }
      

    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);
        
    });

});
//send transaction
$('#send-transaction-from-list').live('submit', function (ev) {

    ev.preventDefault();
    console.log("send-list");
    var $this = $(this);
    var id = $('#accountIdTest').val();
    var url = $this.attr('action');
    var dataToSend = $this.serialize();


    $.post(url, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        //TODO
        toastr.success(response);
        if (id === "Default") {
            $.get("/user/transaction/GetTransactions").done(function (data) {
                $('#target-transaction-accounts').empty();

                $('#target-transaction-accounts').append(data);
            })
                .fail(function (failResponse) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-full-width",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(response.responseText);



                });
        }
        else {
            var url = "/user/transaction/GetTransactions?accountid=" + id;
            $.get(url).done(function (data) {
                $('#target-transaction-accounts').empty();

                $('#target-transaction-accounts').append(data);
            })
                .fail(function (failResponse) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-full-width",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(response.responseText);



                });
        }


    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);
        console.log("test3");
        console.log(response.responseText)
        $('input[type="text"],textarea').val('');
        $('#create-user-form').trigger("reset");
    });



});
//transactions select
$('#transactions-account-selector').live('change', function () {
    var value = $(this).val();
    console.log("select");
    console.log(value);

    if (value === "DEFAULT") {
        var url = "/user/transaction/GetTransactions";
        $.get(url).done(function (data) {
            $('#target-transaction-accounts').empty();

            $('#target-transaction-accounts').append(data);
        })
            .fail(function (failResponse) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);



            });
    }
    if (value != "DEFAULT") {
        var url = "/user/transaction/GetTransactions?accountid=" + value;
        $.get(url).done(function (data) {
            $('#target-transaction-accounts').empty();

            $('#target-transaction-accounts').append(data);
        })
            .fail(function (failResponse) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);



            });
    }

});
//edit payment
$(".modal-edit-payment").live('click', function () {
    console.log("edit");
    var recieverAccountNumber = $(this).data('receiver');
    var senderAccountNumber = $(this).data('sender');
    var amount = $(this).data('amount');
    var description = $(this).data('description');
    var id = $(this).data('id');
    var preselected = $(this).data('preselected');
    console.log(preselected);
    $("#account-pre-id").val(preselected);
    $("#input-for-receiver-account").val(recieverAccountNumber);
    $("#senderAccount").val(senderAccountNumber);
    $("#transaction-amount").val(amount);
    $("#descriptionTran").val(description);
    $("#transaction-id").val(id);

});
//make payment
$(".modal-create-payment").live('click', function () {
    console.log("pay");
    var accountNumber = $(this).data('num');
    console.log(accountNumber);
    var clientName = $(this).data('name');
    console.log(clientName);

    $("#senderAccount").val(accountNumber);
    $("#clientNameSender").text("Client:" + clientName);
});
var clickedButton;
var urlToSubmitTransaction;
$('#safe-transaction').live('click', function (e) {
    clickedButton = $(this).attr('id');

    urlToSubmitTransaction = "/User/Transaction/SaveTransaction";
})
$('#send-transaction').live('click', function (e) {
    clickedButton = $(this).attr('id');
    urlToSubmitTransaction = "/User/Transaction/SendTransaction";
})
$('#create-payment-form').live('submit', function (ev) {
    ev.preventDefault();
    console.log("test3");
    var $this = $(this);
    console.log(urlToSubmitTransaction);
    var dataToSend = $this.serialize();
    var senderAccountNumber = $('#senderAccount').val();
    var amount = Number($('#transaction-amount').val());
    console.log(amount);
    var receiverAccountNumber = $('#input-for-receiver-account').val();
    if (senderAccountNumber === receiverAccountNumber) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Same accounts");
        $('#input-for-receiver-account').val("");
        return false;
    }
    if (amount <= 0) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Amount should be positive!");
        $('#input-for-receiver-account').val("");
        $('#transaction-amount').val("0");
        return false;
    }
    if (!$this.valid()) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Data not Valid!");
        return false;
    };
    $('#formPaymentModal').modal('toggle');
    $('#create-payment-form').trigger("reset");
    $.post(urlToSubmitTransaction, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        //TODO
        toastr.success(response);
        
        
        if (urlToSubmitTransaction === "/User/Transaction/SendTransaction") {


            $.get("/User/Account/GetUserAccounts")
                .done(function (data) {
                    $('#target-all-user-accounts').empty();

                    $('#target-all-user-accounts').append(data);
                })
                .fail(function (failResponse) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-full-width",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(response.responseText);
                    console.log(response.responseText)

                 
                    $('#create-payment-form').trigger("reset");
                });
            $.get("/User/Account/GetAllUserAccounts").done(function (accounts) {
                createChart(accounts);
                $(document).bind("kendo:skinChange", createChart);
                $(".box").bind("change", refresh);

            });
        }
    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);


        $('#create-payment-form').trigger("reset");
       

    });
});
//pie chart 
function createChart(account) {
    console.log("pie");
    console.log(CalculateTotal(account));
    $("#chart").kendoChart({
        title: {
            text: "Accounts chart.Total money in all the accounts " + CalculateTotal(account) + '$'
        },
        legend: {
            position: "top"
        },
        dataSource: {
            data: account
        },
        series: [{
            type: "pie",
            field: "balance",
            categoryField: "nickName",
            explodeField: "explode"
        }],
        seriesColors: ["#ff0000", "#03a9f4", "#ff9800", "#fad84a", "#4caf50", "#00ffff", "#778899"],
        tooltip: {
            visible: true,
            template: "${ category } - ${ value }$"
        },
        seriesDefaults: {
            labels: {
                template: "${ category } - ${ value }$ - #= kendo.format('{0:P}', percentage)#",
                position: "outsideEnd",
                visible: true,
                background: "transparent"
            }
        }
    });
}
function CalculateTotal(account) {
    var total = 0;
    for (var i in account) {
        total += parseInt(account[i].balance);
    }
    return total;
};
function refresh() {
    var chart = $("#chart").data("kendoChart"),
        pieSeries = chart.options.series[0],
        labels = $("#labels").prop("checked"),
        alignInputs = $("input[name='alignType']"),
        alignLabels = alignInputs.filter(":checked").val();

    chart.options.transitions = false;
    pieSeries.labels.visible = labels;
    pieSeries.labels.align = alignLabels;

    alignInputs.attr("disabled", !labels);

    chart.refresh();
}
//rename flow
$(document).ready(function () {
    //var currentPage = 0;
    $(".modal-rename-account").live('click', function () {
        console.log("test2");
        var myAccountId = $(this).data('id');
        console.log(myAccountId);
        //currentPage = $(this).data('currentpage');
        //console.log(currentPage);
        $("#accountId").val(myAccountId);
    });
    //rename
    $('#rename-account-modal-form').live('submit', function (ev) {
        ev.preventDefault();
        console.log("test3");

        var $this = $(this);

        var url = $this.attr('action');
        var nickName = $("#newNickName").val();
        console.log(nickName);
        var dataToSend = $this.serialize();

        if (nickName.length < 3 || nickName.length > 35) {
            console.log("no")
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 2000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error("Data not Valid!");
            $("#nickName-feedback").text("Please enter between 3 and 35 characters");
            return false;
        };
        $('#formRenameAccountsModal').modal('toggle');
        $.post(url, dataToSend).done(function (response) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 2000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }

            //TODO
            toastr.success(response);
            //createChart(data);
            //$(document).bind("kendo:skinChange", createChart);
            //$(".box").bind("change", refresh);

            $('#rename-account-modal-form').trigger("reset");
            $.get("/User/Account/GetUserAccounts")
                .done(function (data) {
                    $('#target-all-user-accounts').empty();
                    console.log(data.account);
                    $('#target-all-user-accounts').append(data);
                })
                .fail(function (failResponse) {
                    toastr.options = {
                        "debug": false,
                        "positionClass": "toast-top-full-width",
                        "onclick": null,
                        "fadeIn": 300,
                        "fadeOut": 1000,
                        "timeOut": 3000,
                        "extendedTimeOut": 3000,
                        "closeButton": true
                    }
                    toastr.error(response.responseText);
                    console.log(response.responseText)

                    $('input[type="text"],textarea').val('');
                    $('#create-user-form').trigger("reset");
                });
            $.get("/User/Account/GetAllUserAccounts").done(function (accounts) {
                createChart(accounts);
                $(document).bind("kendo:skinChange", createChart);
                $(".box").bind("change", refresh);

            });
        }).fail(function (response) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
            console.log("test3");
            console.log(response.responseText)
            $('input[type="text"],textarea').val('');
            $('#create-user-form').trigger("reset");
        });
    });
});



//AdminPage
//Give banner id to modal
$(".send-banner-id-modal").live('click', function () {
    console.log("edit");
    var imageId = $(this).data('id');
    var startDate = $(this).data('startdate');
    var endDate = $(this).data('enddate');
    console.log(startDate);
    console.log(endDate);
    console.log(imageId);
    
    $("#bannerId").val(imageId);
    $('#startDate-id').val(startDate);
    $('#endDate-id').val(endDate);
});
//Give user id to modal
$(".modal-user-id").live('click', function () {
    console.log("test");
    var myUserId = $(this).data('id');
    console.log(myUserId);
    $("#userId").val(myUserId);
});

$(document).ready(function () {
    $(".create-user-to-client-class").live('click', function (ev) {
        var myUserId = $(this).data('id');
        console.log(myUserId);
        $("#ClientId").val(myUserId);
    });
});
//Create user
$('#create-user-form').live("submit", function (ev) {
    ev.preventDefault();
    console.log("test2");
    var $this = $(this);
    var url = $this.attr('action');
    var dataToSend = $this.serialize();
    if (!$this.valid()) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Data not Valid!");
        return false;
    };

    $.post(url, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        //TODO
        toastr.success(response);

        $('input[type="text"],textarea').val('');
        $('#create-user-form').trigger("reset");
        $.get("/Administration/User/ShowAllUsers")
            .done(function (data) {
                $('#target-all-users').empty();

                $('#target-all-users').append(data);
            })
            .fail(function (failResponse) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
                console.log(response.responseText)

                $('input[type="text"],textarea').val('');
                $('#create-user-form').trigger("reset");
            });

    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);
        console.log("test3");
        console.log(response.responseText)
        $('input[type="text"],textarea').val('');
        $('#create-user-form').trigger("reset");
    });
});
//Create client
$('#create-client-form').live("submit", function (ev) {
    ev.preventDefault();
    var $this = $(this);
    var url = $this.attr('action');
    var dataToSend = $this.serialize();
    if (!$this.valid()) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Data not Valid!");
        return false;
    };

    $.post(url, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        //TODO
        toastr.success(response);
        console.log("test");
        $('input[type="text"],textarea').val('');

        $.get("/Administration/Client/ShowAllClients")
            .done(function (data) {
                $('#target-all-clients').empty();

                $('#target-all-clients').append(data);
            })
            .fail(function (failResponse) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
                console.log(response.responseText)
                console.log("test2");
                $('input[type="text"],textarea').val('');
            });

    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);
        console.log("test3");
        console.log(response.responseText)
        $('input[type="text"],textarea').val('');
    });
});
//user accounts
$(".modal-user-account-id").live("click", function (ev) {
    // ev.preventDefault();
    var myUserId = $(this).data('id');
    console.log(myUserId);
    var currentLink = $(ev.target)
    var url = currentLink.data('url');
    var userId = currentLink.data('id');

    console.log(url);
    console.log(userId);
    $.get(url).done(function (data) {
        $('#target-modal-client-accounts').empty();

        $('#target-modal-client-accounts').append(data);
    });
});
//checkbox accounts
$(".checkbox-test").live('change', function (ev) {
    console.log("check")
    var isChecked = $(this).is(":checked");
    var accountId = $(this).attr('data-id');
    var myUserId = $(this).attr('data-userId');
    console.log(myUserId);
    console.log(accountId);
    if (!isChecked) {
        var url = "/administration/account/removeaccountfromuser/" + accountId + "?userId=" + myUserId;
        console.log(url)
        $.get(url).done(function (data) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 2000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(data);
            //$('#target-modal-client-accounts').empty();
            //console.log(data);
            //$('#target-modal-client-accounts').append(data);

        })

    }
    else {
        var url = "/administration/account/addaccounttouser/" + accountId + "?userId=" + myUserId;
        console.log(url)
        $.get(url).done(function (data) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 2000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.success(data);
            //$('#target-modal-client-accounts').empty();
            //console.log(data);
            //$('#target-modal-client-accounts').append(data);

        })
    }
    console.log("check")

});
//Create user from details
$('#create-user-to-client').live("submit", function (ev) {
    ev.preventDefault();
    console.log("s");
    var id = $('input[name="ClientId"]').val()
    var url2 = "/Administration/Client/GetClientUsers/" + id;
    console.log(id);
    console.log(url2);
    var $this = $(this);
    var url = $this.attr('action');
    var dataToSend = $this.serialize();

    if (!$this.valid()) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error("Data not Valid!");
        return false;
    };

    $.post(url, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        //TODO
        toastr.success(response);

        $('input[type="text"],textarea').val('');
        $('#create-user-to-client').trigger("reset");

        console.log(url2);
        $.get(url2)
            .done(function (data) {
                $('#target-all-users-client2').empty();

                $('#target-all-users-client2').append(data);
            })
            .fail(function (failResponse) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
                console.log(response.responseText)

                $('input[type="text"],textarea').val('');
                $('#create-user-form').trigger("reset");
            });

    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);
        console.log("test3");
        console.log(response.responseText)
        $('input[type="text"],textarea').val('');
        $('#create-user-form').trigger("reset");
    });
});
//create account in client
$('#create-account-client').live('submit', function (ev) {
    ev.preventDefault();
    console.log("account");

    var $this = $(this);
    var url = $this.attr('action');
    var dataToSend = $this.serialize();
    var id = $('input[name="id"]').val()
    console.log(id);
    var url2 = "/Administration/Account/GetClientAccounts/" + id;
    console.log(url2);
    $.post(url, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        //TODO
        toastr.success(response);


        $.get(url2)
            .done(function (data) {
                $('#target-all-client-accounts').empty();

                $('#target-all-client-accounts').append(data);
            })
            .fail(function (failResponse) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
                console.log(response.responseText)

                $('input[type="text"],textarea').val('');
                $('#create-user-form').trigger("reset");
            });

    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);
        console.log("test3");
        console.log(response.responseText)
        $('input[type="text"],textarea').val('');
        $('#create-user-form').trigger("reset");
    });
});
//autocomplete user-client
$('#clientName').live('keyup.autocomplete', function () {
    console.log("auto");
    $(this).autocomplete({
        source: function (request, response) {
            $.ajax({
                //url: "/User/Account/FindSingleByAccName",
                url: "/Administration/Client/FindClientbyName",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {

                    response($.map(data, function (item) {
                        console.log("auto4");
                        console.log(data);
                        var itemToSave = Number(item.id);
                        $("#save-target-id").val(itemToSave);
                        var completeAccountListing = item;
                        console.log(completeAccountListing);

                        return {
                            label: completeAccountListing
                        };
                    }));

                }
            });
        },
        minLength: 3,
        delay: 500,
        minLength: 3,
        response: function (event, ui) {

            if (!ui.content.length) {
                var noResult = { value: "", label: "No results found" };
                ui.content.push(noResult);
                $("#message-autocomplete-client-user").text("No results found");
            } else {
                $("#message-autocomplete-client-user").empty();

            }
        }
    });
});
//autocomplete user
$('#search-user-auto').live('keyup.autocomplete', function () {
    console.log("auto");
    $(this).autocomplete({
        source: function (request, response) {
            $.ajax({
               
                url: "/Administration/User/FindUserByName",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {

                    response($.map(data, function (item) {
                        console.log("auto4");
                        console.log(data);
                        var itemToSave = Number(item.id);
                        $("#save-target-id").val(itemToSave);
                        var completeAccountListing = item;
                        console.log(completeAccountListing);
                       
                        return {
                            label: completeAccountListing
                        };
                    }));

                }
            });
        },
        minLength: 3,
        delay: 500,
        minLength: 3,
        response: function (event, ui) {

            if (!ui.content.length) {
                var noResult = { value: "", label: "No results found" };
                ui.content.push(noResult);
                $("#message-autocomplete-user").text("No results found");
            } else {
                $("#message-autocomplete-user").empty();

            }
        }
    });
});

//autocomplete client
$('#search-client-auto').live('keyup.autocomplete', function () {
    console.log("auto");
    $(this).autocomplete({
        source: function (request, response) {
            $.ajax({
                //url: "/User/Account/FindSingleByAccName",
                url: "/Administration/Client/FindClientbyName",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {

                    response($.map(data, function (item) {
                        console.log("auto2");
                        console.log(data);
                        var itemToSave = Number(item.id);
                        $("#save-target-id").val(itemToSave);
                        var completeAccountListing = item;
                        console.log(completeAccountListing);

                        return {
                            label: completeAccountListing
                        };
                    }));

                }
            });
        },
        minLength: 3,
        delay: 500,
        minLength: 3,
        response: function (event, ui) {

            if (!ui.content.length) {
                var noResult = { value: "", label: "No results found" };
                ui.content.push(noResult);
                $("#message-autocomplete").text("No results found");
            } else {
                $("#message-autocomplete").empty();

            }
        }
    });
});
//user paging
$("#paging-users-next").live('click', function (ev) {
    ev.preventDefault();
    console.log("next2");
    var $this = $(this);
    var url = $this.attr("href");
    //alert(url);
    //window.history.pushState("", "", url);
    $.get(url).done(function (data) {
        $('#target-all-users').empty();
        $('#target-all-users').append(data);
    })
        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});
$("#paging-users-prev").live('click', function (ev) {
    ev.preventDefault();
    console.log("prev");
    var $this = $(this);
    var url = $this.attr("href");
    // alert(url);
    //window.history.pushState("", "", url);
    $.get(url).done(function (data) {
        $('#target-all-users').empty();
        $('#target-all-users').append(data);
    })
        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});
$("#paging-users-last").live('click', function (ev) {
    ev.preventDefault();
    console.log("prev");
    var $this = $(this);
    var url = $this.attr("href");
    //alert(url);
    //window.history.pushState("", "", url);
    $.get(url).done(function (data) {
        $('#target-all-users').empty();
        $('#target-all-users').append(data);
    })
        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});
//client paging
$("#paging-clients-next").live('click', function (ev) {
    ev.preventDefault();
    console.log("next2");
    var $this = $(this);
    var url = $this.attr("href");
    //alert(url);
    //window.history.pushState("", "", url);
    $.get(url).done(function (data) {
        $('#target-all-clients').empty();
        $('#target-all-clients').append(data);
    })
        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});
$("#paging-clients-prev").live('click', function (ev) {
    ev.preventDefault();
    console.log("prev");
    var $this = $(this);
    var url = $this.attr("href");
    // alert(url);
    //window.history.pushState("", "", url);
    $.get(url).done(function (data) {
        $('#target-all-clients').empty();
        $('#target-all-clients').append(data);
    })
        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});
$("#paging-clients-last").live('click', function (ev) {
    ev.preventDefault();
    console.log("prev");
    var $this = $(this);
    var url = $this.attr("href");
    //alert(url);
    //window.history.pushState("", "", url);
    $.get(url).done(function (data) {
        $('#target-all-clients').empty();
        $('#target-all-clients').append(data);
    })
        .fail(function (failResponse) {
            toastr.options = {
                "debug": false,
                "positionClass": "toast-top-full-width",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 3000,
                "extendedTimeOut": 3000,
                "closeButton": true
            }
            toastr.error(response.responseText);
        });
});
$('.create-account-client-test').live('submit', function (ev) {
    ev.preventDefault();
    console.log("account");

    var $this = $(this);
    var url = $this.attr('action');
    var dataToSend = $this.serialize();
    var id = $('input[name="id"]').val()
    console.log(id);
    //var url2 = "/Administration/Account/GetClientAccounts/" + id;
    console.log(url);
    $.post(url, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        //TODO
        toastr.success(response);


        //    $.get(url2)
        //        .done(function (data) {
        //            $('#target-all-client-accounts').empty();

        //            $('#target-all-client-accounts').append(data);
        //        })
        //        .fail(function (failResponse) {
        //            toastr.options = {
        //                "debug": false,
        //                "positionClass": "toast-top-full-width",
        //                "onclick": null,
        //                "fadeIn": 300,
        //                "fadeOut": 1000,
        //                "timeOut": 3000,
        //                "extendedTimeOut": 3000,
        //                "closeButton": true
        //            }
        //            toastr.error(response.responseText);
        //            console.log(response.responseText)

        //            $('input[type="text"],textarea').val('');
        //            $('#create-user-form').trigger("reset");
        //        });

        //}).fail(function (response) {
        //    toastr.options = {
        //        "debug": false,
        //        "positionClass": "toast-top-full-width",
        //        "onclick": null,
        //        "fadeIn": 300,
        //        "fadeOut": 1000,
        //        "timeOut": 3000,
        //        "extendedTimeOut": 3000,
        //        "closeButton": true
        //    }
        //    toastr.error(response.responseText);
        //    console.log("test3");
        //    console.log(response.responseText)
        //    $('input[type="text"],textarea').val('');
        //    $('#create-user-form').trigger("reset");
    });
});
$('#create-account-from-client').live('submit', function (ev) {
    ev.preventDefault();
    console.log("account");

    var $this = $(this);
    var url = $this.attr('action');
    var dataToSend = $this.serialize();
    var id = $('input[name="id"]').val()
    console.log(id);
    var url2 = "/Administration/Account/GetClientAccounts/" + id;
    console.log(url);
    $.post(url, dataToSend).done(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 2000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }

        //TODO
        toastr.success(response);


        $.get(url2)
            .done(function (data) {
                $('#target-all-client-accounts').empty();

                $('#target-all-client-accounts').append(data);
            })
            .fail(function (failResponse) {
                toastr.options = {
                    "debug": false,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "fadeIn": 300,
                    "fadeOut": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 3000,
                    "closeButton": true
                }
                toastr.error(response.responseText);
                console.log(response.responseText)


            });

    }).fail(function (response) {
        toastr.options = {
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 3000,
            "extendedTimeOut": 3000,
            "closeButton": true
        }
        toastr.error(response.responseText);
        console.log("test3");
        console.log(response.responseText)

    });
});
