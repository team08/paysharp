﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.ViewModels
{
    public class BannerViewModel
    {

        public string Id { get; set; }


        public string ImgPath { get; set; }


        public string Name { get; set; }


        public string Url { get; set; }


        public string StartDate { get; set; }


        public string EndDate { get; set; }

    }
}
