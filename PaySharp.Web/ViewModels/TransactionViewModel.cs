﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.ViewModels
{
    public class TransactionViewModel
    {
        public long Id { get; set; }

        public long SenderAccountID { get; set; }

        public AccountViewModel SenderAccount { get; set; }

        public long ReceiverAccountID { get; set; }

        public AccountViewModel ReceiverAccount { get; set; }

        public string Description { get; set; }

        public decimal Amount { get; set; }

        public DateTime TimeStamp { get; set; }

        public string Status { get; set; }

        public bool IsSend { get; set; }

        public bool IsRecieved { get; set; }

        // public List<SelectListItem> Accounts { get; set; }
        public IEnumerable<AccountViewModel> Accounts { get; set; }

    }
}
