﻿using PaySharp.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.ViewModels
{
    public class UserViewModel
    {
        [Key]
        public long Id { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(35)]
        public string Name { get; set; }

        [Required]
        [MinLength(5)]
        [MaxLength(16)]
        public string UserName { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }

        public long ClientId { get; set; }
       

        public ICollection<ClientViewModel> Clients { get; set; }
        public ICollection<AccountViewModel> Accounts { get; set; }
    }
}
