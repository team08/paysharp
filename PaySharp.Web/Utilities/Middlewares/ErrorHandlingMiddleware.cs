﻿using Microsoft.AspNetCore.Http;
using PaySharp.Services.Exceptions;
using System;
using System.Threading.Tasks;

namespace PaySharp.Web.Utilities.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await this.next.Invoke(context);
            }
            catch (TokenExpirationException)
            {
                context.Response.Redirect("/Identity/login");
            }
            catch (InsufficientFundsException)
            {
                context.Response.Redirect("/home/invalid");
            }
            catch (EntityAlreadyExistsException)
            {
                context.Response.Redirect("/home/invalid");
            }
            catch (EntityNotFoundException)
            {
                context.Response.Redirect("/home/invalid");
            }
            catch (NoAccessException)
            {
                context.Response.Redirect("/home/noaccess");
            }
            catch (Exception)
            {
                context.Response.Redirect("/home/servererror");
            }
        }
    }
}
