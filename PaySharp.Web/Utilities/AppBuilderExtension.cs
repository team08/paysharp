﻿using Microsoft.AspNetCore.Builder;
using PaySharp.Web.Utilities.Middlewares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Utilities
{
    public static class AppBuilderExtension
    {
        public static void ImportantExceptionHandling(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<ErrorHandlingMiddleware>();
        }
    }
}
