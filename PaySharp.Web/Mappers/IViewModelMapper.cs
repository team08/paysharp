﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaySharp.Web.Mappers
{
    public interface IViewModelMapper<TDto, TViewModel>
    {
        TViewModel MapFrom(TDto dto);
    }
}
