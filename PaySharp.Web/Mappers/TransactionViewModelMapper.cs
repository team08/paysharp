﻿using PaySharp.Services.DTO;
using PaySharp.Web.ViewModels;
using System;

namespace PaySharp.Web.Mappers
{
   public class TransactionViewModelMapper : IViewModelMapper<TransactionDTO, TransactionViewModel>
    {
        private readonly IViewModelMapper<AccountDTO, AccountViewModel> accountMapper;

        public TransactionViewModelMapper(IViewModelMapper<AccountDTO, AccountViewModel> accountMapper)
        {
            this.accountMapper = accountMapper ?? throw new ArgumentNullException(nameof(accountMapper));
        }

        public TransactionViewModel MapFrom(TransactionDTO dto)
         => new TransactionViewModel
         {
             Id = dto.Id,
             SenderAccountID = dto.SenderAccountID,
             ReceiverAccountID = dto.ReceiverAccountID,
             Amount = dto.Amount,
             Description = dto.Description,
             TimeStamp = dto.TimeStamp,
             Status = dto.Status,
             SenderAccount = accountMapper.MapFrom(dto.SenderAccount ?? null),
             ReceiverAccount = accountMapper.MapFrom(dto.ReceiverAccount ?? null),
             IsRecieved = dto.IsRecieved,
             IsSend = dto.IsSend
         };
    }
}
