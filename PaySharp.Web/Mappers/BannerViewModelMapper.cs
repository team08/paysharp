﻿using PaySharp.Services.DTO;
using PaySharp.Web.ViewModels;

namespace PaySharp.Web.Mappers
{
    public class BannerViewModelMapper : IViewModelMapper<BannerDTO, BannerViewModel>
    {
        public BannerViewModel MapFrom(BannerDTO dto)
       => new BannerViewModel()
       {
           Id = dto.Id.ToString(),
           Name = dto.Name,
           Url = dto.Url,
           ImgPath = dto.ImgPath,
           StartDate = dto.StartDate.ToString("dd/MM/yyyy"),
           EndDate = dto.EndDate.ToString("dd/MM/yyyy")
       };
    }
}
