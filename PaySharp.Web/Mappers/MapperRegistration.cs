﻿using Microsoft.Extensions.DependencyInjection;
using PaySharp.Services.DTO;
using PaySharp.Web.ViewModels;

namespace PaySharp.Web.Mappers
{
    public static class MapperRegistration
    {
        public static IServiceCollection AddViewModelMappers(this IServiceCollection services)
        {

            services.AddScoped<IViewModelMapper<AccountDTO, AccountViewModel>, AccountViewModelMapper>();
            services.AddScoped<IViewModelMapper<ClientDTO, ClientViewModel>, ClientViewModelMapper>();
            services.AddScoped<IViewModelMapper<UserDTO, UserViewModel>, UserViewModelMapper>();
            services.AddScoped<IViewModelMapper<BannerDTO, BannerViewModel>, BannerViewModelMapper>();
            services.AddScoped<IViewModelMapper<TransactionDTO, TransactionViewModel>, TransactionViewModelMapper>();
            return services;
        }
    }
}
