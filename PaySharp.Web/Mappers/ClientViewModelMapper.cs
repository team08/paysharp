﻿using PaySharp.Services.DTO;
using PaySharp.Web.ViewModels;

namespace PaySharp.Web.Mappers
{
    public class ClientViewModelMapper : IViewModelMapper<ClientDTO, ClientViewModel>
    {
        public ClientViewModel MapFrom(ClientDTO entity)
        {
            return new ClientViewModel()
            {
                Id = entity.ClientId,
                Name = entity.Name,
            };
        }
    }
}
