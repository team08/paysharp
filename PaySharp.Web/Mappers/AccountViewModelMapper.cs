﻿using PaySharp.Services.DTO;
using PaySharp.Web.ViewModels;

namespace PaySharp.Web.Mappers
{
    public class AccountViewModelMapper : IViewModelMapper<AccountDTO, AccountViewModel>
    {
        public AccountViewModel MapFrom(AccountDTO dto)
          => new AccountViewModel
          {
              Id = dto.Id,
              AccountNumber = dto.AccountNumber,
              Balance = dto.Balance,
              NickName = dto.NickName,
              ClientName=dto.ClientName,
              IsAddedToUser=dto.IsAddedToUser
          };
    }
}
