﻿using PaySharp.Services.DTO;
using PaySharp.Web.ViewModels;

namespace PaySharp.Web.Mappers
{
    public class UserViewModelMapper : IViewModelMapper<UserDTO, UserViewModel>
    {
        public UserViewModel MapFrom(UserDTO entity)
        {
            return new UserViewModel()
            {
                Id = entity.UserId,
                Name = entity.Name,
                UserName = entity.UserName,
                Password = entity.Password,
            };
        }
    }
}
