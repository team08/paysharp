﻿
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PaySharp.Data.DataContext;
using PaySharp.Services;
using PaySharp.Services.Contracts;
using PaySharp.Services.Mappers;
using PaySharp.Services.Utilities.RandomGenerator;
using PaySharp.Services.Utilities.Wrapper;
using PaySharp.Web.Mappers;
using PaySharp.Web.Utilities;

namespace PaySharp.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

      
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
           .AddCookie();

            services.AddDbContext<PaySharpDBContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("AzureConnection")));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<Services.Contracts.ICookieManager, CookieManager>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IBannerService, BannerService>();
            services.AddSingleton<ITokenManager, TokenManager>();
            services.AddScoped<IAuthorizationManager, AuthorizationManager>();
            services.AddSingleton<IPasswordHasher, PasswordHasher>();
            services.AddScoped<IRandomGenerator, RandomGenerator>();
            services.AddSingleton<IDateWrapper, DateWrapper>();

            services.AddDtoMappers();

            services.AddViewModelMappers();


            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


          
            services.AddRouting(options => options.LowercaseUrls = true);
        }

       
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.ImportantExceptionHandling();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.ImportantExceptionHandling();
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "areas",
                template: "{area:exists}/{controller=Dashboard}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Identity}/{action=Login}/{id?}");
            });
        }
    }
}
