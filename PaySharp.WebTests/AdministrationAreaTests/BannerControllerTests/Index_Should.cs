﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Areas.Administration.ViewModels;
using PaySharp.Web.Mappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.BannerControllerTests
{
    [TestClass]
    public class Index_Should
    {
        [TestMethod]
        public async Task InvokeGetAllBannersAsync()
        {
            var bannerService = new Mock<IBannerService>();
            var controller = this.SetupController(bannerService.Object);

            var result = await controller.Index(null);

            bannerService.Verify(a => a.GetAllBannersAsync(1, 6), Times.Once);
            bannerService.Verify(a => a.GetCount(), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var bannerService = new Mock<IBannerService>();
            var controller = this.SetupController(bannerService.Object);

            var banner = new BannerDTO()
            {
                ImgPath = "path",
                Url = "url"

            };
            bannerService.Setup(a => a.GetAllBannersAsync(1, 6))
                         .ReturnsAsync(new List<BannerDTO>() { banner });
            var result = await controller.Index(null);

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(PaginationBannerViewModel));
        }

        //Utility starts here
        private BannerController SetupController(IBannerService bannerService)
        {

            var bannertMapper = new BannerViewModelMapper();
            var hostingEnv = new Mock<IHostingEnvironment>();

            return new BannerController(bannerService, bannertMapper, hostingEnv.Object);
        }
    }
}
