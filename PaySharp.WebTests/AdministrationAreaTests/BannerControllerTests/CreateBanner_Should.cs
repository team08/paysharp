﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.BannerControllerTests
{
    [TestClass]
    public class CreateBanner_Should
    {
        [TestMethod]
        public async Task ReturnsCorrectlyActionResult_ModelIsNotValid()
        {
            var bannerService = new Mock<IBannerService>();
            var hostingEnv = new Mock<IHostingEnvironment>();
            var fileMock = new Mock<IFormFile>();
            var controller = this.SetupController(bannerService.Object, hostingEnv.Object);
            var date = DateTime.Now;

            var banner = new CreateBannerViewModel()
            {
                Name = "test",
                DateRange="10/20/2019 - 10/21/2020",
                Url = "testsite",
                Image = fileMock.Object
            };
            controller.ModelState.AddModelError("key", "error message");
            var result = await controller.CreateBanner(banner);

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Index", redirectResult.ActionName);
            Assert.AreEqual("Banner", redirectResult.ControllerName);
            Assert.AreEqual("Administration", redirectResult.RouteValues["area"]);

        }

        private BannerController SetupController(IBannerService bannerService,
                                          IHostingEnvironment hostingEnv)
        {

            var bannertMapper = new BannerViewModelMapper();

            return new BannerController(bannerService, bannertMapper, hostingEnv);
        }
    }
}
