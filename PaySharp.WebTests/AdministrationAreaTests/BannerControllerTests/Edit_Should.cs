﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using System;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.BannerControllerTests
{
    [TestClass]
    public class Edit_Should
    {
        [TestMethod]
        public async Task InvokeEditBanner()
        {
            var bannerService = new Mock<IBannerService>();
            var hostingEnv = new Mock<IHostingEnvironment>();
            var controller = this.SetupController(bannerService.Object, hostingEnv.Object);
            var date = DateTime.Now;

            var result = await controller.Edit("1", date, date);

            bannerService.Verify(a => a.EditBanner("1", date, date), Times.Once);

        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var bannerService = new Mock<IBannerService>();
            var hostingEnv = new Mock<IHostingEnvironment>();
            var controller = this.SetupController(bannerService.Object, hostingEnv.Object);
            var date = DateTime.Now;

            var result = await controller.Edit("1", date, date);

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Index", redirectResult.ActionName);
            Assert.AreEqual("Banner", redirectResult.ControllerName);
            Assert.AreEqual("Administration", redirectResult.RouteValues["area"]);

        }

        //Utility starts here
        private BannerController SetupController(IBannerService bannerService,
                                             IHostingEnvironment hostingEnv)
        {

            var bannertMapper = new BannerViewModelMapper();

            return new BannerController(bannerService, bannertMapper, hostingEnv);
        }
    }
}
