﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.BannerControllerTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public async Task InvokeDeleteBanner()
        {
            var bannerService = new Mock<IBannerService>();
            var hostingEnv = new Mock<IHostingEnvironment>();
            var controller = this.SetupController(bannerService.Object, hostingEnv.Object);

            hostingEnv.Setup(a => a.WebRootPath).Returns("test/");

            var result = await controller.Delete("1");

            bannerService.Verify(a => a.DeleteBanner("1", "test/banners"), Times.Once);
            hostingEnv.Verify(a => a.WebRootPath, Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var bannerService = new Mock<IBannerService>();
            var hostingEnv = new Mock<IHostingEnvironment>();
            var controller = this.SetupController(bannerService.Object, hostingEnv.Object);

            hostingEnv.Setup(a => a.WebRootPath).Returns("test/");

            var result = await controller.Delete("1");

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Index", redirectResult.ActionName);
            Assert.AreEqual("Banner", redirectResult.ControllerName);
            Assert.AreEqual("Administration", redirectResult.RouteValues["area"]);
        }

        //Utility starts here
        private BannerController SetupController(IBannerService bannerService,
                                             IHostingEnvironment hostingEnv)
        {

            var bannertMapper = new BannerViewModelMapper();

            return new BannerController(bannerService, bannertMapper, hostingEnv);
        }
    }
}
