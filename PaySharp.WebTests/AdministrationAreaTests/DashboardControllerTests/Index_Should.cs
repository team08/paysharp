﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Areas.Administration.ViewModels;
using PaySharp.Web.Mappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.DashboardControllerTests
{
    [TestClass]
    public class Index_Should
    {
        [TestMethod]
        public async Task InvokeTakeNumberOfClientsAsync()
        {
            var userService = new Mock<IUserService>();
            var clientService = new Mock<IClientService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var clientMapper = new ClientViewModelMapper();
            var userMapper = new UserViewModelMapper();

            var controller = new DashboardController(userService.Object, clientService.Object,
                                   tokenManager.Object, clientMapper, cookieManager.Object,
                                   userMapper);

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password",
                Accounts = new List<AccountDTO>(),
                Clients = new List<ClientDTO>()
            };

            userService.Setup(a => a.TakeNumberOfUsersAsync(1, 5))
              .ReturnsAsync(new List<UserDTO>() { userDto });
            userService.Setup(a => a.GetAllUsersCountAsync())
               .ReturnsAsync(1);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.TakeNumberOfClientsAsync(1, 5))
                .ReturnsAsync(new List<ClientDTO>() { client });
            clientService.Setup(a => a.GetAllClientsCountAsync())
               .ReturnsAsync(1);
            var result = await controller.Index();

            userService.Verify(a => a.TakeNumberOfUsersAsync(1, 5), Times.Once);
            userService.Verify(a => a.GetAllUsersCountAsync(), Times.Once);
            clientService.Verify(a => a.TakeNumberOfClientsAsync(1, 5), Times.Once);
            clientService.Verify(a => a.GetAllClientsCountAsync(), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var userService = new Mock<IUserService>();
            var clientService = new Mock<IClientService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var clientMapper = new ClientViewModelMapper();
            var userMapper = new UserViewModelMapper();

            var controller = new DashboardController(userService.Object, clientService.Object,
                                   tokenManager.Object, clientMapper, cookieManager.Object,
                                   userMapper);

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password",
                Accounts = new List<AccountDTO>(),
                Clients = new List<ClientDTO>()
            };

            userService.Setup(a => a.TakeNumberOfUsersAsync(1, 5))
              .ReturnsAsync(new List<UserDTO>() { userDto });
            userService.Setup(a => a.GetAllUsersCountAsync())
               .ReturnsAsync(1);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.TakeNumberOfClientsAsync(1, 5))
                .ReturnsAsync(new List<ClientDTO>() { client });
            clientService.Setup(a => a.GetAllClientsCountAsync())
               .ReturnsAsync(1);
            var result = await controller.Index();

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(DashboardViewModel));
        }
    }
}
