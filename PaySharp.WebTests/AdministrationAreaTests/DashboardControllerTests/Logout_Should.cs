﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;

namespace PaySharp.WebTests.AdministrationAreaTests.DashboardControllerTests
{
    [TestClass]
    public class Logout_Should
    {
        [TestMethod]
        public void InvokeDeleteSessionCookies()
        {
            var userService = new Mock<IUserService>();
            var clientService = new Mock<IClientService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var clientMapper = new ClientViewModelMapper();
            var userMapper = new UserViewModelMapper();

            var controller = new DashboardController(userService.Object, clientService.Object,
                                   tokenManager.Object, clientMapper, cookieManager.Object,
                                   userMapper);

            var result = controller.Logout();

            cookieManager.Verify(a => a.DeleteSessionCookies(), Times.Once);
        }

        [TestMethod]
        public void ReturnsCorrectlyActionResult()
        {
            var userService = new Mock<IUserService>();
            var clientService = new Mock<IClientService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var clientMapper = new ClientViewModelMapper();
            var userMapper = new UserViewModelMapper();

            var controller = new DashboardController(userService.Object, clientService.Object,
                                   tokenManager.Object, clientMapper, cookieManager.Object,
                                   userMapper);

            var result = controller.Logout();

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Login", redirectResult.ActionName);
            Assert.AreEqual("Dashboard", redirectResult.ControllerName);
            Assert.AreEqual("Administration", redirectResult.RouteValues["area"]);
        }
    }
}
