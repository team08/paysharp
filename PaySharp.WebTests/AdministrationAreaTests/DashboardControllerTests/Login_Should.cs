﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.DashboardControllerTests
{
    [TestClass]
    public class Login_Should
    {
        [TestMethod]
        public void ReturnsCorrectlyActionResult_LoginGet()
        {
            var userService = new Mock<IUserService>();
            var clientService = new Mock<IClientService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var clientMapper = new ClientViewModelMapper();
            var userMapper = new UserViewModelMapper();

            var controller = new DashboardController(userService.Object, clientService.Object,
                                   tokenManager.Object, clientMapper, cookieManager.Object,
                                   userMapper);

            var result = controller.Login();

            Assert.IsInstanceOfType(result, typeof(ViewResult));

        }

        [TestMethod]
        public async Task ReturnsActionResult_ModelNotValid()
        {
            var userService = new Mock<IUserService>();
            var clientService = new Mock<IClientService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var clientMapper = new ClientViewModelMapper();
            var userMapper = new UserViewModelMapper();

            var controller = new DashboardController(userService.Object, clientService.Object,
                                   tokenManager.Object, clientMapper, cookieManager.Object,
                                   userMapper);
            var loginVm = new LoginViewModel()
            {
                UserName = "username",
                Password = "password"
            };

            controller.ModelState.AddModelError("key", "error message");
            var result = await controller.Login(loginVm);

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(LoginViewModel));

        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var userService = new Mock<IUserService>();
            var clientService = new Mock<IClientService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var clientMapper = new ClientViewModelMapper();
            var userMapper = new UserViewModelMapper();

            var controller = new DashboardController(userService.Object, clientService.Object,
                                   tokenManager.Object, clientMapper, cookieManager.Object,
                                   userMapper);
            var loginVm = new LoginViewModel()
            {
                UserName = "username",
                Password = "password"
            };

            var adminDto = new AdminDTO()
            {
                Id = 1,
                UserName = "username",
                Password = "password",
                Role = "admin"
            };

            userService.Setup(a => a.GetAdminAsync("username", "password"))
            .ReturnsAsync(adminDto);
            tokenManager.Setup(a => a.GenerateToken("username", "admin", "1"))
            .Returns("token");

            var result = await controller.Login(loginVm);

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Index", redirectResult.ActionName);
            Assert.AreEqual("Dashboard", redirectResult.ControllerName);
            Assert.AreEqual("Administration", redirectResult.RouteValues["area"]);
        }

        [TestMethod]
        public async Task InvokeGetAdminAsync()
        {
            var userService = new Mock<IUserService>();
            var clientService = new Mock<IClientService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var clientMapper = new ClientViewModelMapper();
            var userMapper = new UserViewModelMapper();

            var controller = new DashboardController(userService.Object, clientService.Object,
                                   tokenManager.Object, clientMapper, cookieManager.Object,
                                   userMapper);
            var loginVm = new LoginViewModel()
            {
                UserName = "username",
                Password = "password"
            };

            var adminDto = new AdminDTO()
            {
                Id = 1,
                UserName = "username",
                Password = "password",
                Role = "admin"
            };

            userService.Setup(a => a.GetAdminAsync("username", "password"))
            .ReturnsAsync(adminDto);
            tokenManager.Setup(a => a.GenerateToken("username", "admin", "1"))
            .Returns("token");

            var result = await controller.Login(loginVm);

            userService.Verify(a => a.GetAdminAsync("username", "password"), Times.Once);
            tokenManager.Verify(a => a.GenerateToken("username", "admin", "1"), Times.Once);
            cookieManager.Verify(a => a.AddSessionCookieForToken("token","username"), Times.Once);
        }

        [TestMethod]
        public async Task Throws_EntityNotFoundException()
        {
            var userService = new Mock<IUserService>();
            var clientService = new Mock<IClientService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var clientMapper = new ClientViewModelMapper();
            var userMapper = new UserViewModelMapper();

            var controller = new DashboardController(userService.Object, clientService.Object,
                                   tokenManager.Object, clientMapper, cookieManager.Object,
                                   userMapper);
            var loginVm = new LoginViewModel()
            {
                UserName = "username",
                Password = "password"
            };

            var adminDto = new AdminDTO()
            {
                Id = 1,
                UserName = "username",
                Password = "password",
                Role = "admin"
            };

            userService.Setup(a => a.GetAdminAsync("username", "password"))
            .ThrowsAsync(new EntityNotFoundException("Tests"));

            var result = await controller.Login(loginVm);

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(LoginViewModel));
        }
    }
}
