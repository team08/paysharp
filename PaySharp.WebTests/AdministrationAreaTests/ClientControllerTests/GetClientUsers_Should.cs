﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Areas.Administration.ViewModels;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace PaySharp.WebTests.AdministrationAreaTests.ClientControllerTests
{
    [TestClass]
    public class GetClientUsers_Should
    {
        [TestMethod]
        public async Task InvokeGetClientAsync()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.GetClientAsync(1))
                        .ReturnsAsync(client);
          
            var result = await controller.GetClientUsers(1);

            clientService.Verify(a => a.GetClientAsync(1), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.GetClientAsync(1))
                        .ReturnsAsync(client);

            var result = await controller.GetClientUsers(1);

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));

            var view = (PartialViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(ICollection<UserViewModel>));
        }

        //Utility starts here
        private ClientController SetupController(IClientService clientService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new ClientController(clientService, clientMapper, accountMapper, userMapper);
        }
    }
}
