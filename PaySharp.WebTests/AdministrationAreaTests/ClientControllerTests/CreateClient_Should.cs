﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.ClientControllerTests
{
    [TestClass]
    public class CreateClient_Should
    {
        [TestMethod]
        public async Task InvokeCreateClientAsync()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientViewModel()
            {
               Name="test"
            };
            var result = await controller.CreateClient(client);

            clientService.Verify(a => a.CreateClientAsync("test"), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientViewModel()
            {
                Name = "test"
            };
            var result = await controller.CreateClient(client);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            var view = (OkObjectResult)result;

            Assert.AreEqual(view.Value, "Client with name test was added successfully");
        }

        [TestMethod]
        public async Task ThrowsException()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);
           
            var client = new ClientViewModel()
            {
                Name = "test"
            };

            clientService.Setup(a => a.CreateClientAsync("test"))
      .ThrowsAsync(new EntityAlreadyExistsException("Tests"));

            var result = await controller.CreateClient(client);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var message = (BadRequestObjectResult)result;

            Assert.AreEqual(message.Value, "Tests");
        }

        [TestMethod]
        public async Task ModelNotValid_ReturnsRedirectActionResult()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientViewModel()
            {
               
            };
            controller.ModelState.AddModelError("key", "error message");
            var result = await controller.CreateClient(null);

            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Index", redirectResult.ActionName);
            Assert.AreEqual("Dashboard", redirectResult.ControllerName);
            Assert.AreEqual("Administration", redirectResult.RouteValues["area"]);
        }


        //Utility starts here
        private ClientController SetupController(IClientService clientService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new ClientController(clientService,clientMapper,accountMapper,userMapper);
        }
    }
}
