﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.ClientControllerTests
{
    [TestClass]
    public class FindClientbyName_Should
    {
        [TestMethod]
        public async Task InvokeFindClientContainingAsync()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.FindClientContainingAsync("sho"))
                        .ReturnsAsync(new List<string>() {"gosho","pesho" });

            var result = await controller.FindClientbyName("sho");

            clientService.Verify(a => a.FindClientContainingAsync("sho"), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyIActionResult()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.FindClientContainingAsync("sho"))
                        .ReturnsAsync(new List<string>() { "gosho", "pesho" });

            var result = await controller.FindClientbyName("sho");

            Assert.IsInstanceOfType(result, typeof(JsonResult));

            var json = (JsonResult)result;

            var names = json.Value as List<string>;

            Assert.AreEqual(names.Count, 2);

            Assert.AreEqual(names[0], "gosho");
            Assert.AreEqual(names[1], "pesho");
        }

        [TestMethod]
        public async Task ReturnsCorrectlyIActionResult_NullParameter()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            var result = await controller.FindClientbyName(null);

            Assert.IsInstanceOfType(result, typeof(JsonResult));

            var json = (JsonResult)result;

            Assert.AreEqual(json.Value, "");
        }
        //Utility starts here
        private ClientController SetupController(IClientService clientService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new ClientController(clientService, clientMapper, accountMapper, userMapper);
        }
    }
}
