﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.ClientControllerTests
{
    [TestClass]
    public class ClientDetails_Should
    {

        [TestMethod]
        public async Task InvokeGetClientAsync()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var result = await controller.ClientDetails(1);

            clientService.Verify(a => a.GetClientAsync(1), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsNotFoundActionResult()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var result = await controller.ClientDetails(1);

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));

        }

        [TestMethod]
        public async Task ReturnsCorectlyActionResult()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);
            var account = new AccountDTO()
            {
                AccountNumber= "1234567890",
                Balance=10000,
                ClientName="test",
                NickName="test",
                Id=1
            };
            var client = new ClientDTO()
            {
                Name = "name",
                ClientId=1,
                Accounts = new List<AccountDTO>() {account},
                UsersClients= new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.GetClientAsync(1))
     .ReturnsAsync(client);
            var result = await controller.ClientDetails(1);

            Assert.IsInstanceOfType(result, typeof(ViewResult));


            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(ClientViewModel));
            
        }

        //Utility starts here
        private ClientController SetupController(IClientService clientService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new ClientController(clientService, clientMapper, accountMapper, userMapper);
        }
    }
}
