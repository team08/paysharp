﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Areas.Administration.ViewModels;
using PaySharp.Web.Mappers;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace PaySharp.WebTests.AdministrationAreaTests.ClientControllerTests
{
    [TestClass]
    public class ShowAllClients_Should
    {

        [TestMethod]
        public async Task InvokeTakeNumberOfClientsAsync()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.TakeNumberOfClientsAsync(1,5))
                        .ReturnsAsync(new List<ClientDTO>() {client});
            clientService.Setup(a => a.GetAllClientsCountAsync())
                         .ReturnsAsync(1);

            var result = await controller.ShowAllClients(null);

            clientService.Verify(a => a.TakeNumberOfClientsAsync(1, 5), Times.Once);
            clientService.Verify(a => a.GetAllClientsCountAsync(), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.TakeNumberOfClientsAsync(1, 5))
                        .ReturnsAsync(new List<ClientDTO>() { client });
            clientService.Setup(a => a.GetAllClientsCountAsync())
                         .ReturnsAsync(1);

            var result = await controller.ShowAllClients(null);

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));

            var view = (PartialViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(PaginationClientViewModel));
        }

        //Utility starts here
        private ClientController SetupController(IClientService clientService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new ClientController(clientService, clientMapper, accountMapper, userMapper);
        }
    }
}
