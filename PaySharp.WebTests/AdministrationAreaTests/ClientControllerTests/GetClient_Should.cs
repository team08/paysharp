﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.ClientControllerTests
{
    [TestClass]
    public class GetClient_Should
    {
        [TestMethod]
        public async Task InvokeGetClientAsync()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.GetClientAsync("name"))
                        .ReturnsAsync(client);

            var result = await controller.GetClient("name");

            clientService.Verify(a => a.GetClientAsync("name"), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.GetClientAsync("name"))
                        .ReturnsAsync(client);

            var result = await controller.GetClient("name");

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(ClientViewModel));
        }

        [TestMethod]
        public async Task ThrowsException()
        {
            var clientService = new Mock<IClientService>();
            var controller = this.SetupController(clientService.Object);

            var client = new ClientDTO()
            {
                Name = "name",
                ClientId = 1,
                Accounts = new List<AccountDTO>(),
                UsersClients = new List<UsersClientsDTO>()
            };

            clientService.Setup(a => a.GetClientAsync("name"))
                        .ThrowsAsync(new EntityNotFoundException("Tests"));

            var result = await controller.GetClient("name");

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var message = (BadRequestObjectResult)result;

            Assert.AreEqual(message.Value, "Tests");
        }

        //Utility starts here
        private ClientController SetupController(IClientService clientService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new ClientController(clientService, clientMapper, accountMapper, userMapper);
        }
    }
}
