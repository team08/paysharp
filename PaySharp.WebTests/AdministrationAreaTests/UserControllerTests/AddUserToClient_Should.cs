﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.UserControllerTests
{
    [TestClass]
    public class AddUserToClient_Should
    {
        [TestMethod]
        public async Task InvokeAddUserToClientAsync()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password"
            };

            userService.Setup(a => a.AddUserToClientAsync(1, "newname"))
                    .ReturnsAsync(userDto);

            var result = await controller.AddUserToClient(1,"newname");

            userService.Verify(a => a.AddUserToClientAsync(1,"newname"), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password"
            };

            userService.Setup(a => a.AddUserToClientAsync(1, "newname"))
                    .ReturnsAsync(userDto);

            var result = await controller.AddUserToClient(1, "newname");

            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("UserDetails", redirectResult.ActionName);
            Assert.AreEqual("User", redirectResult.ControllerName);
            Assert.AreEqual("Administration", redirectResult.RouteValues["area"]);
            Assert.AreEqual((long)1, redirectResult.RouteValues["Id"]);
        }

        //Utility starts here
        private UserController SetupController(IUserService userService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new UserController(userService, clientMapper, accountMapper, userMapper);
        }
    }
}
