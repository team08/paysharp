﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Threading.Tasks;


namespace PaySharp.WebTests.AdministrationAreaTests.UserControllerTests
{
    [TestClass]
    public class CreateUser_Should
    {
        [TestMethod]
        public async Task InvokeAddUserAsync()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var user = new UserViewModel()
            {
                UserName = "username",
                Name = "name",
                Password = "password"
            };
            var result = await controller.CreateUser(user);

            userService.Verify(a => a.AddUserAsync("name", "username", "password"), Times.Once);
        }

        [TestMethod]
        public async Task ModelStateIsNotValid_ReturnsRedirectActionResult()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var user = new UserViewModel()
            {
                UserName = "username",
                Name = "name",
                Password = "password"
            };
            controller.ModelState.AddModelError("key", "error message");
            var result = await controller.CreateUser(user);

            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Index", redirectResult.ActionName);
            Assert.AreEqual("Dashboard", redirectResult.ControllerName);
            Assert.AreEqual("Administration", redirectResult.RouteValues["area"]);
        }
        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var user = new UserViewModel()
            {
                UserName = "username",
                Name = "name",
                Password = "password"
            };
            var result = await controller.CreateUser(user);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            var view = (OkObjectResult)result;

            Assert.AreEqual(view.Value, "User with name name was created successfully");
        }

        [TestMethod]
        public async Task ThrowsException()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var user = new UserViewModel()
            {
                UserName = "username",
                Name = "name",
                Password = "password"
            };

            userService.Setup(a => a.AddUserAsync("name", "username", "password"))
    .ThrowsAsync(new EntityAlreadyExistsException("Tests"));

            var result = await controller.CreateUser(user);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var message = (BadRequestObjectResult)result;

            Assert.AreEqual(message.Value, "Tests");
        }
        //Utility starts here
        private UserController SetupController(IUserService userService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new UserController(userService, clientMapper, accountMapper, userMapper);
        }
    }
}
