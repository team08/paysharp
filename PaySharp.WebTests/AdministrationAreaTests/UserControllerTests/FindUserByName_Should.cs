﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.UserControllerTests
{
    [TestClass]
    public class FindUserByName_Should
    {
        [TestMethod]
        public async Task InvokeFindUserContainingAsync()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            userService.Setup(a => a.FindUserContainingAsync("nam"))
                            .ReturnsAsync(new List<string>() {"name"} );

            var result = await controller.FindUserByName("nam");

            userService.Verify(a => a.FindUserContainingAsync("nam"), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyJson()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            userService.Setup(a => a.FindUserContainingAsync("nam"))
                            .ReturnsAsync(new List<string>() { "name","names" });

            var result = await controller.FindUserByName("nam");

            Assert.IsInstanceOfType(result, typeof(JsonResult));

            var json = (JsonResult)result;

            var names = json.Value as List<string>;

            Assert.AreEqual(names.Count, 2);

            Assert.AreEqual(names[0], "name");
            Assert.AreEqual(names[1], "names");
        }

        [TestMethod]
        public async Task ReturnsCorrectlyJsonIfNullOrEmpty()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var result = await controller.FindUserByName(null);

            Assert.IsInstanceOfType(result, typeof(JsonResult));

            var json = (JsonResult)result;

            Assert.AreEqual(json.Value, "");
        }

        //Utility starts here
        private UserController SetupController(IUserService userService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new UserController(userService, clientMapper, accountMapper, userMapper);
        }
    }
}
