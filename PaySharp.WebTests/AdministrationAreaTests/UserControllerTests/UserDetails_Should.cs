﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.UserControllerTests
{
    [TestClass]
    public class UserDetails_Should
    {
        [TestMethod]
        public async Task InvokeGetUserAsync()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password",
                Accounts = new List<AccountDTO>(),
                Clients = new List<ClientDTO>()
            };

            userService.Setup(a => a.GetUserAsync(1)).ReturnsAsync(userDto);

            var result = await controller.UserDetails(1);

            userService.Verify(a => a.GetUserAsync(1), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password",
                Accounts = new List<AccountDTO>(),
                Clients = new List<ClientDTO>()
            };

            userService.Setup(a => a.GetUserAsync(1)).ReturnsAsync(userDto);

            var result = await controller.UserDetails(1);

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(UserViewModel));
        }

        [TestMethod]
        public async Task Throws_EntityNotFoundException()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password",
                Accounts = new List<AccountDTO>(),
                Clients = new List<ClientDTO>()
            };

            userService.Setup(a => a.GetUserAsync(1))
                    .ThrowsAsync(new EntityNotFoundException("Tests"));

            var result = await controller.UserDetails(1);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var message = (BadRequestObjectResult)result;

            Assert.AreEqual(message.Value, "Tests");
        }

        //Utility starts here
        private UserController SetupController(IUserService userService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new UserController(userService, clientMapper, accountMapper, userMapper);
        }
    }
}
