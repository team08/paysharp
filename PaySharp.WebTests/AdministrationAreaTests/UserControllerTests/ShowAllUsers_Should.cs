﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Areas.Administration.ViewModels;
using PaySharp.Web.Mappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.UserControllerTests
{
    [TestClass]
    public class ShowAllUsers_Should
    {
        [TestMethod]
        public async Task InvokeTakeNumberOfUsersAsync()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);


            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password"
            };

            userService.Setup(a => a.TakeNumberOfUsersAsync(1, 5))
                    .ReturnsAsync(new List<UserDTO>() { userDto });
            userService.Setup(a => a.GetAllUsersCountAsync())
                   .ReturnsAsync(1);

            var result = await controller.ShowAllUsers(1);

            userService.Verify(a => a.TakeNumberOfUsersAsync(1, 5), Times.Once);
            userService.Verify(a => a.GetAllUsersCountAsync(), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);


            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password"
            };

            userService.Setup(a => a.TakeNumberOfUsersAsync(1, 5))
                    .ReturnsAsync(new List<UserDTO>() { userDto });
            userService.Setup(a => a.GetAllUsersCountAsync())
                   .ReturnsAsync(1);

            var result = await controller.ShowAllUsers(1);

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));

            var view = (PartialViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(PaginationUserViewModel));
        }
        //Utility starts here
        private UserController SetupController(IUserService userService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new UserController(userService, clientMapper, accountMapper, userMapper);
        }
    }
}
