﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.UserControllerTests
{
    [TestClass]
    public class CreateUserToClient_Should
    {
        [TestMethod]
        public async Task InvokeAddUserAsyncAndAddUserToClientAsync()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var user = new UserViewModel()
            {
                UserName = "username",
                Name = "name",
                Password = "password",
                ClientId = 1
            };
            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password"
            };

            userService.Setup(a => a.AddUserAsync("name", "username", "password"))
                    .ReturnsAsync(userDto);

            var result = await controller.CreateUserToClient(user);

            userService.Verify(a => a.AddUserAsync("name", "username", "password"), Times.Once);
            userService.Verify(a => a.AddUserToClientAsync(1, 1), Times.Once);
        }

        [TestMethod]
        public async Task ModelStateIsNotValid_ReturnsRedirectActionResult()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var user = new UserViewModel()
            {
                UserName = "username",
                Name = "name",
                Password = "password"
            };
            controller.ModelState.AddModelError("key", "error message");
            var result = await controller.CreateUserToClient(user);

            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Index", redirectResult.ActionName);
            Assert.AreEqual("Dashboard", redirectResult.ControllerName);
            Assert.AreEqual("Administration", redirectResult.RouteValues["area"]);
        }
        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var user = new UserViewModel()
            {
                UserName = "username",
                Name = "name",
                Password = "password",
                ClientId = 1
            };
            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Name = "name",
                Password = "password"
            };

            userService.Setup(a => a.AddUserAsync("name", "username", "password"))
   .ReturnsAsync(userDto);

            var result = await controller.CreateUserToClient(user);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            var view = (OkObjectResult)result;

            Assert.AreEqual(view.Value, "User with name name was created successfully");
        }

        [TestMethod]
        public async Task ThrowsException()
        {
            var userService = new Mock<IUserService>();
            var controller = this.SetupController(userService.Object);

            var user = new UserViewModel()
            {
                UserName = "username",
                Name = "name",
                Password = "password"
            };

            userService.Setup(a => a.AddUserAsync("name", "username", "password"))
    .ThrowsAsync(new EntityAlreadyExistsException("Tests"));

            var result = await controller.CreateUserToClient(user);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var message = (BadRequestObjectResult)result;

            Assert.AreEqual(message.Value, "Tests");
        }
        //Utility starts here
        private UserController SetupController(IUserService userService)
        {

            var clientMapper = new ClientViewModelMapper();
            var accountMapper = new AccountViewModelMapper();
            var userMapper = new UserViewModelMapper();

            return new UserController(userService, clientMapper, accountMapper, userMapper);
        }
    }
}
