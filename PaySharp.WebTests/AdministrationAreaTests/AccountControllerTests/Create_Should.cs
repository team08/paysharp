﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.AccountControllerTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task InvokeCreateAccountAsync()
        {
            var accountService = new Mock<IAccountService>();
            var controller = this.SetupController(accountService.Object);

            var result = await controller.Create("1");

            accountService.Verify(a => a.CreateAccountAsync(1), Times.Once);
        }

        [TestMethod]
        public async Task ThrowsException()
        {
            var accountService = new Mock<IAccountService>();
            accountService.Setup(a => a.CreateAccountAsync(1))
                .ThrowsAsync(new EntityAlreadyExistsException("Tests"));
            var controller = this.SetupController(accountService.Object);

            var result = await controller.Create("1");

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var message = (BadRequestObjectResult)result;

            Assert.AreEqual(message.Value, "Tests");

        }

        [TestMethod]
        public async Task ReturnCorrectActionResult()
        {
            var accountService = new Mock<IAccountService>();
            var controller = this.SetupController(accountService.Object);

            var result = await controller.Create("1");

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            var message = (OkObjectResult)result;

            Assert.AreEqual(message.Value, "Account created");

        }

        //Utility starts here
        private AccountController SetupController(IAccountService accountService)
        {

            var accountMapper = new AccountViewModelMapper();

            return new AccountController(accountService, accountMapper);
        }
    }
}
