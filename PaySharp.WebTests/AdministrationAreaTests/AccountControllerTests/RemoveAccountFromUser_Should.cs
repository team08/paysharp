﻿using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace PaySharp.WebTests.AdministrationAreaTests.AccountControllerTests
{
    [TestClass]
    public class RemoveAccountFromUser_Should
    {
        [TestMethod]
        public async Task InvokeAddAccountToUserAsync()
        {
            var accountService = new Mock<IAccountService>();
            var controller = this.SetupController(accountService.Object);

            var account = new AccountViewModel()
            {
                Id = 1,
                UserId = 1
            };
            var result = await controller.RemoveAccountFromUser(account);

            accountService.Verify(a => a.RemoveAccountFromUserAsync(1, 1), Times.Once);
        }


        [TestMethod]
        public async Task ReturnCorrectActionResult()
        {
            var accountService = new Mock<IAccountService>();
            var controller = this.SetupController(accountService.Object);

            var account = new AccountViewModel()
            {
                Id = 1,
                UserId = 1
            };
            var result = await controller.RemoveAccountFromUser(account);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            var view = (OkObjectResult)result;

            Assert.AreEqual(view.Value, "Account removed from user");
        }

        //Utility starts here
        private AccountController SetupController(IAccountService accountService)
        {

            var accountMapper = new AccountViewModelMapper();

            return new AccountController(accountService, accountMapper);
        }

    }
}
