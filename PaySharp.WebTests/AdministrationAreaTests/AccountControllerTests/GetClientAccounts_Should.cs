﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.Administration.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.AdministrationAreaTests.AccountControllerTests
{
    [TestClass]
    public class GetClientAccounts_Should
    {
        [TestMethod]
        public async Task InvokeGetClientAccountsAsync()
        {
            var accountService = new Mock<IAccountService>();
            var controller = this.SetupController(accountService.Object);
            var account = new AccountDTO()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "1234567890",

            };

            accountService.Setup(a => a.GetClientAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });
            var user = new UserViewModel()
            {
                Id = 1
            };
            var result = await controller.GetClientAccounts(1);

            accountService.Verify(a => a.GetClientAccountsAsync(1), Times.Once);
        }

        [TestMethod]
        public async Task ReturnCorrectActionResult()
        {
            var accountService = new Mock<IAccountService>();
            var controller = this.SetupController(accountService.Object);
            var account = new AccountDTO()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "1234567890",

            };

            accountService.Setup(a => a.GetClientAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });
            var user = new UserViewModel()
            {
                Id = 1
            };
            var result = await controller.GetClientAccounts(1);

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));

            var view = (PartialViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(List<AccountViewModel>));
        }

        //Utility starts here
        private AccountController SetupController(IAccountService accountService)
        {

            var accountMapper = new AccountViewModelMapper();

            return new AccountController(accountService, accountMapper);
        }
    }
}
