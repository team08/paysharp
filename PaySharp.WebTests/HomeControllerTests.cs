﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PaySharp.Web.Controllers;


namespace PaySharp.WebTests
{
    [TestClass]
    public class HomeControllerTests
    {
        [TestMethod]
        public void ReturnCorrectlyActionResult_Invalid()
        {
            var controller = new HomeController();

            var result = controller.Invalid();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void ReturnCorrectlyActionResult_ServerError()
        {
            var controller = new HomeController();

            var result = controller.ServerError();

            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
