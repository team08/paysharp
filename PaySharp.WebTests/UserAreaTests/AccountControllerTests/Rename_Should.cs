﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Web.Areas.User.Controllers;
using PaySharp.Web.Mappers;
using System.Threading.Tasks;

namespace PaySharp.WebTests.UserAreaTests.AccountControllerTests
{
    [TestClass]
    public class Rename_Should
    {
        [TestMethod]
        public async Task InvokeRenameAccontAsync()
        {
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var accountService = new Mock<IAccountService>();
            var accountMapper = new AccountViewModelMapper();


            var controller = new AccountController(authorization.Object, httpContextAccessorMock.Object, accountService.Object, accountMapper);

            var result = await controller.Rename(1, "test");

            accountService.Verify(a => a.RenameAccountAsync(1, "test"), Times.Once);
        }


        [TestMethod]
        public async Task ReturnCorrectActionResult()
        {
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var accountService = new Mock<IAccountService>();
            var accountMapper = new AccountViewModelMapper();


            var controller = new AccountController(authorization.Object, httpContextAccessorMock.Object, accountService.Object, accountMapper);

            var result = await controller.Rename(1, "test");

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            var message = (OkObjectResult)result;

            Assert.AreEqual(message.Value, "The account nickname was renamed to test!");
        }
    }
}
