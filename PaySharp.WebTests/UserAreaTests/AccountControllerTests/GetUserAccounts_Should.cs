﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.User.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PaySharp.WebTests.UserAreaTests.AccountControllerTests
{
    [TestClass]
    public class GetUserAccounts_Should
    {
        [TestMethod]
        public async Task ReturnCorrectActionResult()
        {
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var accountService = new Mock<IAccountService>();
            var accountMapper = new AccountViewModelMapper();
           
            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
           authorization.Setup(x => x.GetLoggedUserId(It.IsAny<string>())).Returns(1);

            var account = new AccountDTO()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "1234567890",

            };

            accountService.Setup(a => a.GetUserAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });


            var controller = new AccountController(authorization.Object, httpContextAccessorMock.Object, accountService.Object, accountMapper);

            var result = await controller.GetUserAccounts();

            
            Assert.IsInstanceOfType(result, typeof(PartialViewResult));

            var view = (PartialViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(List<AccountViewModel>));
        }


        [TestMethod]
        public async Task CheckIfMethodsAreInvoke()
        {
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var accountService = new Mock<IAccountService>();
            var accountMapper = new AccountViewModelMapper();

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorization.Setup(x => x.GetLoggedUserId(It.IsAny<string>())).Returns(1);

            var account = new AccountDTO()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "1234567890",

            };

            accountService.Setup(a => a.GetUserAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });


            var controller = new AccountController(authorization.Object, httpContextAccessorMock.Object, accountService.Object, accountMapper);

            var result = await controller.GetUserAccounts();

            accountService.Verify(a => a.GetUserAccountsAsync(1), Times.Once);
            httpContextAccessorMock.Verify(x => x.HttpContext.Request.Cookies["SecurityToken"], Times.Once);
            authorization.Verify(x => x.GetLoggedUserId(It.IsAny<string>()), Times.Once);

        }
    }
}
