﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.User.Controllers;
using PaySharp.Web.Mappers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.UserAreaTests.AccountControllerTests
{
    [TestClass]
    public class FindSingleByAccName_Should
    {
        [TestMethod]
        public async Task IfParameterIsNull_ReturnEmptyJson()
        {
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var accountService = new Mock<IAccountService>();
            var accountMapper = new AccountViewModelMapper();

            var controller = new AccountController(authorization.Object, httpContextAccessorMock.Object, accountService.Object, accountMapper);

            var result = await controller.FindSingleByAccName("");

            Assert.IsInstanceOfType(result, typeof(JsonResult));

            Assert.AreEqual(result.Value, "");

        }


        [TestMethod]
        public async Task InvokeRenameAccontAsync()
        {
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var accountService = new Mock<IAccountService>();
            var accountMapper = new AccountViewModelMapper();


            var controller = new AccountController(authorization.Object, httpContextAccessorMock.Object, accountService.Object, accountMapper);

            var account = new AccountDTO()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "1234567890",

            };

            accountService.Setup(a => a.FindAccountsContainingAsync("test"))
               .ReturnsAsync(new List<AccountDTO>() { account });

            var result = await controller.FindSingleByAccName("test");

            accountService.Verify(a => a.FindAccountsContainingAsync("test"), Times.Once);
        }



        [TestMethod]
        public async Task ReturnCorrectActionResult()
        {
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var accountService = new Mock<IAccountService>();
            var accountMapper = new AccountViewModelMapper();

            var controller = new AccountController(authorization.Object, httpContextAccessorMock.Object, accountService.Object, accountMapper);
            var account = new AccountDTO()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "1234567890",

            };
            accountService.Setup(a => a.FindAccountsContainingAsync("test"))
              .ReturnsAsync(new List<AccountDTO>() { account });
            var result = await controller.FindSingleByAccName("test");

            Assert.IsInstanceOfType(result, typeof(JsonResult));

        }
    }
}
