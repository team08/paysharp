﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.User.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.UserAreaTests.TransactionControllerTests
{
    [TestClass]
    public class MakePayment_Should
    {
        [TestMethod]
        public async Task IfAccountViewModelDoesNotExists_ThrowException()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorization.Setup(x => x.GetLoggedUserId(It.IsAny<string>())).Returns(1);

            var account = new AccountDTO()
            {

            };

            accountService.Setup(a => a.GetUserAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });

            var result = await controller.MakePayment();

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(TransactionViewModel));
        }
    }
}
