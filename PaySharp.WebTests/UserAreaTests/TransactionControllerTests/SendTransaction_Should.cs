﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Web.Areas.User.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Threading.Tasks;

namespace PaySharp.WebTests.UserAreaTests.TransactionControllerTests
{
    [TestClass]
    public class SendTransaction_Should
    {
        [TestMethod]
        public async Task IfModelStateIsInvalid_RedirectToAction()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            controller.ModelState.AddModelError("key", "error message");

            var result = await controller.SendTransaction(new TransactionViewModel());

            var view = (RedirectToActionResult)result;

            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));

            Assert.AreEqual(view.ActionName, "Index");
            Assert.AreEqual(view.ControllerName, "Dashboard");
            Assert.AreEqual(view.RouteValues["area"], "User");
        }


        [TestMethod]
        public async Task IfSenderAccountIsNull_RedirectToAction()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var accountViewModel = new AccountViewModel()
            {
                AccountNumber = "test"
            };

            
            var result = await controller.SendTransaction(new TransactionViewModel()
            {
                ReceiverAccount = accountViewModel,
                Description = "test",
                Amount = 1
            });

            var view = (OkObjectResult)result;

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            Assert.AreEqual(view.Value, "Transaction between 0 and test was made successfully");
            
        }


        [TestMethod]
        public async Task IfSenderAccountIsNotNull_RedirectToAction()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var accountViewModel = new AccountViewModel()
            {
                AccountNumber = "test"
            };


            var result = await controller.SendTransaction(new TransactionViewModel()
            {
                SenderAccount = accountViewModel,
                ReceiverAccount = accountViewModel,
                Description = "test",
                Amount = 1
            });

            var view = (OkObjectResult)result;

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            Assert.AreEqual(view.Value, "Transaction between test and test was made successfully");

        }

        [TestMethod]
        public async Task IfAccountViewModelDoesNotExists_ThrowException()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var result = await controller.SendTransaction(new TransactionViewModel()
            {
                
                Description = "test",
                Amount = 1
            });

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var view = (BadRequestObjectResult)result;

            Assert.AreEqual(view.Value, "Object reference not set to an instance of an object.");

        }
    }
}
