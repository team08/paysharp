﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.User.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.UserAreaTests.TransactionControllerTests
{
    [TestClass]
    public class GetTransactions_Should
    {
        [TestMethod]
        public async Task IfAccountIdIsNull_ReturnViewModel()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorization.Setup(x => x.GetLoggedUserId(It.IsAny<string>())).Returns(1);

            var account = new AccountDTO()
            {

            };

            accountService.Setup(a => a.GetUserAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });

            var transaction = new TransactionDTO()
            {
                Id = 1,
                Amount = 1,
                Description = "test",
                IsRecieved = true,
                IsSend = true,
                ReceiverAccountID = 1,
                SenderAccountID = 1,
                Status = "status",
                StatusId = 1,
                TimeStamp = DateTime.Now,
                ReceiverAccount = account,
                SenderAccount = account
            };

            transactionService.Setup(a => a.GetAllUserTransactions(1, 1, 5))
                .ReturnsAsync(new List<TransactionDTO>() { transaction });
            transactionService.Setup(a => a.GetTransactionsCount(1, null))
                .ReturnsAsync(1);

            var result = await controller.GetTransactions(1, null);

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));

            var view = (PartialViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(PaginationTransactionsViewModel));
        }


        [TestMethod]
        public async Task IfAccountIdIsНотNull_ReturnViewModel()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorization.Setup(x => x.GetLoggedUserId(It.IsAny<string>())).Returns(1);

            var account = new AccountDTO()
            {

            };

            accountService.Setup(a => a.GetUserAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });

            var transaction = new TransactionDTO()
            {
                Id = 1,
                Amount = 1,
                Description = "test",
                IsRecieved = true,
                IsSend = true,
                ReceiverAccountID = 1,
                SenderAccountID = 1,
                Status = "status",
                StatusId = 1,
                TimeStamp = DateTime.Now,
                ReceiverAccount = account,
                SenderAccount = account
            };

            transactionService.Setup(a => a.GetUserTransactionsByAccount(1, 1, 1, 5))
                .ReturnsAsync(new List<TransactionDTO>() { transaction });
            accountService.Setup(a => a.GetAccountAsync(1))
                .ReturnsAsync(new AccountDTO());
            transactionService.Setup(a => a.GetTransactionsCount(1, 1))
                .ReturnsAsync(1);

            var result = await controller.GetTransactions(1, 1);

            Assert.IsInstanceOfType(result, typeof(PartialViewResult));

            var view = (PartialViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(PaginationTransactionsViewModel));

        }



        [TestMethod]
        public async Task VerifyInvokedMethodsInGetTransactioins()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorization.Setup(x => x.GetLoggedUserId(It.IsAny<string>())).Returns(1);

            var account = new AccountDTO()
            {

            };

            accountService.Setup(a => a.GetUserAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });

            var transaction = new TransactionDTO()
            {
                Id = 1,
                Amount = 1,
                Description = "test",
                IsRecieved = true,
                IsSend = true,
                ReceiverAccountID = 1,
                SenderAccountID = 1,
                Status = "status",
                StatusId = 1,
                TimeStamp = DateTime.Now,
                ReceiverAccount = account,
                SenderAccount = account
            };

            transactionService.Setup(a => a.GetUserTransactionsByAccount(1, 1, 1, 5))
                .ReturnsAsync(new List<TransactionDTO>() { transaction });
            accountService.Setup(a => a.GetAccountAsync(1))
                .ReturnsAsync(new AccountDTO());
            transactionService.Setup(a => a.GetTransactionsCount(1, 1))
                .ReturnsAsync(1);

            var result = await controller.GetTransactions(1, 1);

            httpContextAccessorMock.Verify(x => x.HttpContext.Request.Cookies["SecurityToken"], Times.Once);

            authorization.Verify(x => x.GetLoggedUserId(It.IsAny<string>()), Times.Once);

            accountService.Verify(x => x.GetUserAccountsAsync(1), Times.Once);

            transactionService.Verify(x => x.GetUserTransactionsByAccount(1, 1, 1, 5), Times.Once);

            accountService.Verify(x => x.GetAccountAsync(1), Times.Once);

            transactionService.Verify(x => x.GetTransactionsCount(1, 1), Times.Once);

        }
    }
}
