﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.User.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Threading.Tasks;

namespace PaySharp.WebTests.UserAreaTests.TransactionControllerTests
{
    [TestClass]
    public class SendSavedTransactions_Should
    {
        [TestMethod]
        public async Task IfAccountViewModelDoesNotExists_ThrowException()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var accountDTO = new AccountDTO()
            {
                AccountNumber = "1"
            };

            transactionService.Setup(x => x.SendSavedTransactionAsync(1)).ReturnsAsync(new TransactionDTO() { SenderAccount = accountDTO, ReceiverAccount = accountDTO });

            var result = await controller.SendSavedTransactions(1);

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            var ok = (OkObjectResult)result;

            Assert.AreEqual(ok.Value, "Transaction between 1 and 1 was made successfully");

        }

    }
}
