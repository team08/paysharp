﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.User.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System;
using System.Threading.Tasks;

namespace PaySharp.WebTests.UserAreaTests.TransactionControllerTests
{
    [TestClass]
    public class EditTransaction
    {
        [TestMethod]
        public async Task IfModelStateIsInvalid_RedirectToAction()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            controller.ModelState.AddModelError("key", "error message");

            var result = await controller.EditTransaction(new TransactionViewModel());

            var view = (RedirectToActionResult)result;

            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));

            Assert.AreEqual(view.ActionName, "Index");
            Assert.AreEqual(view.ControllerName, "Dashboard");
        }




        [TestMethod]
        public async Task IfModelStateIsValid_RedirectToAction()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var accountViewModel = new AccountViewModel()
            {
                AccountNumber = "test"
            };


            var result = await controller.EditTransaction(new TransactionViewModel()
            {
                ReceiverAccount = accountViewModel,
                Description = "test",
                Amount = 1,
                Id = 1,
                SenderAccount = accountViewModel
            });

            var view = (OkObjectResult)result;

            Assert.IsInstanceOfType(result, typeof(OkObjectResult));

            Assert.AreEqual(view.Value, "Transaction was made modified and saved!");

        }


        [TestMethod]
        public async Task IfArgumentIsNotValid_ThrowArgumentException()
        {
            var transactionService = new Mock<ITransactionService>();
            var accountService = new Mock<IAccountService>();
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();

            var accountMapper = new AccountViewModelMapper();
            var transactionMapper = new TransactionViewModelMapper(accountMapper);

            var controller = new TransactionController(transactionService.Object, accountService.Object, authorization.Object, httpContextAccessorMock.Object, transactionMapper, accountMapper);

            var accountViewModel = new AccountViewModel()
            {
                AccountNumber = "test"
            };


            var result = await controller.EditTransaction(new TransactionViewModel()
            {
                ReceiverAccount = accountViewModel,
                Description = "",
                Amount = 1,
                Id = 1,
                
            });

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
            var message = (BadRequestObjectResult)result;

            Assert.AreEqual(message.Value, "Object reference not set to an instance of an object.");
        }
    }
}
