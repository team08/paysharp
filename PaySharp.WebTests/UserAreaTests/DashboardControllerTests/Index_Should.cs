﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Web.Areas.User.Controllers;
using PaySharp.Web.Mappers;
using PaySharp.Web.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PaySharp.WebTests.UserAreaTests.DashboardControllerTests
{
    [TestClass]
    public class Index_Should
    {
        [TestMethod]
        public async Task InvokeGetUserAccountsAsync()
        {
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var accountService = new Mock<IAccountService>();
            var accountMapper = new AccountViewModelMapper();
            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorization.Setup(x => x.GetLoggedUserId(It.IsAny<string>())).Returns(1);

            var account = new AccountDTO()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "1234567890",
            };

            accountService.Setup(a => a.GetUserAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });

            var controller = new DashboardController(authorization.Object, httpContextAccessorMock.Object, accountService.Object, accountMapper);

            var result = await controller.Index();

            accountService.Verify(a => a.GetUserAccountsAsync(1), Times.Once);
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResult()
        {
            var authorization = new Mock<IAuthorizationManager>();
            var httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            var accountService = new Mock<IAccountService>();
            var accountMapper = new AccountViewModelMapper();
            var exampleToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

            httpContextAccessorMock.Setup(x => x.HttpContext.Request.Cookies["SecurityToken"]).Returns(exampleToken);
            authorization.Setup(x => x.GetLoggedUserId(It.IsAny<string>())).Returns(1);

            var account = new AccountDTO()
            {
                Id = 1,
                AccountNumber = "1234567890",
                Balance = 10000,
                NickName = "1234567890",
            };

            accountService.Setup(a => a.GetUserAccountsAsync(1))
                .ReturnsAsync(new List<AccountDTO>() { account });

            var controller = new DashboardController(authorization.Object, httpContextAccessorMock.Object, accountService.Object, accountMapper);

            var result = await controller.Index();

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(List<AccountViewModel>));
        }
    }
}
