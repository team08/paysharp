﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Services.DTO;
using PaySharp.Services.Exceptions;
using PaySharp.Web.Controllers;
using PaySharp.Web.ViewModels;
using System.Threading.Tasks;

namespace PaySharp.WebTests.IdentityControllerTests
{
    [TestClass]
    public class Login_Post_Should
    {
        [TestMethod]
        public async Task ReturnsCorrectlyActionResult_ModelNotValid()
        {
            var bannerService = new Mock<IBannerService>();
            var userService = new Mock<IUserService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var cache = new Mock<IMemoryCache>();

            var loginVm = new LoginViewModel()
            {
                UserName = "username",
                Password = "password"
            };

            var controller = new IdentityController(cache.Object, userService.Object, bannerService.Object,
                tokenManager.Object, cookieManager.Object);
            controller.ModelState.AddModelError("key", "error message");
            var result = await controller.Login(loginVm);

            Assert.IsInstanceOfType(result, typeof(ViewResult));

            var view = (ViewResult)result;

            Assert.IsInstanceOfType(view.Model, typeof(LoginViewModel));
        }

        [TestMethod]
        public async Task ReturnsCorrectlyActionResul()
        {
            var bannerService = new Mock<IBannerService>();
            var userService = new Mock<IUserService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var cache = new Mock<IMemoryCache>();

            var loginVm = new LoginViewModel()
            {
                UserName = "username",
                Password = "password"
            };

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Password = "password",
                Role = "user"
            };

            userService.Setup(u => u.GetUserAsync("username", "password")).ReturnsAsync(userDto);
            tokenManager.Setup(a => a.GenerateToken("username", "admin", "1"))
           .Returns("token");

            var controller = new IdentityController(cache.Object, userService.Object, bannerService.Object,
                tokenManager.Object, cookieManager.Object);

            var result = await controller.Login(loginVm);

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Index", redirectResult.ActionName);
            Assert.AreEqual("Dashboard", redirectResult.ControllerName);
            Assert.AreEqual("User", redirectResult.RouteValues["area"]);
        }

        [TestMethod]
        public async Task InvokesGetUserAsync()
        {
            var bannerService = new Mock<IBannerService>();
            var userService = new Mock<IUserService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var cache = new Mock<IMemoryCache>();

            var loginVm = new LoginViewModel()
            {
                UserName = "username",
                Password = "password"
            };

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Password = "password",
                Role = "user"
            };

            userService.Setup(u => u.GetUserAsync("username", "password")).ReturnsAsync(userDto);
            tokenManager.Setup(a => a.GenerateToken("username", "user", "1"))
           .Returns("token");

            var controller = new IdentityController(cache.Object, userService.Object, bannerService.Object,
                tokenManager.Object, cookieManager.Object);

            var result = await controller.Login(loginVm);

            userService.Verify(a => a.GetUserAsync("username", "password"), Times.Once);
            tokenManager.Verify(a => a.GenerateToken("username", "user", "1"), Times.Once);
            cookieManager.Verify(a => a.AddSessionCookieForToken("token", "username"), Times.Once);
        }

        [TestMethod]
        public async Task Throws_EntityNotFoundException_ReurnsBadRequest()
        {
            var bannerService = new Mock<IBannerService>();
            var userService = new Mock<IUserService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var cache = new Mock<IMemoryCache>();

            var loginVm = new LoginViewModel()
            {
                UserName = "username",
                Password = "password"
            };

            var userDto = new UserDTO()
            {
                UserId = 1,
                UserName = "username",
                Password = "password",
                Role = "user"
            };

            userService.Setup(u => u.GetUserAsync("username", "password"))
                .ThrowsAsync(new EntityNotFoundException("Tests"));
            tokenManager.Setup(a => a.GenerateToken("username", "user", "1"))
           .Returns("token");

            var controller = new IdentityController(cache.Object, userService.Object, bannerService.Object,
                tokenManager.Object, cookieManager.Object);

            var result = await controller.Login(loginVm);

            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));

            var view = (BadRequestObjectResult)result;

            Assert.AreEqual(view.Value, "Tests");
        }
    }
}
