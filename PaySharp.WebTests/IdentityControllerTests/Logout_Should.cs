﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PaySharp.Services.Contracts;
using PaySharp.Web.Controllers;

namespace PaySharp.WebTests.IdentityControllerTests
{
    [TestClass]
    public class Logout_Should
    {
        [TestMethod]
        public void InvokeDeleteSessionCookies()
        {
            var bannerService = new Mock<IBannerService>();
            var userService = new Mock<IUserService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var cache = new Mock<IMemoryCache>();


            var controller = new IdentityController(cache.Object, userService.Object, bannerService.Object,
                tokenManager.Object, cookieManager.Object);

            var result = controller.Logout();

            cookieManager.Verify(a => a.DeleteSessionCookies(), Times.Once);
        }

        [TestMethod]
        public void ReturnsCorrectlyActionResult()
        {
            var bannerService = new Mock<IBannerService>();
            var userService = new Mock<IUserService>();
            var tokenManager = new Mock<ITokenManager>();
            var cookieManager = new Mock<ICookieManager>();
            var cache = new Mock<IMemoryCache>();


            var controller = new IdentityController(cache.Object, userService.Object, bannerService.Object,
                tokenManager.Object, cookieManager.Object);

            var result = controller.Logout();

            var redirectResult = (RedirectToActionResult)result;

            Assert.AreEqual("Login", redirectResult.ActionName);
            Assert.AreEqual("Identity", redirectResult.ControllerName);
        }
    }
}
