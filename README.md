# ASP.NET MVC Final Project

# [PaySharp Web App](https://paysharpweb20190618012916.azurewebsites.net) 

[![Foo](https://i.ibb.co/B2m39Gw/Screen-Shot-06-17-19-at-09-09-AM.png)](https://paysharpweb20190618012916.azurewebsites.net)



&nbsp; PROJECT NAME: PaySharp
==============================

Link to project - https://paysharpweb20190618012916.azurewebsites.net

&nbsp; TEAM NAME: TEAM Sharp
=====================

&nbsp; TEAM LEADER: Hristo
===================

&nbsp; TEAM MEMBERS: Peter
===================

&nbsp; Project Information
===================

## Technologies used: <br />
 <b>
 &bull; .NET Core 2.2<br />
 &bull; EntityFrameworkCore 2.2.3 <br />
 &bull; SQL Server Express  <br />
 &bull; VisualStudio 2019<br />
 &bull; MS Test 1.3.2 <br />
 &bull; Moq 4.10.1 <br />
 &bull; JavaScript / jQuery <br />
 &bull; HTML/CSS <br />
 &bull; Git/GitLab
 </b> <br /> <br />
 

Project is hosted in Azure and features a Three-Layer architecture:<br />

Presentation Layer: consists of ASP.NET Core 2.2 Web Project.<br />
Business Layer: Services and business features processing class library.<br />
Data Layer: Holding the configuration for the data-base and additional mappings of table-relationships.It's hosted on Azure.<br />
Custom mappers and different date transffer objects for each layer.<br />
App uses Custom JSON-Web-Tokenization system for authentication and authorization.<br />
Thorough unit testing for the services and business features covering more than  90%.<br />                            
The Website allows the client to login and manage all of his resources.<br />
Most of the requests are Ajax based to enable better user experiance. <br />
Server side pagination which makes the transfer of data from the server to user on demand. <br />
Project includes caching for the banners of the login page <br />
Were using azure DevOps for continuous integration<br />

## Admins Part:

After login the admin goes to Dashboard,which provides creating of clients,users and listing of users and clients<br />
Add Users and Clients with Ajax requests.<br />
Listing of all Users and Clients ordered by the last created.The pagination of the users and clients is server side and works with Ajax requests without page refreshing.<br />
List newly added users and clients immediatelly with javascript inside dynamically updated table.<br />
Search for users and clients with Javascript autocomplete using Ajax request to return five mathcing records..<br />
[![](https://i.ibb.co/jr6cdsN/Screen-Shot-06-17-19-at-08-54-AM.png)](#)<br /><br />
Creation of clients account.<br />
Search for clients to attatch to users via Javascript autocomplete using ajax request <br />
Attaching and removing users to accounts with checkbox with ajax request on every checked box <br />
Add,edit, and delete and banners, giving start and ending date for each one of them to be active on the user login page.<br />
[![](https://i.ibb.co/9Nk0jHr/Screen-Shot-06-17-19-at-08-56-AM.png)](#)<br /><br />
## Users Can:

In Account Dashboard - Have a listing of all their accounts with info for each of them.<br />
View all of the Accounts Transactions.<br />
Initiate a new transaction with the account as pre-selected for Sender with a modal.<br />
Rename of the account Nickname.<br />
See  pie-chart of all their accounts' balances inside the account dashboard.<br /><br />
[![](https://i.ibb.co/jbMcK03/Screen-Shot-06-17-19-at-10-08-AM.png)](#)<br /><br />
[![](https://i.ibb.co/kB7h3dz/Screen-Shot-06-17-19-at-10-08-AM-001.png)](#)<br /><br />

Inside Transaction page - listing of all account transactions of the user filtered by account or all accounts.<br />
Edit saved transactions or just directly send them.<br /><br />
Browse the listed transactions with Server-Side pagination.<br />
Initiate new transaction/from.<br />
[![](https://i.ibb.co/TRv1NcZ/Screen-Shot-06-17-19-at-10-07-AM.png)](#)<br /><br />

Inside Payment page - create new transaction with dropdown for all user accounts and autocomplete search for reciever account<br />
Initiate new transaction/from.<br />
[![](https://i.ibb.co/X4S4WhN/Screen-Shot-06-17-19-at-10-07-AM-001.png)](#)<br /><br />








